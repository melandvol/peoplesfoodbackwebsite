<?php
class ControllerCatalogIngredients extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/filter');

		$this->document->setTitle("Игредиенты");

		$this->load->model('catalog/product');

		$this->getList();
	}

	public function edit() {
		$this->language->load('catalog/filter');

		$this->document->setTitle("Игредиенты");

		$this->load->model('catalog/product');

		if (isset($this->request->post['delete']) && $this->request->post['delete'] == "1") {
			$this->delete($this->request->post['selected']);
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_product->editIngredients($this->request->post['product_ingredients']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			if (isset($this->request->get['route'])) {
				$get = explode("/", $this->request->get['route']);
				$folder = $get[0];
				$file = $get[1];

				if ($file == 'user_permission') $table = 'user_group';
				else $table = $file;

				$this->load->model('setting/setting');
				$last_id = $this->model_setting_setting->getLastId($table);

				if ($file == 'setting') {
					$route = 'setting/store';
				} else {
					$route = $folder . '/' . $file;
				}

				if (!isset($url)) $url = "";

				if (($file != 'setting') && (isset($this->request->get[$table . '_id']) || isset($last_id))) {
					$url .= '&' . $table . '_id=' . (isset($this->request->get[$table . '_id']) ? $this->request->get[$table . '_id'] : $last_id);
				}

				if (isset($this->request->post['apply']) && $this->request->post['apply'] == "1") {
					$this->response->redirect($this->url->link($route, 'token=' . $this->session->data['token'] . $url, 'SSL'));
				} else {
					$this->response->redirect($this->url->link($route, 'token=' . $this->session->data['token'] . $url, 'SSL'));
				}
			}

			$this->response->redirect($this->url->link('catalog/ingredients', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
	}


	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'fgd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/ingredients', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['edit'] = $this->url->link('catalog/ingredients/edit', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$ingredients_total = $this->model_catalog_product->getTotalIngredients();

	 	if(isset($this->request->post['product_ingredients']))
			$data['ingredients'] = $this->request->post['product_ingredients'];
		else
			$data['ingredients'] = $this->model_catalog_product->getIngredientsList();


		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_group'] = $this->language->get('column_group');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/filter', 'token=' . $this->session->data['token'] . '&sort=fgd.name' . $url, 'SSL');
		$data['sort_sort_order'] = $this->url->link('catalog/filter', 'token=' . $this->session->data['token'] . '&sort=fg.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $ingredients_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/filter', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		//$data['results'] = sprintf($this->language->get('text_pagination'), ($filter_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($filter_total - $this->config->get('config_limit_admin'))) ? $filter_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $filter_total, ceil($filter_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/ingredients.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/ingredients')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['product_ingredients'] as $product_ingredients) {
			if ((utf8_strlen($product_ingredients['name']) < 1) || (utf8_strlen($product_ingredients['name']) > 64)) {
				$this->error['warning'] = $this->language->get('error_group');
			}
			if ((int)($product_ingredients['price'] == 0) || (int)($product_ingredients['price'] > 200)) {
				$this->error['warning'] = "Некорректная цена, лимит от 0 до 200";
			}
			if ((int)($product_ingredients['weight'] == 0) || (int)($product_ingredients['weight'] > 200)) {
				$this->error['warning'] = "Некорректный вес, лимит от 0 до 200";
			}
			if (((int)$product_ingredients['articul'] == 0)) {
				$this->error['warning'] = "Некорректный артикул";
			}
		}
		foreach ($this->request->post['product_ingredients'] as $key1 => $product_ingredients1) {
			foreach ($this->request->post['product_ingredients'] as $key2 => $product_ingredients2) {
				if($product_ingredients1['articul'] == $product_ingredients2['articul'] && $key1 != $key2) {
					$this->error['warning'] = "У вас есть дубликаты";
					break;
				}
			}
		}
		
		return !$this->error;
	}
	public function delete() {
		$this->language->load('catalog/filter');

		$this->document->setTitle("Ингредиенты");

		$this->load->model('catalog/product');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $ingredients) {
				$this->model_catalog_product->deleteIngredients($ingredients);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			//$this->response->redirect($this->url->link('catalog/ingredients', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		//$this->getList();
	}
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/ingredients')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

//	public function autocomplete() {
//		$json = array();
//
//		if (isset($this->request->get['filter_name'])) {
//			$this->load->model('catalog/filter');
//
//			$filter_data = array(
//				'filter_name' => $this->request->get['filter_name'],
//				'start'       => 0,
//				'limit'       => 5
//			);
//
//			$filters = $this->model_catalog_filter->getFilters($filter_data);
//
//			foreach ($filters as $filter) {
//				$json[] = array(
//					'filter_id' => $filter['filter_id'],
//					'name'      => strip_tags(html_entity_decode($filter['group'] . ' &gt; ' . $filter['name'], ENT_QUOTES, 'UTF-8'))
//				);
//			}
//		}
//
//		$sort_order = array();
//
//		foreach ($json as $key => $value) {
//			$sort_order[$key] = $value['name'];
//		}
//
//		array_multisort($sort_order, SORT_ASC, $json);
//
//		$this->response->addHeader('Content-Type: application/json');
//		$this->response->setOutput(json_encode($json));
//	}
}