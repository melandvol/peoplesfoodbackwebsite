<?php
// Heading
$_['heading_title']     = 'Ajax Product Page Loader';

// Text
$_['text_module']       = 'Modules';
$_['text_success']      = 'Success: You have modified "Ajax Product Page Loader" module!';
$_['text_edit']         = 'Edit "Ajax Product Page Loader" module';

// Entry
$_['loadmore_button_name'] 			= 'Load more';
$_['loadmore_button_name_title'] 	= 'Button name';
$_['loadmore_status_title'] 		= 'Status';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify "Ajax Product Page Loader" module!';