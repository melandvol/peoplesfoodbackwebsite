<?php
class ModelCatalogInformationApp extends Model {
	public function addInformation($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "information_app SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$information_id = $this->db->getLastId();

		foreach ($data['information_description'] as $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "information_app_description SET information_id = '" . (int)$information_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', image = '" . $this->db->escape($value['image']) . "'");
		}

		$this->cache->delete('information_app');

		return $information_id;
	}

	public function editInformation($information_id, $data) {

		$this->db->query("UPDATE " . DB_PREFIX . "information_app SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE information_id = '" . (int)$information_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "information_app_description WHERE information_id = '" . (int)$information_id . "'");
		foreach ($data['information_description'] as $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "information_app_description SET information_id = '" . (int)$information_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', image = '" . $this->db->escape($value['image']) . "'");
		}

		$this->cache->delete('information_app');
	}

	public function deleteInformation($information_id) {

		$this->db->query("DELETE FROM " . DB_PREFIX . "information_app WHERE information_id = '" . (int)$information_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "information_app_description WHERE information_id = '" . (int)$information_id . "'");

		$this->cache->delete('information_app');

	}

	public function getInformation($information_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "information_app WHERE information_id = '" . (int)$information_id . "'");

		return $query->row;
	}

	public function getInformations($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "information_app i LEFT JOIN " . DB_PREFIX . "information_app_description id ON (i.information_id = id.information_id)";

			$sort_data = array(
				'id.title',
				'i.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY id.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$information_data = $this->cache->get('information_app.' . (int)$this->config->get('config_language_id'));

			if (!$information_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information_app i LEFT JOIN " . DB_PREFIX . "information_description_app id ON (i.information_id = id.information_id) ORDER BY id.title");

				$information_data = $query->rows;

				$this->cache->set('information_app.' . (int)$this->config->get('config_language_id'), $information_data);
			}

			return $information_data;
		}
	}

	public function getInformationDescriptions($information_id) {
		$information_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information_app_description WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_description_data[1] = array(
				'title'            => $result['title'],
				'description'      => $result['description'],
				'image'       	   => $result['image']
			);
		}

		return $information_description_data;
	}

	public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "information_app");

		return $query->row['total'];
	}
}