<?php
class ModelCatalogProduct extends Model {
	public function addProduct($data) {
		$this->event->trigger('pre.admin.product.add', $data);

        //valid status
        $data['status'] = isset($data['status']) ? 1 : 0;
        $data['exemplar'] = isset($data['exemplar']) ? 1 : 0;

		$this->db->query("INSERT INTO " . DB_PREFIX . "product SET articul = '" . $this->db->escape($data['articul']) . "', date_available = '" . $this->db->escape($data['date_available']) . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . $this->db->escape($data['weight']) . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', status = '" . (int)$data['status'] . "', single_exemplar = '" . (int)$data['exemplar'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");

		$product_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
		}

		foreach ($data['product_description'] as $language_id => $value) {

			$value['description'] = str_replace('&nbsp;', '', htmlspecialchars_decode($value['description']));

			$value['description'] = str_replace('<br>', '', $value['description']);

			$value['description'] = trim(preg_replace("/ {2,}/"," ",$value['description']));

			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape(trim($value['name'])) . "', description = '" . $this->db->escape($value['description']) . "', tag = '', meta_title = '', meta_description = '', meta_keyword = ''");
		}

		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['product_attribute'])) {
			foreach ($data['product_attribute'] as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
					}
				}
			}
		}

		if (isset($data['product_option'])) {
			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', articul = '" . $this->db->escape($product_option_value['articul']) . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
				}
			}
		}

		if (isset($data['product_ingredients'])) {
			foreach ($data['product_ingredients'] as $product_ingredients) {
				$main_cast = $product_ingredients['main_cast'] ? '1':'0';

				$quantity = $product_ingredients['main_cast'] ? '1':'0';

				$this->db->query("INSERT INTO " . DB_PREFIX . "product_ingredients SET product_id = '" . (int)$product_id . "', ingredients_id = '" . (int)$product_ingredients['ingredients_id'] . "', quantity = '" . $quantity . "', main_cast = '" . $main_cast . "', option_value_my_id = '" . json_encode($product_ingredients['option_value_my_id']) . "'");
				/*
                                $product_ingredients_id = $this->db->getLastId();

                                foreach ($product_ingredients['option_value_my_id'] as $option_value_my_id) {
                                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_ingredients_option_value SET product_id = '" . (int)$product_id . "', option_value_my_id = '" . (int)$option_value_my_id . "',product_ingredients_id = '" . (int)$product_ingredients_id . "'");

                                }*/
			}
		}

		if (isset($data['pizza_option'])) {
			foreach ($data['pizza_option'] as $pizza_option) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_my_option_value SET product_id = '" . (int)$product_id . "', articul = '" . $this->db->escape($pizza_option['articul']) . "', price = '" . (int)$pizza_option['price'] . "', points = '" . (int)$pizza_option['points'] . "', weight = '" . (int)$pizza_option['weight'] . "'");
				$product_my_option_value_id = $this->db->getLastId();

				if (isset($pizza_option['pizza_option_comb_data'])) {
					$pizza_option['option_comb_id'] = $this->getMaxId('option_comb', 'option_comb_id');
					foreach ($pizza_option['pizza_option_comb_data'] as $pizza_option_comb_data) {
						$option_my_id = key($pizza_option_comb_data);
						$option_value_my_id = current($pizza_option_comb_data);

						$this->db->query("INSERT INTO " . DB_PREFIX . "option_comb SET product_id = '" . (int)$product_id . "', option_comb_id = '" . (int)$pizza_option['option_comb_id'] . "', option_my_id = '" . (int)$option_my_id . "', option_value_my_id = '" . (int)$option_value_my_id . "'");
					}
				}
				if(isset($product_my_option_value_id))
					$this->db->query("UPDATE " . DB_PREFIX .  "product_my_option_value SET option_comb_id = '" . (int)$pizza_option['option_comb_id'] . "' WHERE product_my_option_value_id = '" . (int)$product_my_option_value_id . "'");
			}
		}
		//Требуется поправить
		if (isset($data['pizza_option_copy'])) {
			foreach ($data['pizza_option_copy'] as $pizza_option) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_my_option_value SET product_id = '" . (int)$product_id . "', articul = '" . $this->db->escape($pizza_option['articul']) . "', price = '" . (int)$pizza_option['price'] . "', points = '" . (int)$pizza_option['points'] . "', weight = '" . (int)$pizza_option['weight'] . "'");
				$product_my_option_value_id = $this->db->getLastId();

				if (isset($pizza_option['pizza_option_comb_data'])) {
					$pizza_option['option_comb_id'] = $this->getMaxId('option_comb', 'option_comb_id');
					foreach ($pizza_option['pizza_option_comb_data'] as $pizza_option_comb_data) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "option_comb SET option_comb_id = '" . (int)$pizza_option['option_comb_id'] . "', option_my_id = '" . (int)$pizza_option_comb_data['option_my_id'] . "', option_value_my_id = '" . (int)$pizza_option_comb_data['option_value_my_id'] . "'");
					}
				}
				if(isset($product_my_option_value_id))
					$this->db->query("UPDATE " . DB_PREFIX .  "product_my_option_value SET option_comb_id = '" . (int)$pizza_option['option_comb_id'] . "' WHERE product_my_option_value_id = '" . (int)$product_my_option_value_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_payment WHERE product_id = '" . (int)$product_id . "'");

		if(isset($data['only_cash'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_payment SET product_id = '" . (int)$product_id . "', only_cash = '1'");
		}

		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}

		if (isset($data['product_special'])) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}

		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
			}
		}

		if (isset($data['product_download'])) {
			foreach ($data['product_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
			}
		}

		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}
		}

		if (isset($data['product_filter'])) {
			foreach ($data['product_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		if (isset($data['product_related'])) {
			foreach ($data['product_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
			}
		}

		if (isset($data['product_reward'])) {
			foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
				if ((int)$product_reward['points'] > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
				}
			}
		}

		if (isset($data['product_layout'])) {
			foreach ($data['product_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		if (isset($data['product_recurrings'])) {
			foreach ($data['product_recurrings'] as $recurring) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$recurring['customer_group_id'] . ", `recurring_id` = " . (int)$recurring['recurring_id']);
			}
		}

		$this->cache->delete('product');

		$this->event->trigger('post.admin.product.add', $product_id);

		return $product_id;
	}
	public function getMaxId($table, $param) {
		$query = $this->db->query("SELECT MAX(" . $param . ") FROM " . DB_PREFIX . $table);
		$max_id = $query->row['MAX(' . $param . ')'] + 1;
		return empty($max_id) ? 1 : $max_id;
	}
	public function editProduct($product_id, $data) {
		$this->event->trigger('pre.admin.product.edit', $data);

		//valid status
		$data['status'] = isset($data['status']) ? 1 : 0;

		$data['exemplar'] = isset($data['exemplar']) ? 1 : 0;

		$this->db->query("UPDATE " . DB_PREFIX . "product SET articul = '" . $this->db->escape($data['articul']) . "', date_available = '" . $this->db->escape($data['date_available']) . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . $this->db->escape($data['weight']) . "', weight_class_id = '" . (isset($data['weight_class_id']) ? (int)$data['weight_class_id'] : 0) . "', weight_limit = '" . (int)$data['weight_limit'] . "',status = '" . (int)$data['status'] . "', single_exemplar = '" . (int)$data['exemplar'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
		}
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

		foreach ($data['product_description'] as $language_id => $value) {

			$value['description'] = str_replace('&nbsp;', '', htmlspecialchars_decode($value['description']));

			$value['description'] = str_replace('<br>', '', $value['description']);

			$value['description'] = trim(preg_replace("/ {2,}/"," ",$value['description']));

			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape(trim($value['name'])) . "', description = '" . $this->db->escape($value['description']) . "', tag = '', meta_title = '', meta_description = '', meta_keyword = ''");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_payment WHERE product_id = '" . (int)$product_id . "'");

		if(isset($data['only_cash'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_payment SET product_id = '" . (int)$product_id . "', only_cash = '1'");
		}


		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_ingredients WHERE product_id = '" . (int)$product_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_ingredients_option_value WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_ingredients'])) {
			foreach ($data['product_ingredients'] as $product_ingredients) {
				$main_cast = isset($product_ingredients['main_cast']) ? '1':'0';
				
				$quantity = $main_cast;

				$this->db->query("INSERT INTO " . DB_PREFIX . "product_ingredients SET product_id = '" . (int)$product_id . "', ingredients_id = '" . (int)$product_ingredients['ingredients_id'] . "', quantity = '" . $quantity . "', main_cast = '" . $main_cast . "', option_value_my_id = '" . json_encode($product_ingredients['option_value_my_id']) . "'");
/*
				$product_ingredients_id = $this->db->getLastId();

				foreach ($product_ingredients['option_value_my_id'] as $option_value_my_id) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_ingredients_option_value SET product_id = '" . (int)$product_id . "', option_value_my_id = '" . (int)$option_value_my_id . "',product_ingredients_id = '" . (int)$product_ingredients_id . "'");

				}*/
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "option_comb WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_my_option_value WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['pizza_option'])) {
			foreach ($data['pizza_option'] as $pizza_option) {

				$this->db->query("INSERT INTO " . DB_PREFIX . "product_my_option_value SET product_id = '" . (int)$product_id . "', articul = '" . $this->db->escape($pizza_option['articul']) . "', price = '" . (int)$pizza_option['price'] . "', points = '" . (int)$pizza_option['points'] . "', weight = '" . (int)$pizza_option['weight'] . "'");
				$product_my_option_value_id = $this->db->getLastId();

				if (isset($pizza_option['pizza_option_comb_data'])) {
					$pizza_option['option_comb_id'] = $this->getMaxId('option_comb', 'option_comb_id');
						foreach ($pizza_option['pizza_option_comb_data'] as $pizza_option_comb_data) {
							$option_my_id = key($pizza_option_comb_data);
							$option_value_my_id = current($pizza_option_comb_data);

							$this->db->query("INSERT INTO " . DB_PREFIX . "option_comb SET product_id = '" . (int)$product_id . "', option_comb_id = '" . (int)$pizza_option['option_comb_id'] . "', option_my_id = '" . (int)$option_my_id . "', option_value_my_id = '" . (int)$option_value_my_id . "'");
							}
						}
				if(isset($product_my_option_value_id))
					$this->db->query("UPDATE " . DB_PREFIX .  "product_my_option_value SET option_comb_id = '" . (int)$pizza_option['option_comb_id'] . "' WHERE product_my_option_value_id = '" . (int)$product_my_option_value_id . "'");
				}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_special'])) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_download'])) {
			foreach ($data['product_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_filter'])) {
			foreach ($data['product_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");

		if (isset($data['product_related'])) {
			foreach ($data['product_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_reward'])) {
			foreach ($data['product_reward'] as $customer_group_id => $value) {
				if ((int)$value['points'] > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$value['points'] . "'");
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_layout'])) {
			foreach ($data['product_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");

		$this->db->query("DELETE FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = " . (int)$product_id);

		if (isset($data['product_recurring'])) {
			foreach ($data['product_recurring'] as $product_recurring) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$product_recurring['customer_group_id'] . ", `recurring_id` = " . (int)$product_recurring['recurring_id']);
			}
		}

		$this->cache->delete('product');

		$this->event->trigger('post.admin.product.edit', $product_id);
	}

	public function copyProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		if ($query->num_rows) {
			$data = $query->row;

			$data['sku'] = '';
			$data['upc'] = '';
			$data['viewed'] = '0';
			$data['keyword'] = '';
			$data['status'] = '0';

			$data['product_attribute'] = $this->getProductAttributes($product_id);
			$data['product_description'] = $this->getProductDescriptions($product_id);
			$data['product_discount'] = $this->getProductDiscounts($product_id);
			$data['product_filter'] = $this->getProductFilters($product_id);
			$data['product_image'] = $this->getProductImages($product_id);
			$data['product_option'] = $this->getProductOptions($product_id);
			$data['pizza_option_copy'] = $this->getProductMyOptions($product_id);
			$data['product_ingredients'] = $this->getProductIngredients($product_id);
			$data['product_related'] = $this->getProductRelated($product_id);
			$data['product_reward'] = $this->getProductRewards($product_id);
			$data['product_special'] = $this->getProductSpecials($product_id);
			$data['product_category'] = $this->getProductCategories($product_id);
			$data['product_download'] = $this->getProductDownloads($product_id);
			$data['product_layout'] = $this->getProductLayouts($product_id);
			$data['product_store'] = $this->getProductStores($product_id);
			$data['product_recurrings'] = $this->getRecurrings($product_id);

			$this->addProduct($data);
		}
	}

	public function deleteProduct($product_id) {
		$this->event->trigger('pre.admin.product.delete', $product_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int)$product_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "option_comb WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_my_option_value WHERE product_id = '" . (int)$product_id . "'");
		$this->cache->delete('product');

		$this->event->trigger('post.admin.product.delete', $product_id);
	}

	public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "') AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}
	public function getMinimumPrice($product_id) {
		$query = $this->db->query("SELECT MIN(price) as price FROM " . DB_PREFIX . "product_my_option_value WHERE product_id = '" . (int)$product_id . "'");

		return $query->row['price'];
	}

	public function getArticulList($product_id) {
		$query = $this->db->query("SELECT GROUP_CONCAT(DISTINCT articul ORDER BY price ASC SEPARATOR ', ') AS articul FROM " . DB_PREFIX . "product_my_option_value WHERE product_id = '" . (int)$product_id . "'");

		return $query->row['articul'];
	}

	public function getProducts($data = array()) {
		$sql = "SELECT p.*,pd.*,pmov.articul as articul_option FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_my_option_value pmov ON (p.product_id = pmov.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category pc ON (p.product_id = pc.product_id) LEFT JOIN  " . DB_PREFIX . "category_description cd ON (pc.category_id = cd.category_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_category']) && !is_null($data['filter_category'])) {
			$sql .= " AND cd.name LIKE '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (isset($data['filter_articul']) && !empty($data['filter_articul'])) {
			$sql .= " AND p.articul   LIKE '" . $this->db->escape($data['filter_articul']) . "'";
			$sql .= " OR pmov.articul LIKE '" . $this->db->escape($data['filter_articul']) . "'";
		}

		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . (int)$data['filter_quantity'] . "'";
		}

		if (isset($data['filter_status']) &&  !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		$sql .= " GROUP BY p.product_id,p.jan";

		$sort_data = array(
			'pd.name',
			'p.model',
			'p.price',
			'p.quantity',
			'p.status',
			'p.sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY pd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");

		return $query->rows;
	}

	public function getProductDescriptions($product_id) {
		$product_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'tag'              => $result['tag']
			);
		}

		return $product_description_data;
	}

	public function getProductCategories($product_id) {
		$product_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}

	public function getProductFilters($product_id) {
		$product_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_filter_data[] = $result['filter_id'];
		}

		return $product_filter_data;
	}

	public function getProductAttributes($product_id) {
		$product_attribute_data = array();

		$product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' GROUP BY attribute_id");

		foreach ($product_attribute_query->rows as $product_attribute) {
			$product_attribute_description_data = array();

			$product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

			foreach ($product_attribute_description_query->rows as $product_attribute_description) {
				$product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
			}

			$product_attribute_data[] = array(
				'attribute_id'                  => $product_attribute['attribute_id'],
				'product_attribute_description' => $product_attribute_description_data
			);
		}

		return $product_attribute_data;
	}

	public function getProductOptions($product_id) {
		$product_option_data = array();

		$product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();

			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int)$product_option['product_option_id'] . "'");

			foreach ($product_option_value_query->rows as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'articul' 				  => $product_option_value['articul'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'points'                  => $product_option_value['points'],
					'points_prefix'           => $product_option_value['points_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required']
			);
		}

		return $product_option_data;
	}

	public function getProductOptionValue($product_id, $product_option_value_id) {
		$query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getProductMyOptionsTotal($product_id) {
		$pizza_option_total= $this->db->query("
			SELECT 
				COUNT(product_id) as total
			FROM
				" . DB_PREFIX . "product_my_option_value 
				WHERE
			product_id = '" . (int)$product_id . "'");


		return $pizza_option_total->row['total'];
	}

	public function getProductMyOptions($product_id) {
		$pizza_option = array();
			$pizza_option_value_query = $this->db->query("
			SELECT 
				p_v.product_id,
				p_v.option_comb_id,
				p_v.articul,
				ROUND(p_v.price) as price,
				p_v.points,
				ROUND(p_v.weight) as weight
			FROM
				" . DB_PREFIX . "product_my_option_value AS p_v	
			LEFT JOIN  
				" . DB_PREFIX . "option_comb op_c ON (op_c.option_comb_id = p_v.option_comb_id)
			LEFT JOIN
				" . DB_PREFIX . "my_option_value_description AS op_my_v ON op_c.option_value_my_id = op_my_v.option_value_my_id	

			WHERE
				p_v.product_id = '" . (int)$product_id . "'
				AND (op_my_v.option_my_id = '1' 
    			OR op_my_v.option_my_id = '2' )
			ORDER BY op_my_v.sort_order");
		$this->getSortOrder($pizza_option_value_query->rows);

		$pizza_option_value = $pizza_option_value_query->rows;

		foreach ($pizza_option_value as $pizza_option_arr) {
			$pizza_option_comb_query = $this->db->query("
			SELECT 
				p_v.option_comb_id,
				op_my.option_my_id,
    			op_my_v.option_value_my_id,
    			op_my_v.sort_order
			FROM
				" . DB_PREFIX . "product_my_option_value AS p_v
				    LEFT JOIN
				" . DB_PREFIX . "option_comb AS op_c ON p_v.option_comb_id = op_c.option_comb_id
					LEFT JOIN
				" . DB_PREFIX . "my_option AS op_my ON op_c.option_my_id = op_my.option_my_id
					LEFT JOIN
				" . DB_PREFIX . "my_option_value_description AS op_my_v ON op_c.option_value_my_id = op_my_v.option_value_my_id
				WHERE
				p_v.product_id = '" . (int)$pizza_option_arr['product_id'] . "' 
				AND p_v.option_comb_id = '" . (int)$pizza_option_arr['option_comb_id'] . "'");

			$pizza_option_arr['pizza_option_comb_data'] = $pizza_option_comb_query->rows;
			array_push($pizza_option,$pizza_option_arr);
		}


		return $pizza_option;
	}

	public function getProductIngredients($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_ingredients p_i LEFT JOIN " . DB_PREFIX . "ingredients oc_i on(p_i.ingredients_id = oc_i.ingredients_id)  WHERE product_id = '" . (int)$product_id . "' ORDER by p_i.main_cast ASC");

		if($query->num_rows > 0) {
			foreach ($query->rows as $key => $row)
				$query->rows[$key]['option_value_my_id'] = json_decode($row['option_value_my_id']);	
		}
		return $query->rows;
	}

	private function getSortOrder(&$comb_option_array) {
		$sort_array = array();
		foreach ($comb_option_array as $comb_option) {
			$query = $this->db->query("SELECT m_o_v_d.sort_order,oc_op.option_my_id FROM " . DB_PREFIX . "option_comb oc_op LEFT JOIN " . DB_PREFIX . "my_option_value_description as m_o_v_d on (oc_op.option_value_my_id = m_o_v_d.option_value_my_id) WHERE option_comb_id = '" . (int)$comb_option['option_comb_id'] . "' order by oc_op.option_my_id");
			if ($query->num_rows) {
				$sort_order = 0;
				foreach ($query->rows as $option_value_description) {
					if ($option_value_description['option_my_id'] == 1)
						$sort_order = $option_value_description['sort_order'] * 4;
					else
						$sort_order += $option_value_description['sort_order'];
				}
				array_push($sort_array, $sort_order);
			}
		}
		array_multisort($sort_array,$comb_option_array);
	}

	public function getProductCash($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_payment WHERE product_id = '" . (int)$product_id . "'");
		if($query->num_rows > 0)
			return $query->row['only_cash'];
		else 
			return false;
	}

	public function getIngredientsList() {
		$query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "ingredients  ORDER by name ASC");

		return $query->rows;
	}

	public function addIngredients($data) {
		foreach ($data as $ingredients) {
			$sql = "INSERT INTO `" . DB_PREFIX . "ingredients` SET `name` = '" . $this->db->escape($ingredients['name']) . "', `price` = '" . (int)$ingredients['price'] . "', `sort_order` = '" . (int)$ingredients['sort_order'] . "'";
			$this->db->query($sql);
		}
	}
	public function editIngredients($data) {
		foreach ($data as $ingredients) {
			if(!isset($ingredients['ingredients_id'])) {
					$sql = "INSERT INTO `" . DB_PREFIX . "ingredients` SET `name` = '" . $this->db->escape($ingredients['name']) . "', `price` = '" . (int)$ingredients['price'] . "', `weight` = '" . (int)$ingredients['weight'] . "', `articul` = '" . $this->db->escape($ingredients['articul']) . "', `help_string` = '" . $this->db->escape($ingredients['help_string']) . "', `sort_order` = '" . (int)$ingredients['sort_order'] . "'";
					$this->db->query($sql);
			} else {
				$this->db->query("UPDATE " . DB_PREFIX . "ingredients SET `name` = '" . $this->db->escape($ingredients['name']) . "', `price` = '" . (int)$ingredients['price'] . "',`weight` = '" . (int)$ingredients['weight'] . "', `articul` = '" . $this->db->escape($ingredients['articul']) . "', `help_string` = '" . $this->db->escape($ingredients['help_string']) . "', `sort_order` = '" . (int)$ingredients['sort_order'] . "' WHERE ingredients_id = '" . $this->db->escape($ingredients['ingredients_id']) . "'");
				}
			}
	}

	public function deleteIngredients($ingredients_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ingredients WHERE ingredients_id = '" . $this->db->escape($ingredients_id) . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_ingredients WHERE ingredients_id = '" . $this->db->escape($ingredients_id) . "'");

	}

	public function getTotalIngredients() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "ingredients`");

		return $query->row['total'];
	}
	
	public function getOptionMyList($option_my_id) {
		$query = $this->db->query("SELECT name,o_d.option_value_my_id  FROM " . DB_PREFIX . "my_option_value_description  AS o_d  LEFT JOIN " . DB_PREFIX . "my_option_value AS op_v ON op_v.option_value_my_id = o_d.option_value_my_id WHERE o_d.option_my_id = '" . (int)$option_my_id . "' ORDER BY o_d.sort_order ASC");

		return $query->rows;
	}

	public function getProductImages($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getProductDiscounts($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");

		return $query->rows;
	}

	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

		return $query->rows;
	}

	public function getProductRewards($product_id) {
		$product_reward_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
		}

		return $product_reward_data;
	}

	public function getProductDownloads($product_id) {
		$product_download_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_download_data[] = $result['download_id'];
		}

		return $product_download_data;
	}

	public function getProductStores($product_id) {
		$product_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_store_data[] = $result['store_id'];
		}

		return $product_store_data;
	}

	public function getProductLayouts($product_id) {
		$product_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $product_layout_data;
	}

	public function getProductRelated($product_id) {
		$product_related_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_related_data[] = $result['related_id'];
		}

		return $product_related_data;
	}

	public function getRecurrings($product_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = '" . (int)$product_id . "'");

		return $query->rows;
	}
	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category pc ON (p.product_id = pc.product_id) LEFT JOIN  " . DB_PREFIX . "category_description cd ON (pc.category_id = cd.category_id)";

		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (isset($data['filter_category']) && !is_null($data['filter_category'])) {
			$sql .= " AND cd.name LIKE '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalProductsByTaxClassId($tax_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByStockStatusId($stock_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByWeightClassId($weight_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByLengthClassId($length_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByDownloadId($download_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByManufacturerId($manufacturer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByAttributeId($attribute_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByOptionId($option_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByProfileId($recurring_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_recurring WHERE recurring_id = '" . (int)$recurring_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
}
