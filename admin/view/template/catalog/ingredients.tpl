<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button onclick="$('#content #apply').attr('value', '1'); $('#' + $('#content form').attr('id')).submit();" data-toggle="tooltip" title="Применить" class="btn btn-success"><i class="fa fa-save"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="deleteIngredient()"><i class="fa fa-trash-o"></i></button>

      </div>
      <h1>Ингредиенты</h1>

    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Игредиенты </h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $edit; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <?php $ingredients_row = 0;?>
            <table class="table table-bordered table-hover" id="ingredients_table">
              <thead>
              <tr>
                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>                <td class="text-left">Ингредиент</td>
                <td class="text-left">Артикул</td>
                <td class="text-left"><span data-toggle="tooltip" title="Это число отображается в свойствах продукта">Цена</span></td>
                <td class="text-left">Вес</td>
                <td class="text-left">Подсказка</td>
                <td class="text-left"><span data-toggle="tooltip" title="Порядок в котором они отображаются на сайте">Порядок сортировки</span></td>
              </tr>
              </thead>
              <tbody>
              <?php if(isset($ingredients)) { ?>
              <?php foreach($ingredients as $product_ingredients) { ?>
              <tr id ="ingredients_row_tr<?php echo $ingredients_row;?>">
                <td class="text-center">
                  <input type="checkbox" name="selected[]" value="<?php echo $product_ingredients['ingredients_id']?>" />
                </td>
                <?php if(isset($product_ingredients['ingredients_id'])) { ?>
                  <td style="display:none">      <input name="product_ingredients[<?php echo $ingredients_row;?>][ingredients_id]"  value="<?php echo $product_ingredients['ingredients_id'] ?>"> </td>
                <?php } ?>
                <td class="text-left"><input value="<?php echo $product_ingredients['name'] ?>" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][name]"></td>
                <td class="text-left"><input value="<?php echo $product_ingredients['articul'] ?>" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][articul]"></td>
                <td class="text-left"><input value="<?php echo $product_ingredients['price'] ?>" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][price]"></td>
                <td class="text-left"><input value="<?php echo $product_ingredients['weight'] ?>" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][weight]"></td>
                <td class="text-left"><input value="<?php echo $product_ingredients['help_string'] ?>" placeholder="Подсказка" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][help_string]"></td>
                <td class="text-left"><input value="<?php echo $product_ingredients['sort_order'] ?>" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][sort_order]"></td>
                <!--<td class="text-left"><button type="button" onclick="$(this).tooltip('destroy');$('#ingredients_row_tr<?php echo $ingredients_row;?>').remove();" data-toggle="tooltip" title="Удалить" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>-->
              </tr>
              <?php $ingredients_row++;?>
              <?php } ?>
              <?php } ?>
              </tbody>
              <tfoot>
              <tr>
                <td colspan="6"></td>
                <td class="text-right"><button type="button" onclick="addIngredients();" data-toggle="tooltip" title="Добавить" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
              </tfoot>
            </table>
            <div style="display:none;" id="product_ingredients_list">
              <?php if(isset($data['ingredients_list']) && is_array($data['ingredients_list'])) { ?>
                <?php foreach($data['ingredients_list'] as $ingredients_list) { ?>
                  <option value="<?php echo $ingredients_list['ingredients_id'] ?>"><?php echo $ingredients_list['name']?></option>
                <?php } ?>
              <?php } ?>
            </div>
          </div>
          <input type="hidden" name="delete" id="delete" value="0">
          <input type="hidden" name="apply" id="apply" value="0">
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
  var ingredients_row = <?php echo $ingredients_row; ?>;
  function addIngredients() {
    var click = 'onchange=changePrice('+ingredients_row+')';
    var html  = '<tr id="ingredients_row_tr' + ingredients_row + '">';
    html += ' <td class="text-center"><input type="checkbox" name="selected[]" value="" /> </td>';
    html += ' <td class="text-left"><input type="text" placeholder="Название" name="product_ingredients[' + ingredients_row + '][name]"  class="form-control"> </td>';
    html += ' <td class="text-left"><input type="text" placeholder="Артикул" name="product_ingredients[' + ingredients_row + '][articul]"  class="form-control"> </td>';
    html += ' <td class="text-left"><input type="text" placeholder="Цена" name="product_ingredients[' + ingredients_row + '][price]"  class="form-control"> </td>';
    html += ' <td class="text-left"><input type="text" placeholder="Вес" name="product_ingredients[' + ingredients_row + '][weight]"  class="form-control"> </td>';
    html += ' <td class="text-left"><input type="text" placeholder="Подсказка" class="form-control" name="product_ingredients[' + ingredients_row +'][help_string]"></td>';

    html += ' <td class="text-left"><input type="text" value = "0" name="product_ingredients[' + ingredients_row + '][sort_order]"  class="form-control"> </td>';
    html += '</tr>';

    $('#ingredients_table tbody').append(html);
    ingredients_row++;
  }
  function  deleteIngredient() {
    if(confirm('<?php echo $text_confirm; ?>')) {
      $('#content #delete').attr('value', '1');// $('#' + $('#form-product').attr('id')).submit();
      $('#form-product').submit();
    }
    else
      false;
  }
</script>
