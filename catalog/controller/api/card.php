<?php
class ControllerApiCard extends Controller {
	public function getCardInfo()
	{
		if (isset($this->request->get['customer_id'])) {
			$this->load->model('account/customer');
			$this->load->model('catalog/product');
			$this->load->model('tool/image');

			if($this->customer->loginById($this->request->get['customer_id'])) {
				$card_info = $this->customer->getCardInfo();
				if( !$card_info ) {
					$result = $this->model_catalog_product->getProduct(ID_CARD);
					if ($result) {
						$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
						$json['product_id'] = $result['product_id'];
						$json['name'] = $result['name'];
						$json['image'] = $image;
						$json['price'] = (string)round($result['price']);
						$json['special'] = $result['special'] ? (string)round($result['special']) : "";
						$json['single_exemplar'] = (bool)$result['single_exemplar'];
					} else
						$json['error'] = set_error("На данный момент покупка карты невозможна");
				} else
					$json['error'] = set_error("У вас уже есть ID карта");
			}
			else
				$json['error'] = set_error_login("Что то пошло не так, повторите попытку");

			if (isset($json)) {
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
			}
		}

	}	
}
