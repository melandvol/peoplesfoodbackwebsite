<?php
/**
 * Created by PhpStorm.
 * User: kirill_V
 * Date: 09.08.2017
 * Time: 21:20
 */
class  ControllerApiCategories extends Controller {
    public function index(){
        $response['version'] = "1.0";
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $product_info = $this->model_catalog_category->getCategoriesApp();
        $response["categories_list"] = array();
        foreach ($product_info as $product_info) {
            $product = array();
            $name = $this->model_catalog_category->getCategory($product_info['category_id']);
            $product["category"] = $name['name'];
            $product["category_id"] = $product_info["category_id"];
            if ($product_info['image']) {
                $image = $this->model_tool_image->resize($product_info['image'], 300, 300);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 300, 300);
            }
            $product["image"] = empty($image) ? $this->model_tool_image->resize('placeholder.png', 300, 300): $image;
            //Запрос к базе на вывод подкатегории
            $sub_categories_list = $this->model_catalog_category->getCategories($product_info["category_id"]);
            $product["sub_categories_list"] = array();
            //Работа с подкатегориями
            foreach ($sub_categories_list as $sub_categories_list_arr)
            {
                $sub_category = array();
                //$name = $this->model_catalog_category->getCategory($product_info["category_id"]);
                $sub_category["sub_category"] = $sub_categories_list_arr["name"];
                $sub_category["category_id"] = $sub_categories_list_arr["category_id"];
                array_push($product["sub_categories_list"], $sub_category);
            }
            array_push($response["categories_list"], $product);
        }
        $this->response->setOutput(json_encode($response, JSON_UNESCAPED_UNICODE));
    }
    public function productList(){
        $data['version'] = "1.0";
        $data['product_list'] = array();
        if(isset($this->request->get['category_id'])) {
            $category_id = $this->request->get['category_id'];
            $this->load->model('catalog/category');
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $filter_data = array(
                'filter_category_id' => $category_id
            );

            $results = $this->model_catalog_product->getProducts($filter_data);
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }
                $price = round($result['price']);

                $price_min = $this->model_catalog_product->getMinPriceOfProductOption($result['product_id']);

                if($price_min)
                    $price = $price_min;

                $sale = ($result['special'] != 0) ? (string)round(($price - $result['special'])/$price * 100 ): "";

                if($price_min)
                    $price = "От ".round($price);

                $special = ($result['special'] != 0) ? (string)round($result['special'])  : "";

                $points = ($result['points'] != 0) ? (string)$result['points']  : "";
                $data['product_list'][] = array(
                    'product_id'    => (string)$result['product_id'],
                    'image'         => $image,
                    'name'          => str_replace('&quot;', '"', $result['name']),
                    'price'         => (string)$price,
                    'special'       => $special,
                    'sale'          => $sale,
                    'points'        => $points
                );
            };
        }
        $this->response->setOutput(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    public function product(){
        $data['version'] = "1.0";
        if(isset($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
            $this->load->model('catalog/category');
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $result = $this->model_catalog_product->getProduct($product_id);
            //option
            $option = $this->model_catalog_product->getPizzaOption($product_id);
            //ingredients
            $product_ingredients_value = $this->model_catalog_product->getProductIngredients($product_id);
            if( $product_ingredients_value ) {
                $product_ingredients[0]['name'] = "Основной состав";
                $product_ingredients[1]['name'] = "Добавить ингредиенты";

                $product_ingredients[0]['ingredients'] = array();
                $product_ingredients[1]['ingredients'] = array();
                foreach ($product_ingredients_value as $product_ingredient) {
                    if ($product_ingredient['main_cast'] == '1') {
                        array_push($product_ingredients[0]['ingredients'], $product_ingredient);
                    } else if ($product_ingredient['main_cast'] == '0') {
                        array_push($product_ingredients[1]['ingredients'], $product_ingredient);
                    }
                }
            } else
                $product_ingredients = array();

            //$cash_only = $this->model_catalog_product->getProductCash($product_id);

            //price
            $price = empty($option) ? (string)round($result['price']) : "";
            
            $sale = ($result['special'] != 0) ? (string)round(($price - $result['special'])/$price * 100) : "";

            $special = ($result['special'] != 0) ? (string)round($result['special'])  : "";

            $points = ($result['points'] != 0) ? (string)$result['points']  : "";
            //weight
            $weight = $this->currency->formatWeight($result['weight'],$result['weight_class_id']);

            if (!empty($option))
                $weight = $this->currency->formatWeight(0,1);

            $result['description'] = trim(preg_replace("/ {2,}/"," ",$result['description']));
            //image
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 600, 600);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 600, 600);
            }


            $data['product'] = array(
                'product_id'  => (string)$product_id,
                'image'       => $image,
                'product_ingredients'=>$product_ingredients,
                'name'        => str_replace('&quot;', '"', $result['name']),
                'description' => html_entity_decode($result['description']),
                'price'       => $price,
                'points'      => $points,
                'sale'        => $sale,
                'special'     => $special,
                'weight'      => $weight,
                'option'  	  => $option,
                'weight_limit'=> $result['weight_limit']
            );
            if($result['single_exemplar'])
                $data['product']['single_exemplar'] = true;

            /*if($cash_only)
                $data['product']['cash_only'] = true;*/
        }
        if(!$result)
            $data['product'] = array();
        $this->response->setOutput(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
}