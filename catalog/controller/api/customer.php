<?php
class ControllerApiCustomer extends Controller {
	/*
	 *  Login
	 *
	 * */
	public function login()
	{
		if ('POST' === $_SERVER['REQUEST_METHOD']) {
			$entity_body = file_get_contents('php://input');

			$login_data = json_decode($entity_body, true);
			if (!empty($login_data)) {

				$json = array();
				$this->load->model('account/customer');
				$this->language->load('common/quicksignup');

				if (!$this->customer->login($login_data['email'], $login_data['password'])) {
					$json['error'] = set_error($this->language->get('error_login'));
				}
				$customer_info = $this->model_account_customer->getCustomerByEmail($login_data['email']);
				if ($customer_info && !$customer_info['approved']) {
					$json['error'] = set_error($this->language->get('error_approved'));
				}

				if (!$json) {

					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name' => $this->customer->getFirstName(),
						'device' => 1
					);

					$this->model_account_activity->addActivity('login', $activity_data);

					$json['customer_id'] = $this->customer->getId();
				}
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
			}
		}
	}
	/*
	 *  CustomerInfo
	 *
	 * */
	public function getCustomerInfo()
	{
		if(isset($this->request->get['customer_id'])) {

			$customer_id = $this->request->get['customer_id'];
			$customer_info = $this->customer->loginById($customer_id);
			if( $customer_info )
			{
				$json['customer_info'] = array(
					'firstname' 	=> $customer_info['firstname'],
					'email' 		=> $customer_info['email'],
					'telephone' 	=> $customer_info['telephone'],
					'add_telephone' => $customer_info['fax'],
					'newsletter' 	=> $customer_info['newsletter'],
					'newsletter_sms'=> $customer_info['newsletter_sms'],
				);

				if($customer_info['birthday'] != '0000-00-00')
					$json['customer_info']['birthday'] = format_date($customer_info['birthday'],'Y-m-d','j.n.Y');

				$this->load->model('account/customer');
                $this->load->model('account/order');
				$card_info = $this->customer->getCardInfo();

				if( $card_info )
				{
					if($card_info['status'] == 2) {
						$json['customer_info']['card_info'] = array(
							'card' => !empty($card_info['card']) ? $card_info['card'] : "",
							'procent' => $card_info['procent'] ? $card_info['procent'] : "",
							'status' => (int)$card_info['status'],
							'points' => $card_info['points'],
						);
					}
					else
						$json['customer_info']['card_info'] = array(
							'status' 		   => 	(int)$card_info['status']
						);
				} else
					$json['customer_info']['card_info'] = array(
						'status' 		   => 	0
					);

				$this->load->model('account/address');
				$address_info = $this->model_account_address->getAddress( $customer_info['address_id'] );

				if( $address_info )
					$json['customer_info']['address'] = array(
						'address_id' => 	$address_info['address_id'],
						'microdistrict' => 	$address_info['microdistrict'] ? $address_info['microdistrict'] : "",
						'house' => 			$address_info['house'] ? $address_info['house'] : "",
						'street' => 		$address_info['street'] ? $address_info['street'] : "",
						'housing' => 		$address_info['housing'] ? $address_info['housing'] : "",
						'entrance' => 		$address_info['entrance'] ? $address_info['entrance'] : "",
						'code' => 			$address_info['code'] ? $address_info['code'] : "",
						'floor' => 			$address_info['floor'] ? $address_info['floor'] : "",
						'flat' => 			$address_info['flat'] ? $address_info['flat'] : ""
					);
				/*else
					$json['customer_info']['address'] = new stdClass();*/

                $json['customer_info']['order_total'] = (int)$this->model_account_order->getTotalOrdersSuccess();
			}
			else
				$json['error'] = set_error_login("Такого ID не существует");


			if (isset($json)) {
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
			}
		}

	}

	public function setCustomerInfo()
	{
		if ('POST' === $_SERVER['REQUEST_METHOD']) {
			$entity_body = file_get_contents('php://input');

			$customer_info = json_decode($entity_body, true);

			$this->load->model('account/customer');

			$this->model_account_customer->editCustomer($customer_info,$customer_info['customer_id']);
		}

	}

	/*
	 *  Address
	 *
	 * */

	public function getAddresses() {
		if(isset($this->request->get['customer_id'])) {

			if($this->customer->getTotalCustomer($this->request->get['customer_id'])) {
				$this->load->model('account/address');
				$json = array();
				$result = $this->model_account_address->getAddresses();
				foreach ($result as $address_info) {
					$json[] = array(
						'address_id' => 	$address_info['address_id'],
						'default' => 		$address_info['address_id'] == $this->customer->getAddressId() ? true : false,
						'microdistrict' => 	$address_info['microdistrict'] ? $address_info['microdistrict'] : "",
						'house' => 			$address_info['house'] ? $address_info['house'] : "",
						'street' => 		$address_info['street'] ? $address_info['street'] : "",
						'housing' => 		$address_info['housing'] ? $address_info['housing'] : "",
						'entrance' => 		$address_info['entrance'] ? $address_info['entrance'] : "",
						'code' => 			$address_info['code'] ? $address_info['code'] : "",
						'floor' => 			$address_info['floor'] ? $address_info['floor'] : "",
						'flat' => 			$address_info['flat'] ? $address_info['flat'] : ""
					);
				}
			}
			else
				$json['error'] = set_error_login("Такого ID не существует");

			if (isset($json)) {
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
			}
		}
	}

	public function AddAddress()
	{
		// Customer
		if ('POST' === $_SERVER['REQUEST_METHOD']) {
			$entity_body = file_get_contents('php://input');
			$address_info = json_decode($entity_body, true);

			$this->load->model('account/address');

			if($this->customer->getTotalCustomer($address_info['customer_id'])) {
				$address_id = $this->model_account_address->AddAddress($address_info);

				// Add to activity log
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName(),
					'device'      => 1
				);

				$this->model_account_activity->addActivity('address_add', $activity_data);

			}
			else
				$json['error'] = set_error_login("Такого ID не существует");

			$json['address_id'] = $address_id;

			if (isset($json)) {
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
			}

		}

	}
	public function editAddress() {
		// Customer
		if ('POST' === $_SERVER['REQUEST_METHOD']) {
			$entity_body = file_get_contents('php://input');
			$address_info = json_decode($entity_body, true);

			$this->load->model('account/address');

			if($this->customer->getTotalCustomer($address_info['customer_id'])) {
				$this->model_account_address->editAddress($address_info['address_id'], $address_info);

				// Add to activity log
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName(),
					'device'      => 1
				);

				$this->model_account_activity->addActivity('address_edit', $activity_data);

			}
			else
				$json['error'] = set_error_login("Такого ID не существует");

		}
	}
	public function deleteAddress() {
		// Customer
		if ('POST' === $_SERVER['REQUEST_METHOD']) {
			$entity_body = file_get_contents('php://input');
			$address_info = json_decode($entity_body, true);

			$this->load->model('account/address');

			if($this->customer->getTotalCustomer($address_info['customer_id']))
				$this->model_account_address->deleteAddress($address_info['address_id']);
			else
				$json['error'] = set_error_login("Такого ID не существует");

		}
	}

	/*
	 *  Password
	 *
	 * */

	public function editPassword() {
		if ('POST' === $_SERVER['REQUEST_METHOD']) {
			$entity_body = file_get_contents('php://input');

			$customer_info = json_decode($entity_body, true);

			if($this->customer->getTotalCustomer($customer_info['customer_id'])) {

				$this->load->model('account/customer');

				if ($this->model_account_customer->checkPasswordByCustomerId($this->customer->getEmail(), $customer_info['old_password'])) {

					// Add to activity log
					$this->load->model('account/activity');

					$this->model_account_customer->editPassword($this->customer->getEmail(), $customer_info['new_password']);

					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName(),
						'device'      => 1
					);
					$this->model_account_activity->addActivity('password', $activity_data);

				} else
					$json['error'] = set_error("Неверный пароль");
			}
			else
				$json['error'] = set_error_login("Такого ID не существует");

			if (isset($json)) {
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
			}
		}

	}

	public function forgottenPassword() {

		$error = $this->validatePassword();

		if (!$error) {
				$this->load->model('account/customer');
				$this->load->language('mail/forgotten');

				$password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);

				$this->model_account_customer->editPassword($this->request->post['email'], $password);

				$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

				$message  = sprintf($this->language->get('text_greeting'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";
				$message .= $this->language->get('text_password') . "\n\n";
				$message .= $password;

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo($this->request->post['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject($subject);
				$mail->setText($message);
				$mail->send();

		} else
			$json['error'] = set_error($error);

		if (isset($json)) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
		}

	}
	protected function validatePassword() {
		$this->load->model('account/customer');

		$error = '';

		if (!isset($this->request->post['email'])) {
			$error = 'E-Mail адрес не найден, проверьте и попробуйте еще раз!';
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$error = 'E-Mail адрес не найден, проверьте и попробуйте еще раз!';
		}

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
			$error = 'Внимание! Ваш аккаунт еще не активирован.';
		} else {
			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $customer_info['customer_id'],
				'name'        => $customer_info['firstname'],
				'device'      => 1
			);
			$this->model_account_activity->addActivity('forgotten', $activity_data);
		}

		return $error;
	}

	/*
	 *  День рождение
	 *
	 * */

	public function getCustomerBirthday() {

		if(isset($this->request->get['customer_id'])) {

			$customer_id = $this->request->get['customer_id'];

			$customer_info = $this->customer->getTotalCustomer($customer_id);

			if($customer_info)
				$json['birthday'] = $customer_info['birthday'] ? $customer_info['birthday'] : "";
			else
				$json['error'] = set_error_login("Такого ID не существует");


			if (isset($json)) {
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
			}
		}
	}

	/*
	 *  Подпись на новости
	 *
	 * */

	public function SetNewsletter() {
		if (isset($this->request->post['customer_id']) &&
			isset($this->request->post['sms']) &&
			isset($this->request->post['status'])) {

			if($this->customer->getTotalCustomer($this->request->post['customer_id'])) {
				$this->load->model('account/customer');

				if($this->request->post['sms']) {
					$this->model_account_customer->editNewsletterSMS($this->request->post['status']);
				}
				else {
					$this->model_account_customer->editNewsletter($this->request->post['status']);
				}
			}
			else
				$json['error'] = set_error_login("Такого ID не существует");
		}
		if (isset($json)) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
		}

	}

	/*
	 *  Бонусы
	 *
	 * */

	public function getPointsInfo()
	{
		if (isset($this->request->get['customer_id'])) {

			if ($this->customer->getTotalCustomer($this->request->get['customer_id'])) {

				$card_info = $this->customer->getCardInfo();

				if ($card_info) {
					switch ($card_info['status']) {
						case 0: {
							$json['error'] = set_error("Для использования бонусов, пожалуйста приобретите ID карту");
						break;
						}
						case 1: {
							$json['error'] = set_error("Карта находтся в обработка");
							break;
						}
						case 2: {
							if($card_info['points'] != 0)
							{
								$json['pf'] = 5;
								$json['points'] = (int)$card_info['points'];
							} else
								$json['error'] = set_error("У вас нет баллов");
						break;
						}
					}
				} else
					$json['error'] = set_error("Для использования бонусов, пожалуйста приобретите ID карту");
			} else
				$json['error'] = set_error_login("Такого ID не существует");


			if (isset($json)) {
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
			}
		}
	}

    /*
     *  История заказа
     *
     * */
    public function getOrders() {
        if (isset($this->request->get['customer_id']) &&
            isset($this->request->get['start']) &&
            isset($this->request->get['limit']) ) {
            $json = [];

            if ($this->customer->getTotalCustomer($this->request->get['customer_id'])) {

                $this->load->model('account/order');

                $results = $this->model_account_order->getOrdersSuccess($this->request->get['start'], $this->request->get['limit']);
                if( $results ) {
                    foreach ( $results as $result ) {
                        $this->load->model('account/address');
                        $this->model_account_order->ModifiedStatus($result['order_id']);

                        $address_info = $this->model_account_order->getOrderAddress($result['order_id']);

                        if( $address_info ) {
                            $format = '{microdistrict}' . "\n" . ' {house}' . "\n" . '{street}' . "\n" . '{housing}' . "\n" . '{entrance}' . "\n" . '{code} ' . "\n" . '{floor}' . "\n" . '{flat}';

                            $find = [
                                '{microdistrict}',
                                '{house}',
                                '{street}',
                                '{housing}',
                                '{entrance}',
                                '{code}',
                                '{floor}',
                                '{flat}',
                            ];

                            $replace = [
                                'microdistrict' => "<b>Микрорайон: </b>" . $address_info['microdistrict'],
                                'house' => "<b>Дом: </b>" . $address_info['house'],
                                'street' => "<b>Улица: </b>" . $address_info['street'],
                                'housing' => "<b>Корпус: </b>" . $address_info['housing'],
                                'entrance' => "<b>Подъезд: </b>" . $address_info['entrance'],
                                'code' => "<b>Код домофона: </b>" . $address_info['code'],
                                'floor' => "<b>Этаж: </b>" . $address_info['floor'],
                                'flat' => "<b>квартира: </b>" . $address_info['flat']
                            ];

                            foreach ($replace as $key => $delete) {
                                if (!$address_info[$key])
                                    unset($replace[$key]);
                            }

                            //'address_id' => $result['address_id'],
                            $address =  strip_tags(str_replace(array("\r\n", "\r", "\n"), ', ', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), ', ', trim(str_replace($find, $replace, $format)))));


                            $json[] = array(
                                'order_id' => (int)$result['order_id'],
                                'status' => $result['status'],
                                'address' => $address,
                                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                                //'products' => $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']),
                                'total' => (string)(round($result['total'])),
                            );
                        }
                    }
                }
            }
            else {
                $json['error'] = set_error_login();
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
        }
    }
    public function getOrder() {
        if (isset($this->request->get['customer_id']) &&
            isset($this->request->get['order_id']) ) {
            $json = [];

            if ($this->customer->getTotalCustomer($this->request->get['customer_id'])) {

                $this->load->model('account/order');
                $this->load->model('catalog/category');
                $this->load->model('catalog/product');
                $this->load->model('tool/image');

                $products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

                foreach ( $products as $product ) {

                    if($product['quantity'] < 1) $product['quantity'] = 1;

                    $product_in_base = $this->model_catalog_product->getProduct($product['product_id']);
                    if( !$product_in_base ) continue;

                    //option
                    $option = $this->model_account_order->getOrderMyOptions($this->request->get['order_id'], $product['order_product_id'],$product['product_id']);
                    if( $option === -1 ) continue;

                    //ingredients
                    $ingredients = $this->model_account_order->getOrderIngredients($this->request->get['order_id'], $product['order_product_id']);
                    if($ingredients) {
                        $ingredients = json_decode($ingredients, true);
                    }

                    //price
                    $price = $option === 0 ? (string)round($product_in_base['price']) : "";

                    $sale = $product_in_base['special'] != 0 ? (string)round(($price - $product_in_base['special'])/$price * 100) : "";

                    $special = $product_in_base['special'] != 0 ? (string)round($product_in_base['special'])  : "";


                    if ($product_in_base['image']) {
                        $image = $this->model_tool_image->resize($product_in_base['image'], 600, 600);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', 600, 600);
                    }

                    $json[] = [
                        'product_id'       => $product_in_base['product_id'],
                        'image'            => $image,
                        'name'    	 	   => str_replace('&quot;', '"', $product_in_base['name']),
                        'option'   	       => $option === 0 ? null : $option,
                        'ingredients'      => $ingredients ? : null,
                        'price'            => $price,
                        'sale'             => $sale,
                        'special'          => $special,
                        'quantity'		   => $product['quantity'],
                    ];

                    if( $product_in_base['single_exemplar'] ) {
                        $json[count( $json ) - 1 ]['single_exemplar'] = true;
                    }
                }
            }
            else {
                $json['error'] = set_error_login();
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));

        }
    }
}
