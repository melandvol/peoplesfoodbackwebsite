<?php

/**
 * Created by PhpStorm.
 * User: kirill_V
 * Date: 05.01.2018
 * Time: 13:12
 */
class ControllerApiInformation extends Controller {
    public function getContactInfo() {
        $this->addHeader("Контакты");
        $this->getInformation(4);
        $this->addFooter();
    }
    public function getCompanyInfo() {
        $this->addHeader("О компании");
        $this->getInformation(8);
        $this->addFooter();
    }
    public function getConditionsInfo() {
        $this->addHeader("Условия");
        $this->getInformation(9);
        $this->addFooter();
    }
    public function getPrivacyPolicy() {
        $this->addHeader("Privacy Policy");
        $this->getInformation(10);
        $this->addFooter();
    }


    private function getInformation($information_id) {
        $this->load->model('catalog/information');

        $Information = $this->model_catalog_information->getInformation($information_id);
        if(isset($Information['description'])) {
            echo html_entity_decode($Information['description']);
        }
    }

    private function addHeader($caption = "") {
    $header = "
    <!DOCTYPE html>
    <html>
       <head>
          <meta charset=\"utf-8\">
          <title>
             $caption
          </title>
       </head>
       <body>";
        echo $header;
    }

    private function addFooter() {
        $footer = " </body></html>";
        echo $footer;
    }
}