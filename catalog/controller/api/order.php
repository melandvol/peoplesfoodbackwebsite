<?php
class ControllerApiOrder extends Controller
{
	public function add()
	{
		/*ini_set('display_errors', 1);
		error_reporting(E_ALL);*/

		$this->load->language('api/order');

		$json = array();
		// Customer
		if ('POST' === $_SERVER['REQUEST_METHOD']) {
			$entity_body = file_get_contents('php://input');

			$order_data_input = json_decode($entity_body, true);

			//$json['response'] = $tmp['client'];

			$order_data = [];
			if ( $order_data_input ) {
				// customer
				$this->load->model('account/customer');
				// info
				$this->load->model('catalog/information');
				// login
				$this->load->model('account/quicksignup');
				// address
				$this->load->model('account/address');
				// Products
				$this->load->model('catalog/product');

				// Store Details
				$order_data['invoice_prefix'] 	= $this->config->get('config_invoice_prefix');
				$order_data['store_id'] 		= $this->config->get('config_store_id');
				$order_data['store_name'] 		= $this->config->get('config_name');
				$order_data['store_url'] 		= $this->config->get('config_url');

				/*
				 *
				 * Customer Details
				 *
				*/
				
				$login = (isset($order_data_input['email']) && isset($order_data_input['birthday']));

				$order_data['address_id'] = 0;

				// Гость с регистрацией
				if( $login && (int)$order_data_input['customer_id'] === 0 ) {

					// Информация о пользователе, имя фамилия...
					$this->setCustomerInfo($order_data, $order_data_input);
					$order_data['telephone'] = $order_data_input['telephone'];
					$order_data['fax'] 		 = $order_data_input['add_telephone'];

					//Адрес
					$this->setAddress($order_data, $order_data_input);

					$register = $this->register($order_data);

					if (isset($register['error'])) {
						$json['error'] = $register['error'];

					} else if (isset($register['customer_id'])) {
						$order_data['customer_id'] = $register['customer_id'];
						$order_data['address_id'] = $this->customer->getAddressId();
					}
				// Гость без регистрацией
				} else if((int)$order_data_input['customer_id'] === 0 && !isset($order_data_input['email']) && !isset($order_data_input['birthday'])) {

					$order_data['customer_id'] = 0;

					// Информация о пользователе, имя фамилия...
					$this->setCustomerInfo($order_data, $order_data_input);
					$order_data['telephone'] = $order_data_input['telephone'];
					$order_data['fax'] 		 = $order_data_input['add_telephone'];

					//Адрес
					$this->setAddress($order_data, $order_data_input);



				}
				// Пользователь
				else if((int)$order_data_input['customer_id'] !== 0) {

					if($this->customer->getTotalCustomer($order_data_input['customer_id'])) {
						$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

						// Информация о пользователе, имя фамилия...
						$this->setCustomerInfo($order_data,$customer_info);
						$order_data['telephone'] 	= $order_data_input['telephone'];
						$order_data['fax'] 		   	= $order_data_input['add_telephone'];

						$order_data['customer_id'] 	= $this->customer->getId();
						//$order_data['customer_id_second'] 	= $this->customer->getIdSecond();

						
						//Адрес
						if(isset($order_data_input['address_id'])) {
							$address_info = $this->model_account_address->getAddress($order_data_input['address_id']);
							
							$order_data['address_id'] = $order_data_input['address_id'];
							
							if ($address_info) {
								$this->setAddress($order_data, $address_info);
							} else {
                                $this->setAddress($order_data, $order_data_input);
                            }
						} else {
                            $json['error'] = set_error("Выберите адрес");
                        }
					} else {
                        $json['error'] = set_error_login("Такого ID не существует");
                    }
				} else {
                    $json['error'] = set_error_login("Что то пошло не так, повторите попытку");
                }

				// Уведомляем о входе
                if( !isset($json['error']) ) {
                    $login = true;
                }

				/*
				 *
				 * Payment Methods
				 *
				*/

				$this->load->model('payment/cod');

				$payment_method = (int)$order_data_input['payment_method'];

				$order_data['payment_method_second_site'] = $order_data_input['payment_method'];

				$method_data = $this->model_payment_cod->getMethod();

				// 0 -> Оплата картой курьеру
				// 1 -> Оплата наличкой
                if(is_array($method_data)) {
                    foreach ($method_data as $key => $value) {
                        if ($key === $payment_method) {
                            $order_data['payment_method'] = $value['title'];
                            $order_data['payment_code'] = $value['code'];
                        }
                    }
                }


				/*
				 *
				 * Products
				 *
				*/
                $order_data['products'] = [];

				if(isset($order_data_input['products']) && !isset($json['error'])) {

                    if(is_array($order_data_input['products'])) {
                        foreach ($order_data_input['products'] as $product) {

                            $result = $this->model_catalog_product->getProduct($product['product_id']);

                            if ($result) {
                                if ($product['quantity'] < 1) { $product['quantity'] = 1;}

                                if (!isset($product['product_my_option_value_id'])) {
                                    $product['product_my_option_value_id'] = 0;
                                }

                                if ($product['product_my_option_value_id'] !== 0) {

                                    $new_param_products = $this->model_catalog_product->getNewParamProducts($product['product_id'], $product['product_my_option_value_id']);

                                    if ($new_param_products) {
                                        $result['price'] = $new_param_products['price'];
                                        $result['articul'] = $new_param_products['articul'];
                                    } else {
                                        $json['error'] = set_error("Что то пошло не так, повторите попытку");
                                    }
                                }

                                $ingredients_price = 0;

                                $ingredients = array();

                                $cash_only = $this->model_catalog_product->getProductCash($product['product_id']);

                                if ($cash_only && $payment_method === 0) {
                                    $json['error'] = set_error("У вас есть товар, у которого оплата производится только за наличный расчёт");
                                }
                                if (isset($product['ingredients'])) {
                                    $ingredients_price = $this->currency->priceIngredients($product['ingredients']);
                                    $ingredients = $product['ingredients'];
                                }

                                if (!isset($json['error']) && $product['product_id'] == ID_CARD) {
                                    if ($login) {
                                        $status = (int)$this->model_account_customer->checkStatusCardID($this->customer->getId());
                                        if ( !$status ) {
                                            $this->model_account_customer->setStatusCardID($this->customer->getId(), 1);
                                        }
                                        else {
                                            if ( $status === 1 ) {
                                                $json['error'] = set_error("У вас уже приобретена ID карта");
                                            }
                                            if ( $status === 2 ) {
                                                $json['error'] = set_error("У вас уже приобретена ID карта");
                                            }
                                        }
                                    } else {
                                        $json['error'] = set_error("Покупать ID карту могут только авторизованые пользователи");
                                    }
                                }

                                $order_data['products'][] = array(
                                    'product_id' => $product['product_id'],
                                    'articul' => $result['articul'],
                                    'name' => $result['name'],
                                    'quantity' => $product['quantity'],
                                    'price' => $result['price'],
                                    'total' => round(($result['price'] + $ingredients_price) * $product['quantity']),
                                    'ingredients' => $ingredients,
                                    'product_my_option_value_id' => $product['product_my_option_value_id']
                                );
                            } else {
                                $json['error'] = set_error("Что то пошло не так, повторите попытку");
                            }
                        }
                    }
				}

				/*
				 *
				 *  Gift Voucher
				 *
				*/
				if(!isset($json['error'])) {
					
				$order_data['vouchers'] = array();

				if (isset($order_data_input['vouchers'])) {
					foreach ($order_data_input as $voucher) {
						$order_data['vouchers'][] = array(
							'description' => $voucher['description'],
							'code' => substr(token(32), 0, 10),
							'to_name' => $voucher['to_name'],
							'to_email' => $voucher['to_email'],
							'from_name' => $voucher['from_name'],
							'from_email' => $voucher['from_email'],
							'voucher_theme_id' => $voucher['voucher_theme_id'],
							'message' => $voucher['message'],
							'amount' => $voucher['amount']
						);
					}
				}

				/*
				 *
				 *  Order Totals
				 *
				*/

				$this->load->model('extension/extension');

				$order_data['totals'] = array();

				//$total = $this->cart->getTotalApp($order_data['products']);

					$total = 0;

					$taxes = array();

					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');
                    if (is_array($results) && !empty($results)) {
                        foreach ($results as $key => $value) {
                            if ((string)$value['code'] === 'points') {
                                $sort_order[$key] = 3;
                            } else {
                                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                            }
                        }
                        array_multisort($sort_order, SORT_ASC, $results);

                        foreach ($results as $result) {
                            if ((string)$result['code'] === 'points' || $this->config->get($result['code'] . '_status')) {

                                if ((string)$result['code'] !== 'points') {
                                    $this->load->model('total/' . $result['code']);
                                }

                                if ((string)$result['code'] === 'points' && isset($order_data_input['points'])) {
                                    $this->{'model_total_sub_total'}->getTotalPoints($order_data['totals'], $total, $order_data_input['points']);
                                } else if ((string)$result['code'] === 'sub_total') {
                                    $this->{'model_total_sub_total'}->getTotalApp($order_data['totals'], $total, $order_data['products']);
                                } else if ((string)$result['code'] !== 'points') {
                                    $this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
                                }
                            }
                        }
                    }
					$sort_order = array();

					foreach ($order_data['totals'] as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $order_data['totals']);

					$order_data['total'] = $total;

					//var_dump($order_data['totals']);
					$order_data['comment'] = $order_data_input['comment'];

					//ненужное
					$order_data['affiliate_id'] = 0;
					$order_data['commission'] = 0;
					$order_data['marketing_id'] = 0;
					$order_data['tracking'] = '';
					$order_data['shipping_country_id'] = 0;
					$order_data['shipping_zone_id'] = 0;
					$order_data['shipping_method'] = '';
					$order_data['shipping_code'] = '';
					$order_data['forwarded_ip'] = '';

					/*
                     *
                     * Add Total price
                     *
                     * */

					$order_data['accrual_points'] = 0;

					$order_data['procent'] = 0;

					$order_data['points'] = 0;

					if ( $login ) {
						$card_info = $this->customer->getCardInfo();
						if ($card_info && $this->customer->isCard()) {

							$total = 0;
							foreach ($order_data['totals'] as $totals) {
								if($totals['code'] == 'sub_total') {
                                    $total = $totals['value'];
                                }
							}

							$order_data['accrual_points'] = round(($total * $card_info['procent']) / 100);

							if(isset($order_data_input['points']))
								$order_data['points'] = $order_data_input['points'];

							$total_points = $card_info['points'] + $order_data['accrual_points'] - $order_data['points'];

							if($total_points < 0) $total_points = 0;

							$this->model_account_customer->updateCardInfoPoints($this->customer->getId(), $total_points);

							$card_total = $card_info['total_price'] + $total;

							if ($card_total > 3000 && $card_total < 10000 && (int)$card_info['procent'] !== 15) {
                                    $this->model_account_customer->updateCardInfoPercent($this->customer->getId(), 15);

							} else if ($card_total > 10000 && (int)$card_info['procent'] !== 20) {
                                    $this->model_account_customer->updateCardInfoPercent($this->customer->getId(), 20);
							}

							$order_data['procent'] = $card_info['procent'];

							$this->model_account_customer->updateCardInfoTotal($this->customer->getId(), $card_total);
						}
					}

					/*
                     *
                     *  Service info
                     *
                    */
					$order_data['ip'] = $this->request->server['REMOTE_ADDR'];
					$order_data['accept_language'] = '';

					$order_data['language_id'] = $this->config->get('config_language_id');
					$order_data['currency_id'] = $this->currency->getId();
					$order_data['currency_code'] = $this->currency->getCode();
					$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());

					$order_data['user_agent'] = "Android PFApp";

					$order_data['device'] = 1; // ANDROID

					$this->load->model('checkout/order');

					if( $total !== 0 ) {
                        $json['order_id'] = $this->model_checkout_order->addOrder($order_data);
                        if ( $login ) {
                            $json['customer_id'] = $this->customer->getId();
                        }
                    }
					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' 	=> $this->customer->getId(),
						'name' 			=> $order_data['firstname'],
						'device'		=> 1
					);

					$this->model_account_activity->addActivity('order_account', $activity_data);
				}
				// Новый заказ
				$order_status_id = '1';

				// Set the order history
				//$this->model_checkout_order->addOrderHistory($json['order_id'], $order_status_id);
			} else
				$json['error'] = set_error("Ошибка заказа");
		} else
			$json['error'] = set_error("Пустой массив");

		if (isset($json)) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
		}
	}
	public function check()
	{
		if ('POST' === $_SERVER['REQUEST_METHOD']) {

			$entity_body = file_get_contents('php://input');
			$request = json_decode($entity_body, true);

			/*
			 *
			 * Time ZONE
			 *
			 * */

			$time = $this->currency->validTimeZone();

			if($time) {
                $json['error'] = set_error($time);
            }

			/*
			 *
			 * Total price
			 *
			 * */

			if( !isset($json['error']) ) {
				$price = $this->currency->validPrice($request['price_total']);

				if($price) {
                $json['error'] = set_error($price);
            	}
			}

			/*
			 *
			 * 1/2 Пицца
			 *
			 * */

			if( $request['products'] ) {
                if (!isset($json['error'])) {
                    $option_value_my_id_arr = [];
                    foreach ($request['products'] as $key => $product) {
                        $option_value_my_id = $this->db->query("SELECT  o_c.option_value_my_id FROM " . DB_PREFIX . "product_my_option_value p_m_o_v LEFT JOIN  " . DB_PREFIX . "option_comb o_c ON (o_c.option_comb_id = p_m_o_v.option_comb_id) WHERE p_m_o_v.product_id = '" . (int)$product['product_id'] . "' and p_m_o_v.product_my_option_value_id = '" . $product['product_my_option_value_id'] . "'");
                        if ($option_value_my_id->num_rows > 0) {
                            $tmp_array = array();
                            foreach ($option_value_my_id->rows as $arr) {
                                array_push($tmp_array, current($arr));
                            }
                            $option_value_my_id_arr[$key] = $tmp_array;
                        } else
                            $json['error'] = set_error("Попробуйте заказать через некоторое время");
                    }
                    $count = 0;
                    $error = false;
                    $check_arr = array();

                    if (!isset($json['error'])) {
                        if (count($option_value_my_id_arr) === 1) {
                            $first = array_shift($option_value_my_id_arr);
                            $check_half = false;
                            foreach ($first as $check) {
                                if ($this->checkParam($check)) {
                                    $check_half = true;
                                    break;
                                }
                            }
                            if ($check_half)
                                $count = $request['products'][0]['quantity'];
                        } else
                            foreach ($option_value_my_id_arr as $cart_id => $option_value_my_id) {

                                if ($error) break;
                                //Проверка на одинкаовые массивы
                                $bContinue = false;
                                foreach ($check_arr as $arr)
                                    if ($cart_id == $arr) {
                                        $bContinue = true;
                                        break;
                                    }
                                if ($bContinue) continue;

                                $check_half = false;
                                foreach ($option_value_my_id as $check) {
                                    if ($this->checkParam($check)) {
                                        $check_half = true;
                                        break;
                                    }
                                }
                                if ($check_half) {
                                    $iterator = 0;
                                    foreach ($option_value_my_id_arr as $cart_id_2 => $option_value_my_id_2) {
                                        $iterator++;
                                        if ($cart_id != $cart_id_2) {
                                            if (empty(array_diff($option_value_my_id, $option_value_my_id_2))) {
                                                $count += $request['products'][$cart_id]['quantity'] + $request['products'][$cart_id_2]['quantity'];
                                                array_push($check_arr, $cart_id, $cart_id_2);
                                                break;
                                            } else if ($iterator == count($option_value_my_id_arr)) {
                                                if ($request['products'][$cart_id]['quantity'] % 2 != 0) $error = true;

                                            }
                                        }
                                    }
                                }
                            }
                        if ($count % 2 != 0) {
                            $error = true;
                        }

                        if ($error) {
                            $json['error'] = set_error("Ошибка! Выберите пожалуйста 2 половинки одинакого размера и на одном тесте");
                        }
                    }
                }
            }

		}


		if(isset($json)) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
		}

	}


	private function checkParam($item){
		if(($item >= 11 && $item <= 13) || ($item >= 15 && $item <= 17))
			return true;
		else
			return false;
	}
	private function register($register_data) {

            $json = array();
			if (!empty($register_data)) {

				$this->language->load('common/quicksignup');
				$this->language->load('account/success');

				if ((utf8_strlen(trim($register_data['firstname'])) < 1) || (utf8_strlen(trim($register_data['firstname'])) > 32)) {
					$json['error'] = set_error($this->language->get('error_name'));
				}

				if ((utf8_strlen($register_data['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $register_data['email'])) {
					$json['error'] = set_error($this->language->get('error_email'));
				}

				if ($this->model_account_customer->getTotalCustomersByEmail($register_data['email'])) {
					$json['error'] = set_error($this->language->get('error_exists'));
				}

				if ((utf8_strlen($register_data['telephone']) < 3) || (utf8_strlen($register_data['telephone']) > 32)) {
					$json['error'] = set_error($this->language->get('error_telephone'));
				}

				if (!$this->currency->validDate($register_data['birthday'], 'Y-m-d')) {
					$json['error'] = set_error("Ошибка указания дня рождения");
				}
			}

			if ( !$json ) {

				$json['customer_id'] = $this->model_account_customer->addCustomer($register_data,true);

				if($this->customer->getTotalCustomer($json['customer_id'])) {

					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' 	=> $this->customer->getId(),
						'name' 			=> $register_data['firstname'],
						'device'		=> 1
					);

					$this->model_account_activity->addActivity('register', $activity_data);
				} else
					$json['error'] = 'Ошибка регистрации';

			}
			return $json;
		}
	private function setAddress(&$data_output,$data_input) {
		$data_output['microdistrict'] 	= $data_input['microdistrict'];
		$data_output['house'] 			= $data_input['house'];
		$data_output['street'] 			= $data_input['street'];
		$data_output['housing'] 		= $data_input['housing'];
		$data_output['entrance'] 		= $data_input['entrance']; //подъезд
		$data_output['code'] 			= $data_input['code'];
		$data_output['floor'] 			= $data_input['floor'];
		$data_output['flat']			= $data_input['flat'];
	}
	private function setCustomerInfo(&$data_output,$data_input) {
		//Стандартная
		$data_output['customer_group_id']	= 1;

		$data_output['firstname'] 			= $data_input['firstname'];
		if(isset($data_input['email'] ))
			$data_output['email'] 			= $data_input['email'];
		if(isset($data_input['birthday'] ) && $this->currency->validDate($data_input['birthday'], 'j.n.Y'))
			$data_output['birthday'] 		= format_date($data_input['birthday'],'j.n.Y','Y-m-d');
	}
}