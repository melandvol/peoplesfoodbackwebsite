<?php

/**
 * Created by PhpStorm.
 * User: kirill_V
 * Date: 10.12.2017
 * Time: 19:50
 */
class  ControllerApiStock extends Controller {
    public function index() {
        $json['version'] = "1.0";
        $this->load->model('catalog/information');
        $this->load->model('tool/image');
        $actions = $this->model_catalog_information->getInformationsApp();
        $json["stock"] = array();
        foreach ($actions as $action) {
            if ($action['image']) {
                $image = $this->model_tool_image->resize($action['image'], 600, 600);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 600, 600);
            }
            $json["stock"][] = array(
                'information_id'  => $action['information_id'],
                'image'       => $image,
                'description' => $action['description'],
                'title'       => $action['title']
            );
        }
        $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
    }

}