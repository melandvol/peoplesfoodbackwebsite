<?php
class ControllerModuleFaq extends Controller
{
    private $error = array();

    public function index()
    {
        $limit = 15;
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $this->load->model('module/faq');
        $this->load->language('module/faq');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $this->document->setTitle($this->language->get('meta_title'));
        $this->document->setDescription($this->language->get('meta_desc'));
        $this->document->setKeywords($this->language->get('meta_keyw'));
        $this->document->addLink($this->url->link('module/faq'),'');
        $this->document->addScript('catalog/view/javascript/collapse.js');

        $data['heading_title'] = $this->language->get('heading_title');
        $data['questions_empty'] = $this->language->get('questions_empty');
        $data['answer_empty'] = $this->language->get('answer_empty');

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('faq/faq')
        );
        $data['answers'] = $this->model_module_faq->getAnswer((($page - 1) * $limit),$limit);

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->load->model('module/faq');

        $create = $this->model_module_faq->getIpDate();
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($create)) {
            $this->model_module_faq->addFaq($this->request->post);


            $this->session->data['success'] = $this->language->get('text_success');

        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = 'В ближайшее время администратор ответит на ваш вопрос';

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['action'] = $this->url->link('module/faq');
        /* Pagination */
        $answer_total= $this->model_module_faq->getTotalAnswer();
        $pagination = new Pagination();
        $pagination->total = $answer_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('module/faq', '&page={page}');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($answer_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($answer_total - $limit)) ? $answer_total : ((($page - 1) * $limit) + $limit), $answer_total, ceil($answer_total / $limit));

        $this->response->setOutput($this->load->view('default/template/module/faq.tpl', $data));
    }
    protected function validateForm($create) {
        foreach ($this->request->post['faq_question'] as $language_id => $value) {
            if ((utf8_strlen($value['title']) < 3)) {
                $this->error['warning'] = 'Ошибка! Слишком короткий вопрос';
            }
            if (utf8_strlen($value['title']) > 255)
                $this->error['warning'] = 'Ошибка! Слишком длинный вопрос';
            }

            if ($this->error && !isset($this->error['warning'])) {
                $this->error['warning'] = 'Ошибка';
            }

        // Проверка количеста заданных вопросов

        foreach ($create as $tmpArr) {

            if ($this->request->server['REMOTE_ADDR'] == $tmpArr['ip'] && strtotime('-10 minutes') < (strtotime($tmpArr['create']))) {

                $leftTime = (10 - ((strtotime('now') - (strtotime($tmpArr['create']))) / 60 % 60));
                if (5 <= $leftTime && $leftTime <= 10) $minutes = 'минут';
                if (2 <= $leftTime && $leftTime <= 4) $minutes = 'минуты';
                if (1 == $leftTime) $minutes = 'минута';

                $this->error['warning'] = 'Задайте вопрос через ' . $leftTime . ' ' . $minutes;
            }
        }
        $this->getForm();

        return !$this->error;
    }
    protected function getForm() {

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
    }
}