<?php

/**
 * Created by PhpStorm.
 * User: kirill_V
 * Date: 25.03.2018
 * Time: 15:18
 */
class ControllerToolLogApp  extends Controller {
    public function index() {
    }
    public function linkGooglePlay() {
        $this->load->model('account/activity');
        $customer = $this->customer->getId();
        $activity_data = array(
            'customer_id' 	=> $customer,
            'name' 			=> $customer == 0 ? "Гость" : $this->customer->GetFirstName(),
        );
        $this->model_account_activity->addActivity('load_app', $activity_data);

        $this->response->redirect('https://play.google.com/store/apps/details?id=ru.peoplesfood&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1');
    }
}