<?php
class ControllerTotalPointsMob extends Controller {
    public function index() {
        if ($this->config->get('coupon_status')) {
            $this->load->language('total/coupon');

            $data['heading_title'] = "Расплатиться пифами";

            $data['text_loading'] = $this->language->get('text_loading');

            $data['entry_coupon'] = "Введите количетво пифов";

            $data['button_coupon'] = "Рассчитать";

            if (isset($this->session->data['points'])) {
                $data['points'] = $this->session->data['points'];
            } else {
                $data['points'] = '';
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/total/points_mob.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/total/points_mob.tpl', $data);
            } else {
                return $this->load->view('default/template/total/points_mob.tpl', $data);
            }
        }
    }

    public function points() {
        $this->load->language('total/coupon');

        $json = array();

        $this->load->model('total/coupon');

        if (isset($this->request->post['points_mob'])) {
            $points = $this->request->post['points_mob'];
        } else {
            $points = '';
        }


        if (isset($this->session->data['points'])) {
            $points_old = $this->session->data['points'];
        } else {
            $points_old = 0;
        }

        $price_old = round($points_old/ PIFF, 0 ,PHP_ROUND_HALF_DOWN );

        if(round($points/ PIFF, 0 ,PHP_ROUND_HALF_DOWN  ) >= $this->request->post['price_total'] + $price_old)
            $json['error'] ="Количество пифов не должно превышать цену";

        if (empty($this->request->post['points_mob']) || $this->request->post['points_mob'] == 0) {

            unset($this->session->data['points']);

            $json['redirect'] = $this->url->link('checkout/cart');
        
        } elseif($this->customer->isLogged()) {
            
            $card_info = $this->customer->getCardInfo();
            
            if ($card_info && $this->customer->isCard()) {
                if( $card_info['status'] == 2 ) {
                    if ($points > $card_info['points'])
                        $json['error'] = "У вас не хватает пифов";

                    if (!isset($json['error']) && $points % PIFF != 0)
                        $json['error'] = "Количество пифов не кратно " . PIFF;

                    if (!isset($json['error'])) {
                        $this->session->data['points'] = $this->request->post['points_mob'];

                        $this->session->data['success'] = "Баллы успешно применены";

                        $json['redirect'] = $this->url->link('checkout/cart');
                    }
                } else
                    $json['error'] ="Ваша карта находится в обработке";
            } else
                $json['error'] ="Для покупки за пифы приобретите ID карту";
        } else {
            $json['error'] = "Для использования пифов вам необходимо войти или зарегестироваться";
            
            unset($this->session->data['points']);
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
