<?php
// Text
$_['text_signin_register']    = 'Sign In/Register';

$_['text_new_customer']       = 'New Customer';
$_['text_returning']          = 'Returning Customer';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'Name';
$_['entry_password']          = 'Password';
$_['entry_telephone']         = 'Telephone';
$_['text_forgotten']          = 'Forgotten Password';
$_['text_agree']              = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'Login';

//Error
$_['error_name']           = 'Имя должно быть от 1 до 32 символов!';
$_['error_email']          = 'Адрес электронной почты не действителен!';
$_['error_telephone']      = 'Телефон должен быть от 3 до 32 символов!';
$_['error_password']       = 'Пароль должен быть от 4 до 20 символов!';
$_['error_exists']         = 'Внимание: E-Mail уже зарегистрирован!';
$_['error_agree']          = 'Внимание: вы должны согласиться с% s!';
$_['error_warning']        = 'Внимание! Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_approved']       = 'Предупреждение. Перед входом в систему ваша учетная запись требует одобрения.';
$_['error_login']          = 'Ошибка ввода логина или пароля.';