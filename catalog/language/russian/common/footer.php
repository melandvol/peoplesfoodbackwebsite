<?php
// Text
$_['text_information']  = 'Описание';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';
$_['text_bonus']   = 'Бонусная программа';
$_['text_present']   = 'Подарочные сертификаты';
$_['text_how_order']   = 'Как заказать';
$_['text_map']   = 'Карта доставки';
$_['text_contact']   = 'Контакты';

