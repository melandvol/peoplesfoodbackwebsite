<?php
class ModelAccountAddress extends Model {
	public function addAddress($data) {
		$this->event->trigger('pre.customer.add.address', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$this->customer->getId() . "', microdistrict = '" . $this->db->escape($data['microdistrict']) . "', house = '" . (int)$data['house'] . "' , street = '" . $this->db->escape($data['street']) . "', housing = '" . $this->db->escape($data['housing']) . "',floor = '" . (int)$data['floor'] . "', flat = '" . (int)$data['flat'] . "', code = '" . $this->db->escape($data['code']) . "', entrance = '" . (int)$data['entrance'] . "'");

		$address_id = $this->db->getLastId();

		if($this->customer->getAddressId() == 0) {
			$this->db->query("UPDATE peoples_users SET address_id = '" . (int)$address_id . "' WHERE id = '" . (int)$this->customer->getId() . "'");
		}

		if (!empty($data['default'])) {
			$this->db->query("UPDATE peoples_users SET address_id = '" . (int)$address_id . "' WHERE id = '" . (int)$this->customer->getId() . "'");
		}

		$this->event->trigger('post.customer.add.address', $address_id);

		return $address_id;
	}

	public function editAddress($address_id, $data) {
		$this->event->trigger('pre.customer.edit.address', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "address SET microdistrict = '" . $this->db->escape($data['microdistrict']) . "', house = '" . $this->db->escape($data['house']) . "' , street = '" . $this->db->escape($data['street']) . "', housing = '" . $this->db->escape($data['housing']) . "', entrance = '" . (int)$data['entrance'] . "', code = '" . $this->db->escape($data['code']) . "', floor = '" . (int)$data['floor'] . "', flat = '" . (int)$data['flat'] . "' WHERE address_id  = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if (!empty($data['default'])) {
			$this->db->query("UPDATE peoples_users SET address_id = '" . (int)$address_id . "' WHERE id = '" . (int)$this->customer->getId() . "'");
		}

		$this->event->trigger('post.customer.edit.address', $address_id);
	}

	public function deleteAddress($address_id) {
		$this->event->trigger('pre.customer.delete.address', $address_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		$this->event->trigger('post.customer.delete.address', $address_id);
	}

	public function getAddress($address_id) {
		$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if ($address_query->num_rows) {
			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'microdistrict'  => $address_query->row['microdistrict'],
				'house'       	 => $address_query->row['house'],
				'street'         => $address_query->row['street'],
				'housing'      	 => $address_query->row['housing'],
				'entrance'       => $address_query->row['entrance'],
				'code'       	 => $address_query->row['code'],
				'floor'          => $address_query->row['floor'],
				'flat'       	 => $address_query->row['flat'],
				//Не нужное
				'firstname'      => $address_query->row['firstname'],
				'lastname'       => $address_query->row['lastname'],
				'company'        => $address_query->row['company'],
				'address_1'      => $address_query->row['address_1'],
				'address_2'      => $address_query->row['address_2'],
				'postcode'       => $address_query->row['postcode'],
				'city'           => $address_query->row['city'],
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => '',
				'zone_code'      => '',
				'country_id'     => $address_query->row['country_id'],
				'country'        => '',
				'iso_code_2'     => '',
				'iso_code_3'     => '',
				'address_format' => '',
				'custom_field'   => ''
			);

			return $address_data;
		} else {
			return false;
		}
	}

	public function getAddresses() {
		$address_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		foreach ($query->rows as $result) {
			$address_data[$result['address_id']] = array(
				'address_id'     => $result['address_id'],
				'microdistrict'  => $result['microdistrict'],
				'house'       	 => $result['house'],
				'street'         => $result['street'],
				'housing'      	 => $result['housing'],
				'entrance'       => $result['entrance'],
				'code'       	 => $result['code'],
				'floor'          => $result['floor'],
				'flat'       	 => $result['flat'],
				//Не нужное
				'firstname'      => $result['firstname'],
				'lastname'       => $result['lastname'],
				'company'        => $result['company'],
				'address_1'      => $result['address_1'],
				'address_2'      => $result['address_2'],
				'postcode'       => $result['postcode'],
				'city'           => $result['city'],
				'zone_id'        => $result['zone_id'],
				'zone'           => "",
				'zone_code'      => "",
				'country_id'     => $result['country_id'],
				'country'        => "",
				'iso_code_2'     => "",
				'iso_code_3'     => "",
				'address_format' => "",
				'custom_field'   => json_decode($result['custom_field'], true)

			);
		}

		return $address_data;
	}

	public function getTotalAddresses() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
}