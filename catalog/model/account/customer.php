<?php
class ModelAccountCustomer extends Model {
	public function addCustomer($data,$app = false) {
		$this->event->trigger('pre.customer.add', $data);

		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$salt = token(9);

		$password = $app ? substr(sha1(uniqid(mt_rand(), true)), 0, 10) : $data['password'];

		if (!$app) {
			$data['birthday'] = format_date($data['birthday'], 'd.m.Y', 'Y-m-d');
		}

		$this->db->query("INSERT INTO peoples_users SET fio = '" . $this->db->escape($data['firstname']) . "',email = '" . $this->db->escape($data['email']) . "', phone = '" . $this->db->escape($data['telephone']) . "', phone_dop = '" . $this->db->escape($data['fax']) . "',date_birthday = '" . $data['birthday'] . "', pass = '" . $password . "', rss_email = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 1) . "', rss_sms = '" . (isset($data['newsletter_sms']) ? (int)$data['newsletter_sms'] : 1) . "', status = '1', date_added = NOW()");

		//$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "',email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "',birthday = '" . $data['birthday'] . "', salt = '" . $this->db->escape($salt) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 1) . "', newsletter_sms = '" . (isset($data['newsletter_sms']) ? (int)$data['newsletter_sms'] : 1) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW()");

		$customer_id = $this->db->getLastId();

		if(isset($data['house'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', microdistrict = '" . $this->db->escape($data['microdistrict']) . "', house = '" . (int)$data['house'] . "' , street = '" . $this->db->escape($data['street']) . "', housing = '" . $this->db->escape($data['housing']) . "', entrance = '" . (int)$data['entrance'] . "', code = '" . $this->db->escape($data['code']) . "', floor = '" . (int)$data['floor'] . "', flat = '" . (int)$data['flat'] . "'");

			$address_id = $this->db->getLastId();

			$this->db->query("UPDATE peoples_users SET address_id = '" . (int)$address_id . "' WHERE id = '" . (int)$customer_id . "'");

			//$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		}
		$this->load->language('mail/customer');

		$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

		if( $app ) {
			$message .= sprintf($this->language->get('text_login_app'), $password) . "\n";
		} else if (!$customer_group_info['approval']) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}

		$message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();

		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_mail_alert'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		$this->event->trigger('post.customer.add', $customer_id);

		return $customer_id;
	}

	public function editCustomer($data,$customer_id = 0) {
		$this->event->trigger('pre.customer.edit', $data);

		$customer_id = $customer_id == 0 ? $this->customer->getId() : $customer_id;

		$this->db->query("UPDATE peoples_users SET fio = '" . $this->db->escape($data['firstname']) . "', email = '" . $this->db->escape($data['email']) . "', phone = '" . $this->db->escape($data['telephone']) . "', phone_dop = '" . $this->db->escape($data['fax']) . "' WHERE id = '" . (int)$customer_id . "'");

		$this->event->trigger('post.customer.edit', $customer_id);
	}

	public function editPassword($email, $password) {
		$this->event->trigger('pre.customer.edit.password');

		//$this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		$this->db->query("UPDATE peoples_users SET pass = '" . $this->db->escape($password) . "' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");


		$this->event->trigger('post.customer.edit.password');
	}
	public function checkPasswordByCustomerId($email, $password) {

		/*$salt_query = $this->db->query("SELECT salt  FROM ". DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		if($salt_query->num_rows > 0 ) {
			$count =$this->db->query("SELECT COUNT(customer_id) as count FROM " . DB_PREFIX . "customer WHERE password = '" . $this->db->escape(sha1($salt_query->row['salt'] . sha1($salt_query->row['salt'] . sha1($password)))) . "' AND LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
			if( $count->row['count'] > 0 )
				return true;
			else
				return false;
		}
		else
			return false;*/
			$count = $this->db->query("SELECT COUNT(id) as count FROM peoples_users WHERE pass = '" . $password . "' AND LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
			if( $count->row['count'] > 0 )
				return true;
			else
				return false;
	}

	public function checkCard($customer_id) {
		$check_card = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "' AND id_cart IS NULL");

		if($check_card->num_rows > 0)
			return true;
		else
			return false;
	}

	public function editNewsletter($newsletter) {
		$this->event->trigger('pre.customer.edit.newsletter');

		//$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		$this->db->query("UPDATE peoples_users SET rss_email = '" . (int)$newsletter . "' WHERE id = '" . (int)$this->customer->getId() . "'");

		$this->event->trigger('post.customer.edit.newsletter');
	}

	public function editNewsletterSMS($newsletter_sms) {
		
		//$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter_sms = '" . (int)$newsletter_sms . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		$this->db->query("UPDATE peoples_users SET rss_sms = '" . (int)$newsletter_sms . "' WHERE id = '" . (int)$this->customer->getId() . "'");


	}

	public function getCustomer($customer_id) {
		$customer_query = $this->db->query("SELECT * FROM peoples_users WHERE id = '" . (int)$customer_id . "'");

		if($customer_query->rows > 0) {

			$query_return = array(
			'customer_id'			=> $customer_query->row['id'],
			'firstname'				=>  $customer_query->row['fio'],
			'customer_group_id'		=> 0,
			'email'					=> $customer_query->row['email'],
			'telephone'				=> $customer_query->row['phone'],
			'fax'					=> $customer_query->row['phone_dop'],
			'newsletter'			=> $customer_query->row['rss_email'],
			'newsletter_sms'		=> $customer_query->row['rss_sms'],
			'address_id'			=> $customer_query->row['address_id'],
			'birthday' 	 	 		=> $customer_query->row['date_birthday'],
			'custom_field' 	 	 	=> '',
			);
			return $query_return;
		}

	}

	public function getCustomerByEmail($email) {
		$customer_query = $this->db->query("SELECT * FROM peoples_users  WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		if($customer_query->num_rows > 0) {

			$query_return = array(
				'customer_id'			=> $customer_query->row['id'],
				'firstname'				=>  $customer_query->row['fio'],
				'customer_group_id'		=> 0,
				'email'					=> $customer_query->row['email'],
				'telephone'				=> $customer_query->row['phone'],
				'fax'					=> $customer_query->row['phone_dop'],
				'newsletter'			=> $customer_query->row['rss_email'],
				'newsletter_sms'		=> $customer_query->row['rss_sms'],
				'address_id'			=> $customer_query->row['address_id'],
				'birthday' 	 	 		=> $customer_query->row['date_birthday'],
				'custom_field' 	 	 	=> '',
				'approved' 	 	 		=> 1,
			);
			return $query_return;
		}
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");

		return $query->row;
	}

	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM peoples_users WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}

	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}

	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}

	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
	public function getCardInfo($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_card` WHERE customer_id = '" . (int)$customer_id . "'");

		if($query->num_rows > 0)
			return $query->row;
		else
			return false;
	}
	public function checkStatusCardID($customer_id) {
		$query = $this->db->query("SELECT status_cart_id FROM peoples_users WHERE id = '" . (int)$customer_id . "'");
		if($query->num_rows > 0){
			if($query->row['status_cart_id'] == 0)
				return false;
			else
				return $query->row['status_cart_id'];
		}
		else
			return false;
		}
	
	public function setStatusCardID($customer_id,$status) {
		$query = $this->db->query("SELECT status_cart_id FROM peoples_users WHERE id = '" . (int)$customer_id . "'");

		if($query->num_rows > 0) {
			$this->db->query("UPDATE peoples_users SET status_cart_id = '" . (int)$status . "' WHERE id = '" . (int)$customer_id . "'");
		}
	}
	public function updateCardInfoPoints($customer_id,$points) {
	$peoples_users = $this->db->query("SELECT id_cart FROM peoples_users WHERE id = '" . (int)$customer_id . "'");
		if($peoples_users->num_rows > 0) {
			$this->db->query("UPDATE peoples_id_cart_balls SET balls = '" . (int)$points . "' WHERE id_cart = '" . (int)$peoples_users->row['id_cart'] . "'");
		}
	}
	
	public function updateCardInfoTotal($customer_id,$total_price) {
		//$this->db->query("UPDATE " . DB_PREFIX . "customer_card SET total_price = '" . (int)$total_price . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}
	
	public function updateCardInfoPercent($customer_id,$procent) {
		//$this->db->query("UPDATE " . DB_PREFIX . "customer_card SET procent = '" . (int)$procent . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}
}
