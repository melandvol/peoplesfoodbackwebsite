<?php
class ModelCatalogProduct extends Model {
	public function updateViewed($product_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = (viewed + 1) WHERE product_id = '" . (int)$product_id . "'");
	}
	//КАСТОМ
	public function getWeightDescription($weight_class_id) {
		$query = $this->db->query("SELECT title,unit FROM " . DB_PREFIX . "weight_class_description WHERE weight_class_id = '" . (int)$weight_class_id . "'");

		if($query->num_rows > 0)
			return $query->row;
		else
			return false;
	}
	/*public function getProductIngredients($product_id) {
		$query = $this->db->query("SELECT p_i.ingredients_id, p_i.quantity,i.price,p_i.main_cast,i.name FROM " . DB_PREFIX . "product_ingredients AS p_i LEFT JOIN	" . DB_PREFIX . "ingredients AS i ON (p_i.ingredients_id = i.ingredients_id) WHERE product_id = '" . (int)$product_id . "'");
		if($query->num_rows > 0) {
			return $query->rows;
		}
		else
			return false;
	}*/
	public function getProductIngredients($product_id) {
		$query = $this->db->query("SELECT p_i.ingredients_id, p_i.quantity,p_i.option_value_my_id, i.price,p_i.main_cast,i.name,i.weight FROM " . DB_PREFIX . "product_ingredients p_i LEFT JOIN " . DB_PREFIX . "ingredients i on(p_i.ingredients_id = i.ingredients_id)  WHERE product_id = '" . (int)$product_id . "' ORDER by p_i.main_cast ASC");

		if($query->num_rows > 0) {
			foreach ($query->rows as $key => $row)
				$query->rows[$key]['option_value_my_id'] = json_decode($row['option_value_my_id']);

			return $query->rows;
		}
		else
			return false;
	}

	public function getPizzaOption($product_id) {
		//Get Size
		$size_value = array();
		$size_query = $this->db->query("SELECT m_v_d.name , o_c.option_comb_id,  o_c.option_value_my_id FROM " . DB_PREFIX . "option_comb o_c LEFT JOIN " . DB_PREFIX . "my_option_value_description m_v_d ON (m_v_d.option_value_my_id = o_c.option_value_my_id) WHERE o_c.product_id = '" . (int)$product_id . "' AND o_c.option_my_id = 1 order by m_v_d.sort_order");
		$this->getSortOrder($size_query->rows);
		if($size_query->num_rows > 0) {
			foreach ($size_query->rows as $size_array){
				//Get dough
				$dough_query = $this->db->query("SELECT p_m_o.product_my_option_value_id, m_v_d.name , ROUND(p_m_o.price) as price , p_m_o.points, ROUND(p_m_o.weight) as weight FROM " . DB_PREFIX . "option_comb o_c LEFT JOIN  " . DB_PREFIX . "my_option_value_description m_v_d ON (m_v_d.option_value_my_id = o_c.option_value_my_id) LEFT JOIN  " . DB_PREFIX . "product_my_option_value p_m_o ON (p_m_o.option_comb_id = o_c.option_comb_id)  WHERE o_c.product_id = '" . (int)$product_id . "' AND o_c.option_my_id = 3 AND o_c.option_comb_id = '" . (int)$size_array['option_comb_id'] . "'");
				if($dough_query->num_rows > 0) {
					$dough_query->row['weight'] = 'Вес: '.$dough_query->row['weight'].'г.';
					$find = false;
					foreach ($size_value as $key => $size_value_tmp) {
						if ($size_value_tmp['size'] == $size_array['name']) {
							array_push($size_value[$key]['dough_arr'], $dough_query->row);
							$find = true;
							break;
						}
					}
					if(!$find) {
						$tmp['size'] = $size_array['name'];
						$tmp['option_value_my_id'] = $size_array['option_value_my_id'];
						//$tmp['dough_arr'] = $dough_query->row;
						$tmp['dough_arr'] = array();
						array_push($tmp['dough_arr'], $dough_query->row);
						array_push($size_value, $tmp);
					}
				}
			}
		} else {
			$size_query = $this->db->query("SELECT m_v_d.name , o_c.option_comb_id,  o_c.option_value_my_id FROM " . DB_PREFIX . "option_comb o_c LEFT JOIN " . DB_PREFIX . "my_option_value_description m_v_d ON (m_v_d.option_value_my_id = o_c.option_value_my_id) WHERE o_c.product_id = '" . (int)$product_id . "' AND o_c.option_my_id = 2 order by m_v_d.sort_order");
			if($size_query->num_rows > 0) {
				foreach ($size_query->rows as $size_array) {
					//Get dough
					$dough_query = $this->db->query("SELECT p_m_o.product_my_option_value_id, m_v_d.name , ROUND(p_m_o.price) as price , p_m_o.points, ROUND(p_m_o.weight) as weight FROM " . DB_PREFIX . "option_comb o_c LEFT JOIN  " . DB_PREFIX . "my_option_value_description m_v_d ON (m_v_d.option_value_my_id = o_c.option_value_my_id) LEFT JOIN  " . DB_PREFIX . "product_my_option_value p_m_o ON (p_m_o.option_comb_id = o_c.option_comb_id)  WHERE o_c.product_id = '" . (int)$product_id . "' AND o_c.option_my_id = 2 AND o_c.option_comb_id = '" . (int)$size_array['option_comb_id'] . "'");
					if($dough_query->num_rows > 0) {
						$dough_query->row['name'] = '';
						$dough_query->row['weight'] = 'Вес: '.$dough_query->row['weight'].'г.';
						$find = false;
						foreach ($size_value as $key => $size_value_tmp) {
							if ($size_value_tmp['size'] == $size_array['name']) {
								array_push($size_value[$key]['dough_arr'], $dough_query->row);
								$find = true;
								break;
							}
						}
						if(!$find) {
							$tmp['size'] = $size_array['name'];
							//$tmp['dough_arr'] = $dough_query->row;
							$tmp['dough_arr'] = array();
							array_push($tmp['dough_arr'], $dough_query->row);
							array_push($size_value, $tmp);
						}
					}
				}
			}
		}
		return $size_value;
	}



	public function getMyOption($product_id) {
		$query = $this->db->query("SELECT  oc_p.product_my_option_value_id,
										oc_p.option_comb_id,
										oc_p.articul,
										ROUND(oc_p.price) as price,
										oc_p.points,
										ROUND(oc_p.weight) as weight
										
									FROM
										" . DB_PREFIX . "product_my_option_value AS oc_p 
									WHERE product_id = '" . (int)$product_id . "'");
		if($query->num_rows > 0) {
			$this->getSortOrder($query->rows);

			$option_comb = array();
			foreach ($query->rows as $option_comb_id) {
				$option_comb_query = $this->db->query("
				SELECT 
					 my_o.type, disc.name, oc_o.option_my_id , oc_o.option_value_my_id
				FROM
					" . DB_PREFIX . "option_comb AS oc_o
						LEFT JOIN
					" . DB_PREFIX . "my_option_value_description AS disc ON (oc_o.option_my_id = disc.option_my_id  AND oc_o.option_value_my_id = disc.option_value_my_id)
						LEFT JOIN
					" . DB_PREFIX . "my_option AS my_o ON (oc_o.option_my_id = my_o.option_my_id)
				WHERE
					oc_o.product_id = '" . (int)$product_id . "' AND oc_o.option_comb_id = '" . (int)$option_comb_id['option_comb_id'] . "' ORDER BY oc_o.option_my_id");

				if ($option_comb_query->num_rows > 0) {
					$option_comb_id['option_comb'] = $option_comb_query->rows;
					array_push($option_comb, $option_comb_id);
				}
			}
			return $option_comb;
		}
		else false;
	}
	public function getMyOptionArr($product_id) {
		$query = $this->db->query("SELECT oc_op.option_my_id, oc_my.type, oc_op.option_comb_id , oc_op.option_value_my_id , m_o_v_d.name FROM " . DB_PREFIX . "option_comb as oc_op LEFT JOIN " . DB_PREFIX . "my_option as oc_my on (oc_op.option_my_id = oc_my.option_my_id) LEFT JOIN " . DB_PREFIX . "my_option_value_description as m_o_v_d on (oc_op.option_value_my_id = m_o_v_d.option_value_my_id) WHERE product_id = '" . (int)$product_id . "' AND (oc_op.option_my_id = 1 || oc_op.option_my_id = 2) order by oc_op.option_my_id , m_o_v_d.sort_order");

		if($query->num_rows > 0) {
			$this->getSortOrder($query->rows);

			$result = array();
			$find = false;
			$result[$query->rows[0]['type']] = array();

			foreach ($query->rows as $size_all) {
				foreach ($result[$query->rows[0]['type']] as $size) {
					if($size['option_value_my_id'] == $size_all['option_value_my_id']) {
						$find = true;
						break;
					} else
						$find = false;
				}

				if(!$find) {
					array_push($result[$query->rows[0]['type']],$size_all);

				}
			}


			/*
			 *
			 * Выделяем только то тесто которое принадлежит 1 размеру
			 *
			 * */

			$dough_query = $this->db->query("SELECT t2.option_comb_id, t2.option_value_my_id,t2.option_my_id, t2.type, t2.name FROM( SELECT o_c.option_comb_id FROM oc_option_comb o_c WHERE o_c.product_id = '".(int)$product_id."' AND o_c.option_value_my_id = '".(int)$query->rows[0]['option_value_my_id']."') AS t1 INNER JOIN (SELECT o_c.option_value_my_id ,o_c.option_my_id,m_o_v_d.name, oc_my.type, o_c.option_comb_id FROM " . DB_PREFIX . "option_comb o_c  LEFT JOIN " . DB_PREFIX . "my_option as oc_my on (o_c.option_my_id = oc_my.option_my_id) LEFT JOIN " . DB_PREFIX . "my_option_value_description as m_o_v_d on (o_c.option_value_my_id = m_o_v_d.option_value_my_id) WHERE o_c.product_id = '".(int)$product_id."' AND o_c.option_my_id = 3 ) AS t2 ON t1.option_comb_id = t2.option_comb_id");
			if( $dough_query->num_rows > 0) {
				$this->getSortOrder($dough_query->rows);
				$result[$dough_query->rows[0]['type']] = $dough_query->rows;
			}

			$product_my_option_value = $this->db->query("SELECT DISTINCT oc_p.price, oc_p.points, oc_p.weight ,oc_p.product_my_option_value_id FROM " . DB_PREFIX . "product_my_option_value AS oc_p WHERE oc_p.product_id = '" . (int)$product_id . "' AND oc_p.option_comb_id = '" . (int)$query->rows[0]['option_comb_id'] . "' ");
			if ($product_my_option_value->num_rows > 0) {
				$result['product'] = $product_my_option_value->row;
				$result['product']['option_value_my_id'] =(int)$query->rows[0]['option_value_my_id'];
			}

			return $result;
		}
		else false;
	}
	private function getSortOrder(&$comb_option_array) {
		$sort_array = array();
		foreach ($comb_option_array as $comb_option) {
			$query = $this->db->query("SELECT m_o_v_d.sort_order,oc_op.option_my_id FROM " . DB_PREFIX . "option_comb oc_op LEFT JOIN " . DB_PREFIX . "my_option_value_description as m_o_v_d on (oc_op.option_value_my_id = m_o_v_d.option_value_my_id) WHERE option_comb_id = '" . (int)$comb_option['option_comb_id'] . "' order by oc_op.option_my_id");
			if ($query->num_rows) {
				$sort_order = 0;
				foreach ($query->rows as $option_value_description) {
					if ($option_value_description['option_my_id'] == 1)
						$sort_order = $option_value_description['sort_order'] * 4;
					else
						$sort_order += $option_value_description['sort_order'];
				}
				array_push($sort_array, $sort_order);
			}
		}
		array_multisort($sort_array,$comb_option_array);
	}

	//ГОВНОКОД
	public function getMyOptionArrOld($product_id) {
		//Получаем опции и тип
		$query = $this->db->query("SELECT DISTINCT oc_op.option_my_id, oc_my.type FROM " . DB_PREFIX . "option_comb as oc_op LEFT JOIN " . DB_PREFIX . "my_option as oc_my on (oc_op.option_my_id = oc_my.option_my_id) WHERE product_id = '" . (int)$product_id . "' AND oc_my.type <> 'dough' order by oc_op.option_my_id");
		if($query->num_rows > 0) {
			//$this->getSortOrder($query->rows);
			$result = array();
			foreach ($query->rows as $option_my_id) {
				$option_comb_query = $this->db->query("SELECT DISTINCT disc.name, oc_o.option_my_id , oc_o.option_value_my_id FROM " . DB_PREFIX . "option_comb AS oc_o LEFT JOIN " . DB_PREFIX . "my_option_value_description AS disc ON (oc_o.option_my_id = disc.option_my_id AND oc_o.option_value_my_id = disc.option_value_my_id) LEFT JOIN " . DB_PREFIX . "my_option_value AS my_o ON (oc_o.option_my_id = my_o.option_my_id) WHERE oc_o.product_id = '" . (int)$product_id . "' AND disc.option_my_id = '" . (int)$option_my_id['option_my_id'] . "' ORDER BY disc.sort_order");
				if ($option_comb_query->num_rows > 0) {
					$result[$option_my_id['type']] = $option_comb_query->rows;
					if ($option_my_id['type'] == 'size_it') {
						//Ищем существующий тип теста для первого продукта
						$option_dough_comb = $this->db->query("SELECT DISTINCT oc_o.option_comb_id FROM " . DB_PREFIX . "option_comb AS oc_o WHERE oc_o.product_id = '" . (int)$product_id . "' AND oc_o.option_my_id = '" . (int)$option_comb_query->rows[0]['option_my_id'] . "' AND oc_o.option_value_my_id = '" . (int)$option_comb_query->rows[0]['option_value_my_id'] . "' ");
						if($option_dough_comb->num_rows > 0) {
							$result['dough'] = array();
							foreach ($option_dough_comb->rows as $dough_comb) {
								$option_dough = $this->db->query("SELECT DISTINCT disc.name, oc_o.option_my_id , oc_o.option_value_my_id FROM " . DB_PREFIX . "option_comb AS oc_o LEFT JOIN " . DB_PREFIX . "my_option_value_description AS disc ON (oc_o.option_my_id = disc.option_my_id AND oc_o.option_value_my_id = disc.option_value_my_id) LEFT JOIN " . DB_PREFIX . "my_option_value AS my_o ON (oc_o.option_my_id = my_o.option_my_id) WHERE oc_o.product_id = '" . (int)$product_id . "' AND oc_o.option_my_id = 3 AND oc_o.option_comb_id = '" . (int)$dough_comb['option_comb_id'] . "' ORDER BY disc.sort_order");
								if ($option_dough->num_rows > 0) {
									array_push($result['dough'],$option_dough->row);
								}
							}
						}


						//Ищем цену и вес для первого продукта с связаными опциями
						$product_my_option = $this->db->query("SELECT t1.option_comb_id	FROM (SELECT oc_o.option_comb_id FROM oc_option_comb AS oc_o WHERE oc_o.product_id = '" . (int)$product_id . "' AND oc_o.option_my_id = '" . (int)$option_comb_query->rows[0]['option_my_id'] . "' AND oc_o.option_value_my_id = '" . (int)$option_comb_query->rows[0]['option_value_my_id'] . "') AS t1 INNER JOIN (SELECT oc_o2.option_comb_id FROM oc_option_comb AS oc_o2 WHERE oc_o2.product_id = '" . (int)$product_id . "' AND oc_o2.option_my_id = '" . (int)$result['dough'][0]['option_my_id'] . "' AND oc_o2.option_value_my_id = '" . (int)$result['dough'][0]['option_value_my_id'] . "') AS t2 ON t1.option_comb_id = t2.option_comb_id ");
						if($product_my_option->num_rows == 1) {
							$product_my_option_value = $this->db->query("SELECT DISTINCT oc_p.price, oc_p.points, oc_p.weight ,oc_p.product_my_option_value_id FROM " . DB_PREFIX . "product_my_option_value AS oc_p WHERE oc_p.product_id = '" . (int)$product_id . "' AND oc_p.option_comb_id = '" . (int)$product_my_option->row['option_comb_id'] . "' ");
							if ($product_my_option_value->num_rows == 1) {
								$result['product'] = $product_my_option_value->row;
							}
						}
					}
					else {
						//Ищем цену и вес для первого продукта без связаных опций
						$product_my_option = $this->db->query("SELECT oc_o.option_comb_id FROM oc_option_comb AS oc_o WHERE oc_o.product_id = '" . (int)$product_id . "' AND oc_o.option_my_id = '" . (int)$option_comb_query->rows[0]['option_my_id'] . "' AND oc_o.option_value_my_id = '" . (int)$option_comb_query->rows[0]['option_value_my_id'] . "'");
						if ($product_my_option->num_rows == 1) {
							$product_my_option_value = $this->db->query("SELECT DISTINCT oc_p.price, oc_p.points, oc_p.weight, oc_p.product_my_option_value_id FROM " . DB_PREFIX . "product_my_option_value AS oc_p WHERE oc_p.product_id = '" . (int)$product_id . "' AND oc_p.option_comb_id = '" . (int)$product_my_option->row['option_comb_id'] . "' ");
							if ($product_my_option_value->num_rows == 1) {
								$result['product'] = $product_my_option_value->row;
							}
						}
					}

				}

			}
			return $result;
		}
		else false;
	}
	public function getMyProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return array(
				'product_id'       => $query->row['product_id'],
				'image'            => $query->row['image'],
				'name'             => $query->row['name'],
				'description'      => $query->row['description'],
				'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'special'          => $query->row['special'],
				'minimum'          => $query->row['minimum'],
			);
		} else {
			return false;
		}
	}
	public function getMinPriceOfProductOption($product_id) {
		$query = $this->db->query("SELECT MIN(ROUND(oc_p.price)) as price
										
									FROM
										" . DB_PREFIX . "product_my_option_value AS oc_p 
									WHERE product_id = '" . (int)$product_id . "'");
		if($query->num_rows > 0) {
			return $query->row['price'];
		}
		else
			return false;
	}
	//КАСТОМ
	public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return array(
				'product_id'       => $query->row['product_id'],
				'articul'          => $query->row['articul'],
				'name'             => $query->row['name'],
				'description'      => $query->row['description'],
				'meta_title'       => $query->row['meta_title'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'special'          => $query->row['special'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating']),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed'],
				'single_exemplar'  => $query->row['single_exemplar'],
				'weight_limit'     => $query->row['weight_limit']
			);
		} else {
			return false;
		}
	}
	public function getProducts($data = array()) {
		$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}
			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";
		if (!empty($data['filter_category_id'])) {
			if($data['filter_category_id']!=65)
				if (!empty($data['filter_sub_category'])) {
					$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
				} else {
					$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
				}
			if (!empty($data['filter_filter'])) {
				$implode = array();
				$filters = explode(',', $data['filter_filter']);
				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}
				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
			}
		}
		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";
			if (!empty($data['filter_name'])) {
				$implode = array();
				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));
				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}
				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}
				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}
			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}
			if (!empty($data['filter_tag'])) {
				$sql .= "pd.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%'";
			}
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}
			$sql .= ")";
		}
		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}
		$sql .= " GROUP BY p.product_id";
		$sort_data = array(
			'pd.name',
			'p.model',
			'p.quantity',
			'p.price',
			'rating',
			'p.sort_order',
			'p.date_added'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
				$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
			} elseif ($data['sort'] == 'p.price') {
				$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			$sql .= " ORDER BY p.sort_order";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC, LCASE(pd.name) DESC";
		} else {
			$sql .= " ASC, LCASE(pd.name) ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$product_data = array();
		$query = $this->db->query($sql);
		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}
		return $product_data;
	}
	public function getProductSpecials($data = array()) {
		$sql = "SELECT DISTINCT ps.product_id, (SELECT AVG(rating) FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = ps.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id";
		$sort_data = array(
			'pd.name',
			'p.model',
			'ps.price',
			'rating',
			'p.sort_order'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
				$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			$sql .= " ORDER BY p.sort_order";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC, LCASE(pd.name) DESC";
		} else {
			$sql .= " ASC, LCASE(pd.name) ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$product_data = array();
		$query = $this->db->query($sql);
		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}
		return $product_data;
	}
	public function getLatestProducts($limit) {
		$product_data = $this->cache->get('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit);
		if (!$product_data) {
			$query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.date_added DESC LIMIT " . (int)$limit);
			foreach ($query->rows as $result) {
				$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
			}
			$this->cache->set('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit, $product_data);
		}
		return $product_data;
	}
	public function getPopularProducts($limit) {
		$product_data = array();
		$query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.viewed DESC, p.date_added DESC LIMIT " . (int)$limit);
		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}
		return $product_data;
	}
	public function getBestSellerProducts($limit) {
		$product_data = $this->cache->get('product.bestseller.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit);
		if (!$product_data) {
			$product_data = array();
			$query = $this->db->query("SELECT op.product_id, SUM(op.quantity) AS total FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE o.order_status_id > '0' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' GROUP BY op.product_id ORDER BY total DESC LIMIT " . (int)$limit);
			foreach ($query->rows as $result) {
				$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
			}
			$this->cache->set('product.bestseller.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit, $product_data);
		}
		return $product_data;
	}
	public function getProductAttributes($product_id) {
		$product_attribute_group_data = array();
		$product_attribute_group_query = $this->db->query("SELECT ag.attribute_group_id, agd.name FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE pa.product_id = '" . (int)$product_id . "' AND agd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, agd.name");
		foreach ($product_attribute_group_query->rows as $product_attribute_group) {
			$product_attribute_data = array();
			$product_attribute_query = $this->db->query("SELECT a.attribute_id, ad.name, pa.text FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE pa.product_id = '" . (int)$product_id . "' AND a.attribute_group_id = '" . (int)$product_attribute_group['attribute_group_id'] . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pa.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY a.sort_order, ad.name");
			foreach ($product_attribute_query->rows as $product_attribute) {
				$product_attribute_data[] = array(
					'attribute_id' => $product_attribute['attribute_id'],
					'name'         => $product_attribute['name'],
					'text'         => $product_attribute['text']
				);
			}
			$product_attribute_group_data[] = array(
				'attribute_group_id' => $product_attribute_group['attribute_group_id'],
				'name'               => $product_attribute_group['name'],
				'attribute'          => $product_attribute_data
			);
		}
		return $product_attribute_group_data;
	}
	public function getProductOptions($product_id) {
		$product_option_data = array();
		$product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");
		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();
			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order");
			foreach ($product_option_value_query->rows as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'name'                    => $product_option_value['name'],
					'image'                   => $product_option_value['image'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']
				);
			}
			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required']
			);
		}
		return $product_option_data;
	}
	public function getProductDiscounts($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity > 1 AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");
		return $query->rows;
	}
	public function getProductImages($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");
		return $query->rows;
	}
	public function getProductRelated($product_id) {
		$product_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pr.product_id = '" . (int)$product_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
		foreach ($query->rows as $result) {
			$product_data[$result['related_id']] = $this->getProduct($result['related_id']);
		}
		return $product_data;
	}
	public function getProductLayoutId($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");
		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}
	public function getCategories($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		return $query->rows;
	}
	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total";
		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}
			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";
		if (!empty($data['filter_category_id'])) {
			if($data['filter_category_id']!=65)
				if (!empty($data['filter_sub_category'])) {
					$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
				} else {
					$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
				}
			if (!empty($data['filter_filter'])) {
				$implode = array();
				$filters = explode(',', $data['filter_filter']);
				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}
				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
			}
		}
		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";
			if (!empty($data['filter_name'])) {
				$implode = array();
				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));
				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}
				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}
				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}
			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}
			if (!empty($data['filter_tag'])) {
				$sql .= "pd.tag LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_tag'])) . "%'";
			}
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}
			$sql .= ")";
		}
		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getProfile($product_id, $recurring_id) {
		return $this->db->query("SELECT * FROM `" . DB_PREFIX . "recurring` `p` JOIN `" . DB_PREFIX . "product_recurring` `pp` ON `pp`.`recurring_id` = `p`.`recurring_id` AND `pp`.`product_id` = " . (int)$product_id . " WHERE `pp`.`recurring_id` = " . (int)$recurring_id . " AND `status` = 1 AND `pp`.`customer_group_id` = " . (int)$this->config->get('config_customer_group_id'))->row;
	}
	public function getProfiles($product_id) {
		return $this->db->query("SELECT `pd`.* FROM `" . DB_PREFIX . "product_recurring` `pp` JOIN `" . DB_PREFIX . "recurring_description` `pd` ON `pd`.`language_id` = " . (int)$this->config->get('config_language_id') . " AND `pd`.`recurring_id` = `pp`.`recurring_id` JOIN `" . DB_PREFIX . "recurring` `p` ON `p`.`recurring_id` = `pd`.`recurring_id` WHERE `product_id` = " . (int)$product_id . " AND `status` = 1 AND `customer_group_id` = " . (int)$this->config->get('config_customer_group_id') . " ORDER BY `sort_order` ASC")->rows;
	}
	public function getTotalProductSpecials() {
		$query = $this->db->query("SELECT COUNT(DISTINCT ps.product_id) AS total FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))");
		if (isset($query->row['total'])) {
			return $query->row['total'];
		} else {
			return 0;
		}
	}
	public function getNewParamProducts($product_id,$product_my_option_value_id) {
		return $this->db->query("SELECT p_m_o_v.points, p_m_o_v.price, p_m_o_v.weight, p_m_o_v.articul FROM " . DB_PREFIX . "product_my_option_value p_m_o_v WHERE p_m_o_v.product_id = '" . (int)$product_id . "' and p_m_o_v.product_my_option_value_id = '" . $product_my_option_value_id . "'")->row;
	}
	public function getProductCash($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_payment WHERE product_id = '" . (int)$product_id . "'");
		if($query->num_rows > 0)
			return $query->row['only_cash'];
		else
			return false;
	}

	public function getStatusCategory($product_id) {
		$query = $this->db->query("SELECT o_c.status FROM " . DB_PREFIX . "product_to_category o_p left join " . DB_PREFIX . "category o_c ON(o_c.category_id = o_p.category_id) WHERE product_id = '" . (int)$product_id . "'");
		if($query->num_rows > 0)
		{
			if($query->row['status']) {
				$parent_id_query = $this->db->query("SELECT o_c.parent_id FROM " . DB_PREFIX . "product_to_category o_p left join " . DB_PREFIX . "category o_c ON(o_c.category_id = o_p.category_id) WHERE product_id = '" . (int)$product_id . "'");

				if($parent_id_query->num_rows > 0) {
					if($parent_id_query->row['parent_id'] != 0) {
						$get_status_query = $this->db->query("SELECT status FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$parent_id_query->row['parent_id'] . "'");
						if ($get_status_query->num_rows > 0) {
							return $get_status_query->row['status'];
						}
						else
							return false;
					} else
						return true;
				}
				else
					return false;
			}
			return $query->row['status'];
		}
		else
			return false;
	}
}