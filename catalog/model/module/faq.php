<?php
class ModelModuleFaq extends Model {

    public function getAnswer($page,$limit)
    {
        $query = $this->db->query("SELECT * FROM ". DB_PREFIX . "faq f
            LEFT JOIN " . DB_PREFIX . "faq_description fd
	            ON f.id = fd.question_id
            WHERE f.status = 1 AND
	            fd.language_id = " . (int)$this->config->get('config_language_id') . "
            ORDER BY f.sort_order ASC LIMIT $page, ".$limit );

        return $query->rows;
    }
    public function getTotalAnswer() {
        $query = $this->db->query("SELECT count(*) FROM ". DB_PREFIX . "faq WHERE status = 1");
        return  $query->row['count(*)'];
    }
    public function addFaq($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "faq SET `sort_order` = '" . 0 . "', `status` = '" . 0 . "', `create` = NOW(), `ip` = '".$this->db->escape($_SERVER["REMOTE_ADDR"])."'");

        $question_id = $this->db->getLastId();

        foreach ($data['faq_question'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "faq_description SET question_id = '" . (int)$question_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . NULL . "'");
        }

        return $question_id;
    }
    public function getIpDate() {
        $query = $this->db->query("SELECT `create`,`ip` FROM ". DB_PREFIX . "faq");

        return $query->rows;
    }
}