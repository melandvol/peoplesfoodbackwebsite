<?php
class ModelPaymentCOD extends Model {
	public function getMethod() {
		$method_data['0'] = array(
			'code'       => 'cart',
			'title'      => "Оплата картой курьеру",
			'terms'      => '',
			'sort_order' => '0'
		);
		$method_data['1'] = array(
				'code'       => 'cashing',
				'title'      => "Оплата наличными",
				'terms'      => '',
				'sort_order' => '1'
			);

		return $method_data;
	}
}