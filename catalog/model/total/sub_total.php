<?php
class ModelTotalSubTotal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->load->language('total/sub_total');

		$sub_total = $this->cart->getSubTotal();

		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total += $voucher['amount'];
			}
		}

		$total_data[] = array(
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'value'      => $sub_total,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);

		$total += $sub_total;
	}
	public function getTotalApp(&$total_data, &$total,$products) {

		$this->load->language('total/sub_total');

		$total = $this->cart->getTotalApp($products);

		$total_data[] = array(
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'value'      => $total,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);
	}
	public function getTotalPoints(&$total_data, &$total,$points) {
		
		$price_discount = round($points / 5);
		
		$total -= $price_discount;

		$this->load->language('total/sub_total');
		$total_data[] = array(
			'code'       => 'points',
			'title'      => "Бонусы",
			'value'      => -$price_discount,
			'sort_order' => "3"
		);
	}
}