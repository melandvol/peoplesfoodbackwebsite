var pagination_exist = true; // оставить пагинацию и добавить кнопку
var button_more = false; // наличие кнопки "загрузить ещё"
var top_offset = 100; // высота отступа от верха окна, запускающего arrow_top
var window_height = 0; // высота окна
var product_block_offset = 0; // отступ от верха окна блока, содержащего контейнеры

var arrow_top = true; // true - показывать стрелку "вверх"
var product_block = ''; // определяет div, содержащий товары
var pages_count = 0; // счетчик массива ссылок пагинации
var pages = []; // массив для ссылок пагинации
var waiting = false;

function getNextProductPage() {
	if (waiting) return;
    if (pages_count >= pages.length) return;
	waiting = true;
	$(product_block).parent().after('<div id="ajax_loader"><img src="/image/ajax-loader-horizontal.gif" /></div>');
	$.ajax({
		url:pages[pages_count], 
		type:"GET", 
		data:'',
		success:function (data) {
			console.log("ajax success!");
			$data = $(data);
			$('#ajax_loader').remove();
			if ($data) {         
				if ($data.find('.product-list').length > 0)    {
					$(product_block).parent().append($data.find('.product-list').parent().html());
					if (product_block == '.product-grid') {$('#grid-view').trigger('click')};
				} else {
					$(product_block).parent().append($data.find('.product-grid').parent().html());
					if (product_block == '.product-list') {$('#list-view').trigger('click')};
				}
				if (pagination_exist) {
					$('.pagination').html($data.find('.pagination'));
				}
				$('script').each(function(){eval($(this).text())});
			}
			waiting = false;
		}
	});
	pages_count++;
	if (pages_count >= pages.length) {$('.load_more').hide();};
}

function scroll_to_top() {
    $('html, body').animate({
		scrollTop: 0
	}, 300, function() {
		$('.arrow_top').remove();
	});  
}

function getProductBlock() {
    if ($('.product-list').length > 0) {
        product_block = '.product-list';
    } else {
        product_block = '.product-grid';
    }
    return product_block;
}

$(document).ready(function(){ 
    window_height = $(window).height();
    product_block = getProductBlock();
	var button_more_block = $('#load_more').html(); //
    if ($(product_block).length > 0) {
        product_block_offset = $(product_block).offset().top;
                       
        $('.pagination').each(function(){
            href = $(this).find('li:last a').attr('href');
			TotalPages = href.substring(href.indexOf("page=")+5);
			First_index = $(this).find('li.active span').html();
			console.log(TotalPages);
			console.log(First_index);
			i = parseInt(First_index) + 1;
			while (i <= TotalPages) {
				pages.push(href.substring(0,href.indexOf("page=")+5) + i);
				i++;
			}
			console.log(pages);			
        });	
		
		$(window).scroll(function(){
			if (arrow_top) {
				if ($(document).scrollTop() > top_offset) {
					if ($('.arrow_top').length==0) {
						$('body').append('<a class="arrow_top" onclick="scroll_to_top();"></a>')
					}
				} else {
					$('.arrow_top').remove();
				} 
			}
		});
		
		if (button_more) {
			console.log("button_more!");
			$('.pagination').parent().parent().before(button_more_block);
			if (!pagination_exist) {
				$('.pagination').parent().parent().remove();
			} else {
				$('.pagination').parent().parent().find('.col-sm-6.text-right').remove();
			}
			$('.load_more').click( function(event) {
				event.preventDefault();
				getNextProductPage();
			});
		} else {
			$('.pagination').parent().parent().hide();
			$(window).scroll(function(){
				product_block = getProductBlock();
				product_block_height = $(product_block).parent().height();
				if (pages.length > 0) {
					if((product_block_offset+product_block_height-window_height)<($(this).scrollTop())){
						getNextProductPage();
					}
				}
			});
		}
    }
	
});