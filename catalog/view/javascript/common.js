function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    } else { // Изменения для seo_url от Русской сборки OpenCart 2x
        var query = String(document.location.pathname).split('/');
        if (query[query.length - 1] == 'cart') value['route'] = 'checkout/cart';
        if (query[query.length - 1] == 'checkout') value['route'] = 'checkout/checkout';

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

$(document).ready(function () {
    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    // Currency
    $('#currency .currency-select').on('click', function (e) {
        e.preventDefault();

        $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

        $('#currency').submit();
    });

    // Language
    $('#language a').on('click', function (e) {
        e.preventDefault();

        $('#language input[name=\'code\']').attr('value', $(this).attr('href'));

        $('#language').submit();
    });

    /* Search */
    $('#search input[name=\'search\']').parent().find('button').on('click', function () {
        url = $('base').attr('href') + 'index.php?route=product/search';

        var value = $('header input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        location = url;
    });

    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header input[name=\'search\']').parent().find('button').trigger('click');
        }
    });

    // Menu
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 5) + 'px');
        }
    });

    // Product List
    // $('#list-view').click(function() {
    // 	$('#content .product-grid > .clearfix').remove();
    //
    // 	//$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
    // 	$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
    //
    // 	localStorage.setItem('display', 'list');
    // });
    // Product Grid
    $('#grid-view').ready(function () {
        // What a shame bootstrap does not take into account dynamically loaded columns
        cols = $('#column-right, #column-left').length;

        if (cols == 2) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
        } else if (cols == 1) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
        } else {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
        }
        localStorage.setItem('display', 'grid');

        if (counterGrid > 2) {
            resizeProduct();
            window.counterGrid = 1;
        }
        window.counterGrid++;
    });

    counterGrid = 1;
    maxProductHeader = 0;
    maxProductDescription = 0;
    maxProductWeight = 0;
    maxProducType = 0; 
    maxProductSize = 0

    function resizeProduct() {
        $('.product-layout').each(function () {
            var thisHeader = $(this).find('h4').height();
            var thisDescription = $(this).find('.caption>p').height();
            if (thisHeader > maxProductHeader) maxProductHeader = thisHeader;
            if (thisDescription > maxProductDescription) maxProductDescription = thisDescription;

            if ($(this).find('.mark-weight .weight').length > 0) {
                var thisWeight = $(this).find('.mark-weight .weight').height();
                if (thisWeight > maxProductWeight) maxProductWeight = thisWeight;
            }
            if ($(this).find('.product-type').length > 0) {
                var thisType = $(this).find('.product-type').height();
                if (thisType > maxProducType) maxProducType = thisType;
            }
            if ($(this).find('.product-size').length > 0) {
                var thisSize = $(this).find('.product-size>p').height();
                if (thisSize > maxProductSize) maxProductSize = thisSize;
            }
        });
        $('.product-layout').each(function () {
            $(this).find('h4').height(maxProductHeader);
            $(this).find('.caption>p').height(maxProductDescription);
            if(maxProducType > 0)
                $(this).find('.product-type').height(maxProducType+5);
            $(this).find('.product-size').height(maxProductSize);
            var thisHeightWeight = $(this).find('.mark-weight p').height();
            if ($(this).find('.mark-weight .weight').length > 0 &&
                thisHeightWeight >30) {
                $(this).find('.mark-weight>span').css({
                    "display": "inline-block",
                    "margin-top": "8px",
                    "position": "absolute"
                });
                $(this).find('.mark-weight').height(maxProductWeight);
            }
        });
    };
    if (localStorage.getItem('display') == 'grid' && false) {
        $('#list-view').trigger('click');
    } else {
        $('#grid-view').trigger('click');
    }
$(window).resize(function () {
    if (screen.width < 767) {
        $('#search').toggle(false);
        $('#search').remove().appendTo('div.header');
    } else {
            $('#search').remove().appendTo('#mark-search');
            $('#search').toggle(true);
    }
});

    // Checkout
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function (e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({
        container: 'body'
    });

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({
            container: 'body'
        });
    });
});
function mobSearchToggle() {
    var state = $('#search').css('display');
    if (state == 'none')
        $('#search').slideDown();
    else
        $('#search').slideUp();
}
// Cart add remove functions
var cart = {
    'add': function (product_id, param) {
        test = $('input[name*=\'product_my_option_value_id[' + param + ']\']').val();
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name~=\'quantity[' + param + ']\'],input[name~=\'product_id[' + param + ']\'], input[name^=\'ingredients[' + param + '][quantity]\'], input[name*=\'product_my_option_value_id[' + param + ']\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    if (param !== 'modal') {
                        $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        // Need to set timeout otherwise it wont update the total
                        setTimeout(function () {
                            $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                        }, 100);

                        $('html, body').animate({
                            scrollTop: 0
                        }, 'slow');

                        $('#cart > ul').load('index.php?route=common/cart/info ul div');
                    } else {
                        $('#modalProduct .success').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout')
                            location = 'index.php?route=checkout/cart';

                        // Need to set timeout otherwise it wont update the total
                        setTimeout(function () {
                            $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                        }, 100);

                        $('#cart > ul').load('index.php?route=common/cart/info ul div');
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul div');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('...');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    // alert('#cart').load('index.php?route=common/cart/info #cart-total').text());
                    if (json['total'] == 0)
                        $('#cart > ul').load('index.php?route=common/cart/info ul p');
                    else
                        $('#cart > ul').load('index.php?route=common/cart/info ul div');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function () {

    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul div');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function (product_id, modal = false) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    if (!modal)
                        $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    else
                        $('#modalProduct .success').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);
                if (!modal)
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
// Autocomplete */
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function () {
                this.request();
            });

            // Blur
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function () {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    }
})(window.jQuery);

//Кастом
$(document).on("click.bs.dropdown.data-api", ".noclose", function (e) {
    e.stopPropagation()
});

function resizeIngridients(id) {
    var image = $('#product-' + id + ' div.image').height();
    var title = $('#product-' + id + ' h4').height();
    var text = $('#product-' + id + ' p').height();
    $('#product-' + id + ' ul button').css('display', 'none');

};

function resizeIngridientsModal() {
    var image = 0;
    var title = $('.modal-thumb h4').height();
    var text = $('.modal-thumb p').height();
    if (document.body.clientWidth < 767)
        image = $('.modal-thumb .modal-img').height();
    $('.modal-thumb ul').height(image + title + text + 10);
    $("#product-ingredients-modal button").css('display', 'none');
};


//------------ Input number of pizza or anything product ------------------
$.Tween.propHooks.number = {
    get: function (tween) {
        var num = tween.elem.innerHTML.replace(/^[^\d-]+/, '');
        return parseFloat(num) || 0;
    },

    set: function (tween) {
        var opts = tween.options;
        tween.elem.innerHTML = (opts.prefix || '') +
            tween.now.toFixed(opts.fixed || 0) +
            (opts.postfix || '');
    }
};
var arr_ingredient = [];

function add_rem_product(param, price, add, special) {
    var nop = $("#number-of-product" + param).val();
    add ? nop++ : nop--;
    if (nop > 0) {
        if (special == 0) {
            $("#number-of-product" + param).val(nop);
            if (selectOption(param) === false) {
                $("#price-" + param).animate({
                    number: parseInt(price) * nop
                }, {
                    duration: 'fast',
                    postfix: 'р.'
                });
            }
        } else {
            $("#number-of-product" + param).val(nop);
            if (selectOption(param) === false) {
                $("#price-new-" + param).animate({
                    number: parseInt(special) * nop
                }, {
                    duration: 'fast',
                    postfix: 'р.'
                });
                $("#price-old-" + param).animate({
                    number: parseInt(price) * nop
                }, {
                    duration: 'fast',
                    postfix: 'р.'
                });
            }
        }
    }
};
$("#number-of-product").keydown(function (event) {
    if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (event.keyCode >= 35 && event.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
        event.preventDefault();
    }
});

//------------ Input ingredients ------------------
function changeIngredient(param, product_id) {
    var option_value_my_id = $('#size_it-' + param).find(':selected').val();

    if (option_value_my_id === undefined)
        option_value_my_id = $('#size_am-' + param).find(':selected').val();

    $.ajax({
        url: 'index.php?route=product/product/getProductIngredients',
        type: 'POST',
        data: 'product_id=' + product_id + '&param=' + param + '&option_value_my_id=' + option_value_my_id,
        dataType: 'json',
        success: function (json) {
            //addModalProduct(json);
            $("#product-ingredients-" + param).empty();
            $("#product-ingredients-" + param).append(json);
        },
        error: function (data) {
            console.log('Error', data);
        }
    });
}

function add_rem_ingredient(param, price, add, ingredient_id, main) {

    var tmp_summ_price = 0;

    if (typeof arr_ingredient[param] !== 'undefined') tmp_summ_price = arr_ingredient[param];

    if (param === 'modal') tmp_summ_price = modal_ingredient;

    var quan = $('input[name^="ingredients[' + param + '][quantity][' + ingredient_id + ']"]').val();
    if (main) {
        var quan_source = quan;
        add ? ++quan : --quan;
        if (quan <= 1 && quan >= 0 && quan_source != 2) {
            $('input[name^="ingredients[' + param + '][quantity][' + ingredient_id + ']"]').val(quan);
            add ? tmp_summ_price += 0 : tmp_summ_price -= 0;
        } else if (quan >= 1) {
            $('input[name^="ingredients[' + param + '][quantity][' + ingredient_id + ']"]').val(quan);
            add ? tmp_summ_price += price : tmp_summ_price -= price;

            param != 'modal' ? arr_ingredient[param] = tmp_summ_price : modal_ingredient = tmp_summ_price;
            selectOption(param);
        }
    } else {
        add ? ++quan : --quan;
        if (quan >= 0) {
            $('input[name^="ingredients[' + param + '][quantity][' + ingredient_id + ']"]').val(quan);
            add ? tmp_summ_price += price : tmp_summ_price -= price;

            param != 'modal' ? arr_ingredient[param] = tmp_summ_price : modal_ingredient = tmp_summ_price;
            selectOption(param);
        }
    }
}

//-------------- JSON Interface ----------------------------
count = 0;

function selectOption(param) {
    var lengh;
    lengh = (param !== 'modal') ? product_option[param].length : product_option_modal.length;
    if (lengh > 0) {
        var nop = $("#number-of-product" + param).val();
        var comb_arr = [];
        var bSelectOption = false;
        if (param !== 'modal') {
            var ingredient_price = 0;
            if (typeof arr_ingredient[param] !== 'undefined')
                ingredient_price = arr_ingredient[param];
            else
                ingredient_price = 0;
            product_option[param].forEach(function (item, i, option) {
                var count = 0;
                item.option_comb.forEach(function (comb, j, comb_option) {
                    var option_value_my_id = $('#' + comb.type + '-' + param).find(':selected').val();
                    if (comb.type == 'dough' && count == 1)
                        comb_arr.push(comb);

                    if (comb.option_value_my_id == option_value_my_id) {
                        count++;
                    }
                });
                if (item.option_comb.length == count) {
                    bSelectOption = true;
                    $('input[name^=\'product_my_option_value_id[' + param + ']\']').val(item.product_my_option_value_id);
                    $("#price-" + param).animate({
                        number: (parseInt(item.price) + ingredient_price) * nop
                    }, {
                        duration: 'fast',
                        postfix: 'р.'
                    });
                    $("#weight-" + param).text('Вес: ' + item.weight + 'г.');
                }

            });
        } else {
            product_option_modal.forEach(function (item, i, option) {
                var count = 0;
                item.option_comb.forEach(function (comb, j, comb_option) {
                    var option_value_my_id = $('#' + comb.type + '-' + param).find(':selected').val();
                    if (comb.type == 'dough' && count == 1)
                        comb_arr.push(comb);

                    if (comb.option_value_my_id == option_value_my_id) {
                        count++;
                    }
                });
                if (item.option_comb.length == count) {
                    bSelectOption = true;
                    $('input[name^=\'product_my_option_value_id\']').val(item.product_my_option_value_id);
                    $("#price-" + param).animate({
                        number: (parseInt(item.price) + modal_ingredient) * nop
                    }, {
                        duration: 'fast',
                        postfix: 'р.'
                    });
                    $("#weight-" + param).text('Вес: ' + item.weight + 'г.');
                }

            });
        }
        if ($('*').is('#dough-' + param)) {
            var option_value_my_id = $('#dough-' + param).find(':selected').val();
            $('#dough-' + param).empty();

            comb_arr.forEach(function (comb, i, option) {
                if (comb.option_value_my_id == option_value_my_id)
                    $('#' + comb.type + '-' + param).append($('<option value="' + comb.option_value_my_id + '" selected>' + comb.name + '</option>'))
                else
                    $('#' + comb.type + '-' + param).append($('<option value="' + comb.option_value_my_id + '">' + comb.name + '</option>'))
            });
        }
        //Если произошла ошибка в установке опции
        if (!bSelectOption && count < 5) {
            count++;
            selectOption(param);
        }
    } else return false;
}

function getProduct(product_id) {
    $.ajax({
        url: 'index.php?route=product/product/getProduct',
        type: 'POST',
        data: 'product_id=' + product_id,
        dataType: 'json',
        success: function (json) {
            if( !json['error'] ) {
                $("#modalProduct").empty();
                $('#modalProduct').append(json);
                $("#modalProduct").modal('show');
            } else {
                alert('В данный момент покупака невозможна');
            }
        },
        error: function (data) {
            console.log('Error', data);
        }
    });
}

function changePriceCart(cart_id, add, price) {
    var nop = $('input[name^="quantity[' + cart_id + ']').val();
    add ? nop++ : nop--;
    if (nop > 0) {
        $('input[name^="quantity[' + cart_id + ']').val(nop);
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: $('input[name^=\'quantity\']'),
            dataType: 'json',
            success: function (json) {
                if (!json['redirect']) {
                    $("#price-" + cart_id).animate({
                        number: parseInt(price) * nop
                    }, {
                        duration: 'fast',
                        postfix: 'р.'
                    });
                    $("#price-mob-" + cart_id).animate({
                        number: parseInt(price) * nop
                    }, {
                        duration: 'fast',
                        postfix: 'р.'
                    });

                    var totals = JSON.parse(json['success']);
                    totals.forEach(function (total, i, option) {
                        $('#total-' + total.code).animate({
                            number: total.text
                        }, {
                            duration: 'fast',
                            postfix: 'р.'
                        });
                        $('#total-mob-' + total.code).animate({
                            number: total.text
                        }, {
                            duration: 'fast',
                            postfix: 'р.'
                        });

                        if (total.code == 'total')
                            $('input[name^=\'price_total\']').val(total.text);
                    });
                } else
                    location.reload();
            },
            error: function (error) {
                location = 'index.php?route=checkout/cart';
            }
        });


    } else if (nop == 0) {
        $("#button-remove-" + cart_id).click();
    }
}

function changePizza(param, product_id) {

    changeIngredient(param, product_id);

    if (param === 'modal')
        modal_ingredient = 0;
    else if (typeof arr_ingredient[param] !== 'undefined')
        arr_ingredient[param] = 0;

    selectOption(param);
}