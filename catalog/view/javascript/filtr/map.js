  $(document).ready(function() {
   var html = document.documentElement;
   var b1 = document.getElementById("b1");
   var b2 = document.getElementById("b2");
   if(html.clientWidth < 960) {
    b1.style.marginTop = b2.offsetHeight + "px";
    b2.style.marginTop = -(b1.offsetHeight + b2.offsetHeight) + "px";
  }
  window.onresize= function(){
    if(html.clientWidth < 960) {
      b1.style.marginTop = b2.offsetHeight + "px";
      b2.style.marginTop = -(b1.offsetHeight + b2.offsetHeight) + "px";
    } else {
      b1.style.marginTop = 0 + "px";
      b2.style.marginTop = 0 + "px";
    }
  };
  $('a[id ^= kitchen]').hover(function(){
   var id = $(this).attr("id");
   $('#mark').toggleClass(id);
   $('#mark').toggleClass('display-none');
 });
});
