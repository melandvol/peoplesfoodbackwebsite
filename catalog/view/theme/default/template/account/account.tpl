<?php echo $header; ?>
<div class="container">
 <h3 class="text-center">Личный кабинет</h3>
 <?php if ($success) { ?>
 <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
 <?php } ?>
 <div class="row"><?php echo $column_left; ?>
  <?php if ($column_left && $column_right) { ?>
  <?php $class = 'col-sm-6'; ?>
  <?php } elseif ($column_left || $column_right) { ?>
  <?php $class = 'col-sm-9'; ?>
  <?php } else { ?>
  <?php $class = 'col-sm-12'; ?>
  <?php } ?>
  <div class="col-xs-12 text-center client-score">
      <?php if($card_info['status'] == 2) { ?>

      <span>
            <p>Номер ID карты</p>
            <p><?php echo $card_info['id_cart'] ?></p>
      </span>
      <?php } else if($card_info['status'] == 1)  { ?>
            <span>
                <p><?php echo $card_info['id_cart'] ?></p>
            </span>
      <?php } else if($card_info['status'] == 0)  { ?>
            <span>
              <a style="cursor:pointer;" onclick="getProduct(<?php echo $card_info['id_cart_bay'] ?>);" >
                  <p><?php echo $card_info['id_cart'] ?></p>
              </a>
            </span>
      <?php } ?>
  </div>
  <div id="content" class="<?php echo $class; ?> user-account"><?php echo $content_top; ?>
    <div class="row">
      <div class="col-sm-6">
        <h2><?php echo $text_my_account; ?></h2>
        <ul class="list-unstyled">
          <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
          <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
          <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-6">
        <h2><?php echo $text_my_orders; ?></h2>
        <ul class="list-unstyled text-left">
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <?php if ($reward) { ?>
          <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
          <?php } ?>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
      <?php echo $content_bottom; ?>
    </div>
  </div>
  <?php echo $column_right; ?>
</div>
</div>
<?php echo $footer; ?>
