<?php echo $header; ?>
<div class="container">
  <h3 class="text-center"><?php echo $text_edit_address; ?></h3>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"> <?php echo $content_top; ?>

     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-microdistrict">Микрорайон / Нас. пункт</label>
                <div class="col-sm-10">
                    <input type="text" name="microdistrict" value="<?php echo $microdistrict; ?>" placeholder="Микрорайон / Нас. пункт" id="input-microdistrict" class="form-control" />
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-street">Улица</label>
                <div class="col-sm-10">
                    <input type="text" name="street" value="<?php echo $street; ?>" placeholder="Улица" id="input-street" class="form-control" />
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-house">Дом / Корп.</label>
                <div class="col-sm-10">
                    <input type="text" name="house" value="<?php echo $house; ?>" placeholder="Дом / Корп." id="input-house" class="form-control" />
                    <?php if ($error_house) { ?>
                    <div class="text-danger"><?php echo $error_house; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-housing">Строение</label>
                <div class="col-sm-10">
                    <input type="text" name="housing" value="<?php echo $housing; ?>" placeholder="Строение" id="input-housing" class="form-control" />
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-entrance">Подъезд</label>
                <div class="col-sm-10">
                    <input type="text" name="entrance" value="<?php echo $entrance; ?>" placeholder="Подъезд" id="input-entrance" class="form-control" />
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-code">Код / Домофон</label>
                <div class="col-sm-10">
                    <input type="text" name="code" value="<?php echo $code; ?>" placeholder="Код / Домофон" id="input-code" class="form-control" />
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-floor">Этаж</label>
                <div class="col-sm-10">
                    <input type="text" name="floor" value="<?php echo $floor; ?>" placeholder="Этаж" id="input-floor" class="form-control" />
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-flat">Квартира / Офис</label>
                <div class="col-sm-10">
                    <input type="text" name="flat" value="<?php echo $flat; ?>" placeholder="Квартира / Офис" id="input-flat" class="form-control" />
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>
                <div class="col-sm-10">
                  <?php if ($default) { ?>
                  <label class="radio-inline">
                    <input type="radio" name="default" value="1" checked="checked" />
                    <?php echo $text_yes; ?></label>
                    <label class="radio-inline">
                        <input type="radio" name="default" value="0" />
                        <?php echo $text_no; ?></label>
                        <?php } else { ?>
                        <label class="radio-inline">
                            <input type="radio" name="default" value="1" />
                            <?php echo $text_yes; ?></label>
                            <label class="radio-inline">
                                <input type="radio" name="default" value="0" checked="checked" />
                                <?php echo $text_no; ?></label>
                                <?php } ?>
                            </div>
                        </div>
                    </fieldset>
                    <div class="buttons clearfix">
                      <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                      <div class="pull-right">
                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                    </div>
                </div>
            </form>
            <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
        </div>
        <script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
		$('.form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('.form-group').length) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('.form-group').length) {
		$('.form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
       clearInterval(timer);
   }

   timer = setInterval(function() {
      if ($('#form-upload input[name=\'file\']').val() != '') {
         clearInterval(timer);

         $.ajax({
            url: 'index.php?route=tool/upload',
            type: 'post',
            dataType: 'json',
            data: new FormData($('#form-upload')[0]),
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
               $(node).button('loading');
           },
           complete: function() {
               $(node).button('reset');
           },
           success: function(json) {
               $(node).parent().find('.text-danger').remove();

               if (json['error']) {
                  $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
              }

              if (json['success']) {
                  alert(json['success']);

                  $(node).parent().find('input').attr('value', json['code']);
              }
          },
          error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
       }
   });
     }
 }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo isset($zone_id)? $zone_id: -1 ?>') {
						html += ' selected="selected"';
                 }

                 html += '>' + json['zone'][i]['name'] + '</option>';
             }
         } else {
            html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
        }

        $('select[name=\'zone_id\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
     alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
 }
});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<?php echo $footer; ?>
