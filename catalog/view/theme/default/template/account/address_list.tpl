<?php echo $header; ?>
<div class="container">
  <h3 class="text-center"><?php echo $text_address_book; ?></h3>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row" style="margin-top: 92px;"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if (isset($addresses)) { ?>
      <div class="row">
        <?php foreach ($addresses as $result) if ($result['default']) { ?>
          <div class="col-sm-8">
              <h4>Основной</h4>
                <?php echo $result['address']; ?>
          </div>
          <div class="col-sm-4 text-center">
            <a href="<?php echo $result['update']; ?>" class="btn btn-blue "><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn btn-blue"><?php echo $button_delete; ?></a>
          </div>
        <?php } ?>
        <?php $echo_text = true; ?>
        <?php foreach ( $addresses as $result ) if (!$result['default']) { ?>
        <div class="col-sm-8">
          <?php  if( $echo_text ) { ?>
          <h4>Дополнительный</h4>
          <?php $echo_text = false;  } ?>
          <?php echo $result['address']; ?>
          <?php echo '<hr>' ?>
        </div>
        <div class="col-sm-4 text-center">
          <a href="<?php echo $result['update']; ?>" class="btn btn-blue "><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn btn-blue"><?php echo $button_delete; ?></a>
        </div>
        <?php } ?>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
       <div class="pull-left btn-newadress">
        <a href="<?php echo $add; ?>" class="btn "><span class="fa">+</span>Добавить новый адрес</a>
      </div>
      <?php echo $content_bottom; ?></div>
      <?php echo $column_right; ?></div>
    </div>
    <?php echo $footer; ?>
<script>
$('.btn-newadress>a').hover(
  function() {  $(this).children('span').css('border-color', '#ff0000'); },
  function() {  $(this).children('span').css('border-color', '#88c240'); }
  );
// $('.btn-newadress>a').focus(function(e) {$(this).children('span').css('border-color', '#ff0000');});
</script>
