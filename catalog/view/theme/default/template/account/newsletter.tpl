<?php echo $header; ?>
<div class="container">
  <h3 class="text-center"><?php echo $heading_title; ?></h3>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <div class="form-group">
            <label class="col-sm-4 control-label">Подписаться на SMS рассылку</label>
            <div class="col-sm-8">
              <?php if ($newsletter_sms) { ?>
              <label class="radio-inline">
                <input type="radio" name="newsletter_sms" value="1" checked="checked" />
                <?php echo $text_yes; ?> </label>
                <label class="radio-inline">
                  <input type="radio" name="newsletter_sms" value="0" />
                  <?php echo $text_no; ?></label>
                  <?php } else { ?>
                  <label class="radio-inline">
                    <input type="radio" name="newsletter_sms" value="1" />
                    <?php echo $text_yes; ?> </label>
                    <label class="radio-inline">
                      <input type="radio" name="newsletter_sms" value="0" checked="checked" />
                      <?php echo $text_no; ?></label>
                      <?php } ?>
                    </div>
                  </div>
                </fieldset>

        <fieldset>
          <div class="form-group">
            <label class="col-sm-4 control-label">Подписаться на E-mail рассылку</label>
            <div class="col-sm-8">
              <?php if ($newsletter) { ?>
              <label class="radio-inline">
                <input type="radio" name="newsletter" value="1" checked="checked" />
                <?php echo $text_yes; ?> </label>
              <label class="radio-inline">
                <input type="radio" name="newsletter" value="0" />
                <?php echo $text_no; ?></label>
              <?php } else { ?>
              <label class="radio-inline">
                <input type="radio" name="newsletter" value="1" />
                <?php echo $text_yes; ?> </label>
              <label class="radio-inline">
                <input type="radio" name="newsletter" value="0" checked="checked" />
                <?php echo $text_no; ?></label>
              <?php } ?>
            </div>
          </div>
        </fieldset>

                <div class="buttons clearfix">
                  <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                  <div class="pull-right">
                    <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                  </div>
                </div>
              </form>
              <?php echo $content_bottom; ?></div>
              <?php echo $column_right; ?></div>
            </div>
            <?php echo $footer; ?>
