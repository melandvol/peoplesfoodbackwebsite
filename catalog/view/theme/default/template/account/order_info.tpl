<?php echo $header; ?>
<div class="container">
  <h3 class="text-center" ><?php echo $heading_title; ?></h3>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <td class="text-left" colspan="2"><h4><?php echo $text_order_detail; ?></h4></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left" style="width: 50%;"><?php if ($invoice_no) { ?>
              <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
              <?php } ?>
              <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
              <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
              <td class="text-left"><?php if ($payment_method) { ?>
                <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
                <?php } ?>
                <?php if ($shipping_method) { ?>
                <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
                <?php } ?></td>
              </tr>
            </tbody>
          </table>
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <td class="text-left" style="width: 50%;"><h4>Адрес доставки</h4></td>
              </tr>
            </thead>
            <tbody>
            <tr>
            <?php foreach ($addresses as $result) { ?>
              <td class="text-left"><div class="col-sm-8"><?php echo $result['address']; ?></div></td>
            <?php } ?>
            </tr>
            </tbody>
          </table>
          <div class="table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <td class="text-left"><h4><?php echo $column_name; ?></h4></td>
                  <td class="text-right"><h4><?php echo $column_price; ?></h4></td>
                  <td class="text-right"><h4><?php echo $column_total; ?></h4></td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($products as $product) { ?>
                <tr>
                  <td class="text-left"><?php echo $product['name']; ?> </td>
                  <td class="text-right"><?php echo $product['price']; ?></td>
                  <td class="text-right"><?php echo $product['total']; ?></td>
                </tr>
                <?php } ?>
                <?php foreach ($vouchers as $voucher) { ?>
                <tr>
                  <td class="text-left"><?php echo $voucher['description']; ?></td>
                  <td class="text-left"></td>
                  <td class="text-right">1</td>
                  <td class="text-right"><?php echo $voucher['amount']; ?></td>
                  <td class="text-right"><?php echo $voucher['amount']; ?></td>
                  <?php if ($products) { ?>
                  <td></td>
                  <?php } ?>
                </tr>
                <?php } ?>
              </tbody>
              <tfoot>
                <?php foreach ($totals as $total) { ?>
                <tr>
                  <td colspan="1"></td>
                  <td class="text-right"><b><?php echo $total['title']; ?></b></td>
                  <td class="text-right"><?php echo $total['text']; ?></td>
                </tr>
                <?php } ?>
              </tfoot>
            </table>
          </div>
          <?php if ($comment) { ?>
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left"><?php echo $text_comment; ?></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-left"><?php echo $comment; ?></td>
              </tr>
            </tbody>
          </table>
          <?php } ?>
          <?php if ($histories) { ?>
          <h3><?php echo $text_history; ?></h3>
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left"><?php echo $column_date_added; ?></td>
                <td class="text-left"><?php echo $column_status; ?></td>
                <td class="text-left"><?php echo $column_comment; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($histories as $history) { ?>
              <tr>
                <td class="text-left"><?php echo $history['date_added']; ?></td>
                <td class="text-left"><?php echo $history['status']; ?></td>
                <td class="text-left"><?php echo $history['comment']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php } ?>
          <div class="buttons clearfix">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
          </div>
          <?php echo $content_bottom; ?></div>
          <?php echo $column_right; ?></div>
        </div>
        <?php echo $footer; ?>
