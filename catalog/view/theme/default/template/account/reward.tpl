<?php echo $header; ?>
<div class="container">
  <h3 class="text-center"><?php echo $heading_title; ?></h3>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if($card_info['status']!= 2) { ?>
        <h4><?= $card_info['id_cart'] ?></h4>
      <?php } else if($card_info['status']== 2) { ?>
        <h4><?php echo $text_total; ?> <b><?= $card_info['points'] ?></b>.</h4>
      <?php } ?>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        </div>
        <div class="buttons clearfix">
          <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
        </div>
        <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
      </div>
      <?php echo $footer; ?>
