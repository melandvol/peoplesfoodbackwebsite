<?php echo $header; ?>
<div class="container">
    <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="text-center">
                <h3><?php echo $heading_title; ?>
                    <?php if ($weight) { ?>
                    &nbsp;(<?php echo $weight; ?>)
                    <?php } ?>
                </h3>
            </div>
            <div class="error-coup">
                <?php if ($attention) { ?>
                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
                <?php } ?>
                <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
                <?php } ?>
                <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
                <?php } ?>
            </div>
            <?php if ($logged) { ?>
            <div class="col-xs-12 client-score-info lateral-padding-0">
                <div class="text-center col-sm-12 col-xs-12 col-md-4 lateral-padding-0">
        <span>
          <p><?php echo $firstname ?></p>
        </span>
                </div>
                <?php if($card_info['status'] == 2) { ?>
                <div class="text-center col-sm-12 col-xs-12 col-md-4 lateral-padding-0">
        <span>
          <p>Номер карты</p>
          <p><?php echo $card_info['id_cart'] ?></p>
        </span>
                </div>
                <div class="text-center col-sm-12 col-xs-12 col-md-4 lateral-padding-0">
        <span>
          <p>количество пифов на счету</p>
          <p><?php echo $card_info['points'] ?></p>
        </span>
                </div>
                <?php } else if($card_info['status'] == 1)  { ?>
                <div class="text-center col-sm-12 col-xs-12 col-md-8 lateral-padding-0">
        <span>
         <p><?php echo $card_info['id_cart'] ?></p>
       </span>
                </div>
                <?php } else if($card_info['status'] == 0)  { ?>
                <div class="text-center col-sm-12 col-xs-12 col-md-8 lateral-padding-0">
      <span>
        <a style="cursor:pointer;" onclick="getProduct(<?php echo $card_info['id_cart_bay'] ?>);">
          <p><?php echo $card_info['id_cart'] ?></p></a>
        </span>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
            <div class="col-xs-12 lateral-padding-0 ">
                <!--<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">-->
                <div class="hidden-lg hidden-md hidden-sm">
                    <?php foreach ($products as $key => $product) { ?>
                    <div class="cart-mob-product">
                        <a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>);"><?php echo $product['name']; ?></a>

                            <div class="row">
                                <?php if ($product['thumb']) { ?>
                                    <div class="img img-responsive">
                                        <a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>);"> <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive"/></a>
                                    </div>
                                <?php } ?>
                            <div class="description">
                                <?php if ($product['my_option']) { ?>
                                    <?php foreach ($product['my_option'] as $key_op => $my_option) { ?>
                                        <?php if($my_option['type'] == 'size_it' || $my_option['type'] == 'size_am') { ?>
                                            <small>Размер пиццы: <?php echo $my_option['name']; ?></small>
                                            <input style="display: none" name="option_value_my_id_header[<?php echo $product['cart_id']; ?>][<?php echo $key_op ?>]" value="<?php if(isset($my_option['option_value_my_id'])) echo $my_option['option_value_my_id'] ?>"/>
                                        <?php } else if($my_option['type'] == 'dough') { ?>
                                            <small>Тип теста: <?php echo $my_option['name']; ?></small>
                                            <input style="display: none" name="option_value_my_id_header[<?php echo $product['cart_id']; ?>][<?php echo $key_op ?>]" value="<?php if(isset($my_option['option_value_my_id'])) echo $my_option['option_value_my_id'] ?>"/>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>

                                <?php if ($product['weight']) { ?>
                                    <small><?php echo $product['weight']; ?></small>
                                <?php } ?>

                                <?php if ($product['ingredients']) { ?>
                                    <?php if ($product['ingredients']['add']) { ?>
                                        <small class="add-ingredients" style=""><?php echo $product['ingredients']['add']?></small>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                    <?php if ($product['ingredients']['remove']) { ?>
                                    <small class="remove-ingredients" style=""><?php echo $product['ingredients']['remove']?></small>
                                    <?php } ?>
                                <?php } ?>

                                <?php if ($product['reward']) { ?>
                                    <small><?php echo $product['reward']; ?></small>
                                <?php } ?>

                                <?php if ($product['recurring']) { ?>
                                    <span class="label label-info"><?php echo $text_recurring_item; ?></span>
                                    <small><?php echo $product['recurring']; ?></small>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row other">
                            <div class="col-xs-4 col-xs-offset-1">
                                <div class="input-group">
                                    <?php if(!$product['single_exemplar']) { ?>
                                        <span class="input-group-btn"><button onclick="changePriceCart(<?php echo $product['cart_id']; ?>,false,<?php echo $product['total']; ?>)" title="Удалить" class="btn" style="margin-right: 12px;"><i class="fa fa-minus" aria-hidden="true"></i></button></span>
                                        <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" maxlenght="2" class="form-control">
                                        <span class="input-group-btn"><button style="margin-left: 12px;" onclick="changePriceCart(<?php echo $product['cart_id']; ?>,true,<?php echo $product['total']; ?>)" title="Добавить" class="btn"><i class="fa fa-plus" aria-hidden="true"></i></button></span>
                                    <?php } else { ?>
                                        <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="1" size="1" maxlenght="2" readonly>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-offset-5 col-xs-2">
                                <p class="product-total" id="price-mob-<?php echo $product['cart_id']; ?>" style=""><?php echo $product['total'] * $product['quantity']; ?>р.</p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
                <div class="table-mob hidden-sm hidden-lg hidden-md">
                    <table class="" style="width: 100%;">
                        <tbody>
                        <?php foreach ($vouchers as $vouchers) { ?>
                            <tr>
                                <td></td>
                                <td class="text-left"><?php echo $vouchers['description']; ?></td>
                                <td class="text-left"></td>
                                <td class="text-left">
                                    <div class="inline-group btn-block">
                                        <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control"/>
                                        <span class="input-group-btn"><button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle" aria-hidden="true"></i></button></span>
                                    </div>
                                </td>
                                <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                                <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tbody>
                        <?php if ($coupon_mob || $voucher || $reward || $points_mob) { ?>
                            <tr style="border-collapse: collapse;">
                                <td colspan="6" style="border-top: none;">
                                    <div class="panel-group" id="accordion-mob"><?php echo $coupon_mob; ?><?php if(isset($points_mob)) echo $points_mob; ?><?php /*echo $voucher; */?><?php echo $reward; ?></div>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php foreach ($totals as $total) { ?>
                        <tr>
                            <td colspan="2" style="border-top: none;"></td>
                            <td colspan="2" class="text-center cart-total"><?php echo $total['title']; ?>: <span id="total-mob-<?php echo $total['code']; ?>"><?php echo $total['text']; ?>р.</span></td>

                            <?php if($total['code'] == 'total') { ?>
                                <input style="display: none" name="price_total" value="<?php echo $total['text']; ?>"/>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="2" class="cart-order" style="border-top: none;">
                                <div class="pull-right">
                                    <a href="<?php echo $continue; ?>" class="btn btn-default"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i>Добавить товар</a>
                                </div>
                            </td>
                            <td colspan="2" class="cart-continue" style="border-top: none;">
                                <div class="pull-right">
                                    <a id="button-mob-checkout" class="btn btn-primary">Оформить</a>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive slh-border hidden-xs">

                    <table class="table cart-table">
                        <thead class="cart-table-head">
                        <tr>
                            <td>Товар</td>
                            <td>Количество</td>
                            <td>Цена</td>
                            <td>Удалить</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($products as $key => $product) { ?>
                        <tr class="">
                            <td class="product-name">
                                <?php if ($product['thumb']) { ?>
                                    <a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>);"> <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail"/></a>
                                <?php } ?>
                                <div>
                                    <a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>);"><?php echo $product['name']; ?></a>

                                    <br>
                                    <br>

                                    <?php if ($product['my_option']) { ?>
                                        <?php foreach ($product['my_option'] as $key_op => $my_option) { ?>
                                            <?php if($my_option['type'] == 'size_it' || $my_option['type'] == 'size_am') { ?>
                                                <small>Размер пиццы: <?php echo $my_option['name']; ?></small>
                                                <input style="display: none" name="option_value_my_id_header[<?php echo $product['cart_id']; ?>][<?php echo $key_op ?>]" value="<?php if(isset($my_option['option_value_my_id'])) echo $my_option['option_value_my_id'] ?>"/>
                                            <?php } else if($my_option['type'] == 'dough') { ?>
                                                <small>Тип теста: <?php echo $my_option['name']; ?></small>
                                                <input style="display: none" name="option_value_my_id_header[<?php echo $product['cart_id']; ?>][<?php echo $key_op ?>]" value="<?php if(isset($my_option['option_value_my_id'])) echo $my_option['option_value_my_id'] ?>"/>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if ($product['weight']) { ?>
                                        <small><?php echo $product['weight']; ?></small>
                                    <?php } ?>

                                    <?php if ($product['ingredients']) { ?>
                                        <?php if ($product['ingredients']['add']) { ?>
                                            <small class="add-ingredients" style=""><?php echo $product['ingredients']['add']?></small>
                                        <?php } ?>
                                        <div class="clearfix"></div>
                                        <?php if ($product['ingredients']['remove']) { ?>
                                            <small class="remove-ingredients" style=""><?php echo $product['ingredients']['remove']?></small>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if ($product['reward']) { ?>
                                        <small><?php echo $product['reward']; ?></small>
                                    <?php } ?>

                                    <?php if ($product['recurring']) { ?>
                                        <span class="label label-info"><?php echo $text_recurring_item; ?></span>
                                        <small><?php echo $product['recurring']; ?></small>
                                    <?php } ?>
                                </div>
                            </td>
                            <td class="product-number">
                                <div class="input-group btn-block" style="max-width: 10px;">
                                    <?php if(!$product['single_exemplar']) { ?>
                                    <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" maxlenght="2">
                                    <span class="input-group-btn">
                                        <button onclick="changePriceCart(<?php echo $product['cart_id']; ?>,false,<?php echo $product['total']; ?>)" title="Удалить" class="btn"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button onclick="changePriceCart(<?php echo $product['cart_id']; ?>,true,<?php echo $product['total']; ?>)" title="Добавить" class="btn"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                    </span>
                                    <?php } else { ?>
                                        <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="1" size="1" maxlenght="2" readonly>
                                    <?php } ?>
                                </div>
                            </td>
                            <td class="product-total" id="price-<?php echo $product['cart_id']; ?>">
                                <?php echo $product['total'] * $product['quantity']; ?>р.
                            </td>
                            <td class="product-remove">
                                <div class="btn-block">
                                    <button type="button" id="button-remove-<?php echo $product['cart_id']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="glyphicon glyphicon-remove-circle" aria-hidden="true"></i></button>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php foreach ($vouchers as $vouchers) { ?>
                        <tr>
                            <td></td>
                            <td class="text-left"><?php echo $vouchers['description']; ?></td>
                            <td class="text-left"></td>
                            <td class="text-left">
                                <div class="inline-group btn-block">
                                    <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control"/>
                                    <span class="input-group-btn"> <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle" aria-hidden="true"></i></button></span>
                                </div>
                            </td>
                            <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                            <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                        <tbody>
                        <?php if ($coupon || $voucher || $reward || $points) { ?>
                        <tr style="border-collapse: collapse;">
                            <td colspan="6" style="border-top: none;">
                                <div class="panel-group" id="accordion"><?php echo $coupon; ?><?php if(isset($points)) echo $points; ?><?php /*echo $voucher; */?><?php echo $reward; ?></div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php foreach ($totals as $total) { ?>
                        <tr>
                            <td colspan="2" style="border-top: none;"></td>
                            <td colspan="2" class="text-center cart-total"><?php echo $total['title']; ?>: <span id="total-<?php echo $total['code']; ?>"><?php echo $total['text']; ?>р.</span></td>
                            <?php if($total['code'] == 'total') { ?>
                                <input style="display: none" name="price_total" value="<?php echo $total['text']; ?>"/>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="2" class="cart-order" style="border-top: none;">
                                <div class="pull-right">
                                    <a href="<?php echo $continue; ?>" class="btn btn-default"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i>Добавить товар</a>
                                </div>
                            </td>
                            <td colspan="2" class="cart-continue" style="border-top: none;">
                                <div class="pull-right">
                                    <a id="button-checkout" class="btn btn-primary">Продолжить оформление</a>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!--</form>-->
            </div>

            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<br/>
</div>
<?php echo $footer; ?>
<script>
    $(document).delegate('#button-checkout', 'click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/check',
            type: 'post',
            data: $('input[name^=\'option_value_my_id_header\'], input[name^=\'quantity\'], input[name^=\'price_total\'] '),
            dataType: 'json',
            beforeSend: function () {
                $('#button-checkout').button('Загрузка...');
            },
            complete: function () {
                $('#button-checkout').button('reset');
            },
            success: function (json) {
                $('.alert').remove();

                if (json['error']) {
                    $('.error-coup').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({scrollTop: 0}, 'slow');
                } else if (json['redirect']) {
                    location = json['redirect'];
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
</script>
<script>
    $(document).delegate('#button-mob-checkout', 'click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/check',
            type: 'post',
            data: $('input[name^=\'option_value_my_id_header\'], input[name^=\'quantity\'], input[name^=\'price_total\'] '),
            dataType: 'json',
            beforeSend: function () {
                $('#button-mob-checkout').button('Загрузка...');
            },
            complete: function () {
                $('#button-mob-checkout').button('reset');
            },
            success: function (json) {
                $('.alert').remove();

                if (json['error']) {
                    $('.error-coup').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({scrollTop: 0}, 'slow');
                } else if (json['redirect']) {
                    location = json['redirect'];
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
</script>
