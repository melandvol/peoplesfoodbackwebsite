<div class="row">
  <div class="col-sm-4">
    <fieldset id="account">
      <legend><?php echo $text_your_details; ?></legend>
      <div class="form-group" style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
        <label class="control-label"><?php echo $entry_customer_group; ?></label>
        <?php foreach ($customer_groups as $customer_group) { ?>
        <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
        <div class="radio">
          <label>
            <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
            <?php echo $customer_group['name']; ?></label>
          </div>
          <?php } else { ?>
          <div class="radio">
            <label>
              <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" />
              <?php echo $customer_group['name']; ?></label>
            </div>
            <?php } ?>
            <?php } ?>
          </div>
          <div class="form-group required">
            <label class="control-label" for="input-payment-firstname"><?php echo $entry_firstname; ?></label>
            <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-payment-firstname" class="form-control" />
          </div>
          <div class="form-group required">
            <label class="control-label" for="input-payment-telephone"><?php echo $entry_telephone; ?></label>
            <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-payment-telephone" class="form-control" />
          </div>
          <div class="form-group">
            <label class="control-label" for="input-payment-fax">Дополнительный телефон</label>
            <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="Дополнительный телефон" id="input-payment-fax" class="form-control" />
          </div>
  </fieldset>
</div>
<div class="col-sm-8">
  <fieldset id="address">
    <legend><?php echo $text_your_address; ?></legend>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label class="control-label" for="input-microdistrict">Микрорайон / Нас. пункт</label>
          <input type="text" name="microdistrict" value="" placeholder="Микрорайон / Нас. пункт" id="input-microdistrict" class="form-control" />
        </div>
        <div class="form-group">
          <label class="control-label" for="input-street">Улица</label>
          <input type="text" name="street" value="" placeholder="Улица" id="input-street" class="form-control" />
        </div>
        <div class="form-group required">
          <label class="control-label" for="input-house">Дом / Корп.</label>
          <input type="text" name="house" value="" placeholder="Дом / Корп." id="input-house" class="form-control" />
        </div>

        <div class="form-group">
          <label class="control-label" for="input-housing">Строение</label>
          <input type="text" name="housing" value="" placeholder="Строение" id="input-housing" class="form-control" />
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label class="control-label" for="input-entrance">Подъезд</label>
          <input type="text" name="entrance" value="" placeholder="Подъезд" id="input-entrance" class="form-control" />
        </div>
        <div class="form-group">
          <label class="control-label" for="input-code">Код / Домофон</label>
          <input type="text" name="code" value="" placeholder="Код / Домофон" id="input-code" class="form-control" />
        </div>
        <div class="form-group">
          <label class="control-label" for="input-floor">Этаж</label>
          <input type="text" name="floor" value="" placeholder="Этаж" id="input-floor" class="form-control" />
        </div>
        <div class="form-group">
          <label class="control-label" for="input-flat">Квартира / Офис</label>
          <input type="text" name="flat" value="" placeholder="Квартира" id="input-flat" class="form-control" />
        </div>
      </div>
    </div>
  </fieldset>

  <?php echo $captcha; ?>
</div>
</div>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-guest" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>
<script type="text/javascript"><!--
// Sort the custom fields
$('#account .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
		$('#account .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .form-group').length) {
		$('#account .form-group:first').before(this);
	}
});

$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});

$('#collapse-payment-address input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('#collapse-payment-address .custom-field').hide();
			$('#collapse-payment-address .custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#payment-custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#payment-custom-field' + custom_field['custom_field_id']).addClass('required');
				} else {
					$('#payment-custom-field' + custom_field['custom_field_id']).removeClass('required');
				}
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#collapse-payment-address input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address button[id^=\'button-payment-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
   clearInterval(timer);
 }

 timer = setInterval(function() {
  if ($('#form-upload input[name=\'file\']').val() != '') {
   clearInterval(timer);

   $.ajax({
    url: 'index.php?route=tool/upload',
    type: 'post',
    dataType: 'json',
    data: new FormData($('#form-upload')[0]),
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function() {
     $(node).button('loading');
   },
   complete: function() {
     $(node).button('reset');
   },
   success: function(json) {
     $(node).parent().find('.text-danger').remove();

     if (json['error']) {
      $(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
    }

    if (json['success']) {
      alert(json['success']);

      $(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
    }
  },
  error: function(xhr, ajaxOptions, thrownError) {
   alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
 }
});
 }
}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#collapse-payment-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#collapse-payment-address input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('#collapse-payment-address input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('#collapse-payment-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#collapse-payment-address select[name=\'country_id\']').trigger('change');
//--></script>
