<form class="form-horizontal">
  <?php if (isset($addresses)) { ?>
  <div class="radio">
    <label>
      <input type="radio" name="payment_address" value="existing" checked="checked" />
      <?php echo $text_address_existing; ?></label>
  </div>
  <div id="payment-existing">
    <select name="address_id" class="form-control">
      <?php foreach ($addresses as $address) { ?>
      <option value="<?php echo $address['address_id']; ?>" <?php if ($address['address_id'] == $address_id) echo 'selected' ?>>
		<?php echo $address['address']; ?>
	  </option>
      <?php } ?>
    </select>
  </div>
  <div class="radio">
    <label>
      <input type="radio" name="payment_address" value="new" />
      <?php echo $text_address_new; ?></label>
  </div>
  <?php } ?>
  <br />
  <div id="payment-new" style="display:"<?php echo (isset($addresses) ? 'none' : 'block'); ?>";">
	  <div class="col-md-4">
      <div class="form-group">
          <label class=" control-label" for="input-microdistrict">Микрорайон / Нас. пункт</label>
          <div class="col-sm-6">
              <input type="text" name="microdistrict" value="" placeholder="Микрорайон / Нас. пункт" id="input-microdistrict" class="form-control" />
          </div>
      </div>
	  <div class="form-group">
		  <label class="control-label" for="input-street">Улица</label>
		  <div class="col-sm-6">
			  <input type="text" name="street" value="" placeholder="Улица" id="input-street" class="form-control" />
		  </div>
	  </div>
      <div class="form-group required">
          <label class="control-label" for="input-house">Дом / Корп.</label>
          <div class="col-sm-6">
              <input type="text" name="house" value="" placeholder="Дом / Корп." id="input-house" class="form-control" />
          </div>
      </div>
      <div class="form-group">
          <label class="control-label" for="input-housing">Строение</label>
          <div class="col-sm-6">
              <input type="text" name="housing" value="" placeholder="Корпус" id="input-housing" class="form-control" />
          </div>
      </div>
  </div>
	  <div class="col-md-4">
		  <div class="form-group">
			  <label class="control-label" for="input-entrance">Подъезд</label>
			  <div class="col-sm-6">
				  <input type="text" name="entrance" value="" placeholder="Подъезд" id="input-entrance" class="form-control" />
			  </div>
		  </div>
		  <div class="form-group">
			  <label class="control-label" for="input-code">Код / Домофон</label>
			  <div class="col-sm-6">
				  <input type="text" name="code" value="" placeholder="Код / Домофон" id="input-code" class="form-control" />
		  </div>
		  </div>
		  <div class="form-group">
			  <label class="control-label" for="input-floor">Этаж</label>
			  <div class="col-sm-6">
				  <input type="text" name="floor" value="" placeholder="Этаж" id="input-floor" class="form-control" />
			  </div>
		  </div>
		  <div class="form-group">
			  <label class="control-label" for="input-flat">Квартира / Офис</label>
			  <div class="col-sm-6">
				  <input type="text" name="flat" value="" placeholder="Квартира / Офис" id="input-flat" class="form-control" />
			  </div>
		  </div>
	  </div>
  </div>
  <div class="buttons clearfix">
    <div class="pull-right">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-address" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
    </div>
  </div>
</form>
<script type="text/javascript"><!--
$('input[name=\'payment_address\']').on('change', function() {
	if (this.value == 'new') {
		$('#payment-existing').hide();
		$('#payment-new').show();
	} else {
		$('#payment-existing').show();
		$('#payment-new').hide();
	}
});
//--></script>
<script type="text/javascript"><!--
// Sort the custom fields
$('#collapse-payment-address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-payment-address .form-group').length) {
		$('#collapse-payment-address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#collapse-payment-address .form-group').length) {
		$('#collapse-payment-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#collapse-payment-address .form-group').length) {
		$('#collapse-payment-address .form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address button[id^=\'button-payment-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#collapse-payment-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#collapse-payment-address input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('#collapse-payment-address input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('#collapse-payment-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#collapse-payment-address select[name=\'country_id\']').trigger('change');
//--></script>
