<div id="cart" class="btn-group btn-block">
  <button type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-block btn-lg dropdown-toggle"><i class="fa fa-shopping-cart"></i> <span id="cart-total"><?php echo $text_items; ?></span></button>
  <ul class="dropdown-menu pull-right">
    <?php if ($products || $vouchers) { ?>
    <?php foreach ($products as $key =>  $product) { ?>
    <div class="form-inline product">
      <span>
        <?php if ($product['thumb']) { ?>
        <a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>);"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
        <?php } ?>
        <span class="name">
          <a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>)"><?php echo $product['name']; ?></a>
          <?php if ($product['my_option']) { ?>
          <?php foreach ($product['my_option'] as $key_op => $my_option) { ?>
          <?php if($my_option['type'] == 'size_it' || $my_option['type'] == 'size_am') { ?>
          <p style="color: black;">Размер: <?php echo $my_option['name']; ?></p>
          <input style="display: none" name="option_value_my_id_header[<?php echo $product['cart_id']; ?>][<?php echo $key_op ?>]" value="<?php if(isset($my_option['option_value_my_id'])) echo $my_option['option_value_my_id'] ?>" />
          <?php } else if($my_option['type'] == 'dough') { ?>
          <p style="color: black;">Тесто: <?php echo $my_option['name']; ?></p>
          <input style="display: none" name="option_value_my_id_header[<?php echo $product['cart_id']; ?>][<?php echo $key_op ?>]" value="<?php if(isset($my_option['option_value_my_id'])) echo $my_option['option_value_my_id'] ?>" />
          <?php } ?>
          <?php } ?>
          <?php } ?>
        </span>
        <input style="display: none" type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>">
        <p class="text-left">x <?php echo $product['quantity']; ?></p>
      </span>
      <p class="" style="display: inline-block; width: 15%; padding: 0; margin-left: 6px"><?php echo $product['total']; ?></p>
      <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-xs"><i class="fa fa-times"></i></button>
    </div>
    <?php } ?>
    <?php foreach ($vouchers as $voucher) { ?>
    <div><?php echo $voucher['description']; ?></div>
    <div>x&nbsp;1</div>
    <div><?php echo $voucher['amount']; ?></div>
    <button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>
    <?php } ?>
    <div class="total text-right">
      <?php foreach ($totals as $total) { ?>
        <p><?php echo $total['title']; ?>: <?php echo $total['text']; ?>р.</p>
      <?php } ?>
    </div>
    <div>
      <a href="<?php echo $cart; ?>"><i class="fa fa-shopping-cart"></i> В корзину</a>
      <!--<a href="<?php echo $checkout; ?>" id="checkout-link"><i class="fa fa-share"></i> Оформить</a>-->
    </div>
  </ul>
  <?php } else { ?>
  <p class="text-center"><?php echo $text_empty; ?></p>
  <?php } ?>
</ul>
</div>
<script>
  $( document ).ready(function() {
   $("ul a").each(function(index, value) {
    var link = $(this).attr("href");
    $(this).on('click', function() {
      if( link == '<?php echo $cart; ?>' ){
        location.href = link;
      }
    });
  });
 });
</script>
<!--
<script>
  $(document).delegate('#checkout-link', 'click', function() {
    $.ajax({
      url: 'index.php?route=checkout/cart/check',
      type: 'post',
      data: $('input[name^=\'option_value_my_id_header\'], input[name^=\'quantity\'] ,input[name^=\'price_total\'] '),
      dataType: 'json',
      beforeSend: function() {
        $('#button-checkout').button('Загрузка...');
      },
      complete: function() {
        $('#button-checkout').button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('.error-coup').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          //$('html, body').animate({ scrollTop: 0 }, 'slow');
        } else if (json['redirect']) {
          location = json['redirect'];
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
</script>
-->