<script src="catalog/view/javascript/common.js"></script>
<script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />


<footer>
  <div class="hr-line"></div>
  <div class="container">
    <div class="row hidden-xs" style="margin: 28px 0;">
      <div class="col-md-five col-xs-4">
        <div class="colums text-center">
          <a href="<?= $href.'7'?>">
            <h4>БОНУСНАЯ ПРОГРАММА</h4>
            <img src="catalog/view/theme/default/image/footer/bonus.png" alt="Bonus">
          </a>
          <p></p>
        </div>
      </div>
      <div class="col-md-five col-xs-4">
        <div class="colums text-center">
          <a href="<?php echo $href.'3'?>">
            <h4>ПОДАРОЧНЫЕ СЕРТИФИКАТЫ</h4>
            <img src="catalog/view/theme/default/image/footer/gift.png" alt="Gift">
          </a>
          <p></p>
        </div>
      </div>
      <div class="col-md-five col-xs-4">
        <div class="colums text-center">
          <a href="<?php echo $href.'5'?>">
            <h4>КАК ЗАКАЗАТЬ</h4>
            <img src="catalog/view/theme/default/image/footer/answer.png" alt="Answer">
          </a>
          <p></p>
        </div>
      </div>
      <div class="clearfix hidden-md hidden-lg"></div>
      <div class="col-xs-2 hidden-md hidden-lg"></div>
      <div class="col-md-five col-xs-4">
        <div class="colums text-center">
          <a href="<?php echo $href.'6'?>">
            <h4>КАРТА ДОСТАВКИ</h4>
            <img src="catalog/view/theme/default/image/footer/mappoint.png" alt="MapPoint">
          </a>
          <p></p>
        </div>
      </div>
      <div class="col-md-five center col-xs-4">
        <div class="colums text-center">
          <a href="<?php echo $href.'4'?>">
            <h4>КОНТАКТЫ</h4>
            <img src="catalog/view/theme/default/image/footer/contact.png" alt="Contacts">
          </a>
          <p></p>
        </div>
      </div>
    </div>
    <div class="row hidden-lg hidden-md hidden-sm">
      <div class="col-xs-12">
        <div id="carouselBonus" class="owl-carousel" style="opacity: 1;">
         <div class="item">
          <a href="<?php echo $href.'7'?>" clas>
            <h4>БОНУСНАЯ ПРОГРАММА</h4>
            <img src="catalog/view/theme/default/image/footer/bonus.png" alt="Bonus" class="img-responsive text-center">
          </a>
          <p></p>
        </div>
        <!-- Слайд №2 -->
        <div class="item">
          <a href="<?php echo $href.'3'?>">
            <h4>ПОДАРОЧНЫЕ СЕРТИФИКАТЫ</h4>
            <img src="catalog/view/theme/default/image/footer/gift.png" alt="Gift" class="img-responsive text-center">
          </a>
          <p></p>
        </div>
        <!-- Слайд №3 -->
        <div class="item">
          <a href="<?php echo $href.'5'?>">
            <h4>КАК ЗАКАЗАТЬ</h4>
            <img src="catalog/view/theme/default/image/footer/answer.png" alt="Answer" class="img-responsive text-center">
          </a>
          <p></p>
        </div>
        <!-- Слайд №4 -->
        <div class="item">
         <a href="<?php echo $href.'6'?>">
          <h4>КАРТА ДОСТАВКИ</h4>
          <img src="catalog/view/theme/default/image/footer/mappoint.png" alt="MapPoint" class="img-responsive text-center">
        </a>
        <p></p>
      </div>
      <!-- Слайд №5 -->
      <div class="item">
       <a href="<?php echo $href.'4'?>">
        <h4>КОНТАКТЫ</h4>
        <img src="catalog/view/theme/default/image/footer/contact.png" alt="Contacts" class="img-responsive text-center">
      </a>
      <p></p>
    </div>
  </div>
</div>
</div>
<hr>
<div class="container related">
  <div  class="row">
    <div class="col-md-4 col-sm-4 hidden-xs label-brand">
      <div" class="row brand">
      <div class="col-md-3 col-sm-3 img">
        <img src="catalog/view/theme/default/image/footer/logo.png" alt="Brand">
      </div>
      <div class="col-md-6 col-lg-6 col-sm-7">
        <a>
          <p class="brand-header1">Peoples Food</p>
          <p class="brand-header2">ГИПЕРМАРКЕТ ЕДЫ</p>
        </a>
      </div>
      <div class="col-md-2 col-sm-1 social">
      <a href="https://vk.com/pf2233444" style="margin-left: 0;">
        <img class="image-up" src="catalog/view/theme/default/image/footer/vk.png" alt="vk"/>
        <img class="image-down" src="catalog/view/theme/default/image/footer/vk_down.png" alt="vk"/>
        </a>
 <a href="https://www.instagram.com/pf2233444/" style="margin-left: 0;">
        <img class="image-up" src="catalog/view/theme/default/image/footer/insta.png" alt="Instagram">
        <img class="image-down" src="catalog/view/theme/default/image/footer/insta_down.png" alt="Instagram">
  </a>
        <img class="image-up" src="catalog/view/theme/default/image/footer/tube.png" alt="YouTube">
        <img class="image-down" src="catalog/view/theme/default/image/footer/tube_down.png" alt="YouTube">
      </div>
    </div>
    <div class="col-md-4 col-sm-4 text-center cards ">
      <p>Принимаем к оплате</p>
      <img class="hidden-xs hidden-sm" src="catalog/view/theme/default/image/footer/visa.png" alt="Brand">
      <img class="hidden-md hidden-lg" src="catalog/view/theme/default/image/footer/visa-mob1.png" alt="Brand1">
      <img class="hidden-md hidden-lg" src="catalog/view/theme/default/image/footer/visa-mob2.png" alt="Brand2">
      <a class="clearfix" href="<?= $href.'10'?>">Политика безопасности</a>
    </div>
    <div class="col-md-4 col-sm-4 hidden-xs enter">
      <div class="row" id="quick-login-footer">
        <?php if(!$login) { ?>
        <div class="col-md-8 col-sm-8">
          <p class="">Вход в личный кабинет</p>
          <div class="form-group required">
            <input value="Логин" onBlur="if(this.value=='')this.value='Логин'" onFocus="if(this.value=='Логин')this.value='' " type="text" name="email"   id="input-email-footer" class="form-control" />
          </div>
          <div class="form-group required">
            <input value="Пароль" onBlur="if(this.value=='')this.value='Пароль'" onFocus="if(this.value=='Пароль')this.value='' " type="password"  name="password"  id="input-password-footer" class="form-control" />
          </div>
        </div>
        <div class="col-sm-4 button-padding clearfix">
          <button type="button" class="btn text-center loginaccount-footer"  data-loading-text="Загрузка...">Вход</button>
        </div>
        <div class="col-sm-12">
          <div class="error"></div>
        </div>
        <?php } else { ?>
        <div class="col-md-11 col-sm-11 footer-logout">
          <a href="<?php echo $account ?>"> <span class="glyphicon glyphicon-user"></span> Перейти в личный кабинет</a>
          <a href="<?php echo $logout ?>"> <span class="glyphicon glyphicon-log-out"></span> Выход</a>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
</div>
<p class="text-center" style="margin: 72px 0">Copyright © Peoples Food, 2001-2018. Пользовательское соглашение</p>
</footer>
<?php if ($loadmore_status) { ?>
<div id="load_more" style="display:none;">
  <div class="row text-center">
    <a href="#" class="load_more" style="display:inline-block; margin:0 auto 20px auto; padding: 0.5em 2em; border: 1px solid #069; border-radius: 5px; text-decoration:none; text-transform:uppercase;"><?php echo $loadmore_button; ?></a>
  </div>
</div>
<?php } ?>
<?php if ($live_search_ajax_status):?>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/live_search.css" />
  <script type="text/javascript"><!--
  var live_search = {
    selector: '#search input[name=\'search\']',
    text_no_matches: '<?php echo $text_empty; ?>',
    height: '80px'
  }
  $('#carouselBonus').owlCarousel({
    items: 5,
    autoPlay: 3000,
    singleItem: true,
    navigation: false,
    navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
    pagination: false
  });

  $(document).ready(function() {
    var html = '';
    html += '<div class="live-search">';
    html += '	<ul>';
    html += '	</ul>';
    html += '<div class="result-text"></div>';
    html += '</div>';

//$(live_search.selector).parent().closest('div').after(html);
$(live_search.selector).before(html);

$(live_search.selector).autocomplete({
  'source': function(request, response) {
    var filter_name = $(live_search.selector).val();
    var live_search_min_length = '<?php echo (int)$live_search_min_length; ?>';
    if (filter_name.length < live_search_min_length) {
      $('.live-search').css('display','none');
    }
    else{
      var html = '';
      html += '<li style="text-align: center;height:10px;">';
      html +=	'<img class="loading" src="catalog/view/theme/default/image/loading.gif" />';
      html +=	'</li>';
      $('.live-search ul').html(html);
      $('.live-search').css('display','block');

      $.ajax({
        url: 'index.php?route=product/live_search&filter_name=' +  encodeURIComponent(filter_name),
        dataType: 'json',
        success: function(result) {
          var products = result.products;
          $('.live-search ul li').remove();
          $('.result-text').html('');
          if (!$.isEmptyObject(products)) {
            var show_image = <?php echo $live_search_show_image;?>;
            var show_price = <?php echo $live_search_show_price;?>;
            var show_description = <?php echo $live_search_show_description;?>;
            $('.result-text').html('<a href="<?php echo $live_search_href;?>'+filter_name+'" class="view-all-results"><?php echo $text_view_all_results;?> ('+result.total+')</a>');

            $.each(products, function(index,product) {
              var click = 'onclick=getProduct('+parseInt(product.product_id)+')';
              var html = '';
              html += '<li>';
              html += '<a style="cursor:pointer;" '+click+'>';
              if(product.image && show_image){
                html += '	<div class="product-image"><img alt="' + product.name + '" src="' + product.image + '"></div>';
              }
              html += '	<div class="product-name">' + product.name ;
              if(show_description){
                html += '<p>' + product.extra_info + '</p>';
              }
              html += '</div>';
              if(show_price){
                if (product.special) {
                  html += '	<div class="product-price"><span class="special">' + product.price + '</span><span class="price">' + product.special + '</span></div>';
                } else {
                  html += '	<div class="product-price"><span class="price">' + product.price + '</span></div>';
                }
              }
              html += '<span style="clear:both"></span>';
              html += '</a>';
              html += '</li>';
              $('.live-search ul').append(html);
            });
          } else {
            var html = '';
            html += '<li style="text-align: center;height:10px; color: black; font-size: larger">';
            html +=	live_search.text_no_matches;
            html +=	'</li>';

            $('.live-search ul').html(html);
          }
          $('.live-search ul li').css('height',live_search.height);
          $('.live-search').css('display','block');
          return false;
        }
      });
    }
  },
  'select': function(product) {
    $(live_search.selector).val(product.name);
  }
});

$(document).bind( "mouseup touchend", function(e){
  var container = $('.live-search');
  if (!container.is(e.target) && container.has(e.target).length === 0)
  {
    container.hide();
  }
});
});
//--></script>
<?php endif;?>
<script type="text/javascript"><!--
$('#quick-login-footer input').on('keydown', function(e) {
  if (e.keyCode == 13) {
    $('#quick-login-footer .loginaccount-footer').trigger('click');
  }
});
$('#quick-login-footer .loginaccount-footer').click(function() {
  $.ajax({
    url: 'index.php?route=common/quicksignup/login',
    type: 'post',
    data: $('#quick-login-footer input[type=\'text\'], #quick-login-footer input[type=\'password\']'),
    dataType: 'json',
    beforeSend: function() {
      $('#quick-login-footer .loginaccount-footer').button('loading');
      //$('#modal-quicksignup .alert-danger').remove();
    },
    complete: function() {
      $('#quick-login-footer .loginaccount-footer').button('reset');
    },
    success: function(json) {
      $('#quick-login-footer .form-group').removeClass('has-error');
      if(json['islogged']){
        window.location.href="index.php?route=account/account";
      }

      if (json['error']) {
    //Вот здесь надо добавить свой код
    if(!$('*').is('#quick-login-footer .alert-danger'))
      $('#quick-login-footer .error').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
    $('#quick-login-footer #input-email-footer').parent().addClass('has-error');
    $('#quick-login-footer #input-password-footer').parent().addClass('has-error');
    $('#quick-login-footer #input-email-footer').focus();
  }
  if(json['success']){
    loacation();
    //$('#modal-quicksignup').modal('hide');
  }

}
});
});
//--></script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '529476220830553'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=529476220830553&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</body></html>
