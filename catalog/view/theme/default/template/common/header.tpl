<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/jquery/masked-input/jquery.maskedinput.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/style.css" rel="stylesheet">
    <script src="catalog/view/javascript/jquery/jquery.mask.js" type="text/javascript"></script>
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
    <!--<script type="text/javascript" src="catalog/view/javascript/ajax-product-page-loader.js"></script>
    <style>
    #ajax_loader {
        width: 100%;
        height: 30px;
        margin-top: 15px;
        text-align: center;
        border: none!important;
    }
    .arrow_top {
        background: url("../../../../../image/chevron_up.png") no-repeat transparent;
        background-size: cover;
        position: fixed;
        bottom: 50px;
        right: 15px;
        cursor: pointer;
        display: block;
        height: 50px;
        width: 50px;
    }
</style>-->
</head>
<body class="<?php echo $class; ?>">
    <header>
    <div class="old-site">
     <div class="container alert text-center">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">Закрыть</a>
        <a href="http://www.2233444.ru/">Перейти на другую версию сайта</a>
        </div>
  </div>
        <div class="container header">
            <div class="row navbar">
                <div class="col-md-3 col-sm-5 col-xs-4 lateral-padding-0">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-7 lateral-padding-0">
                            <a href="<?= HTTP_SERVER ?>" class="img"><img src="catalog/view/theme/default/image/header/logo.png" alt=""></a>
                        </div>
                        <div class="col-md-6 col-sm-6 brand hidden-xs lateral-padding-0">
                            <a class="text-center" href="<?= HTTP_SERVER ?>">People's Food</a>
                            <p class="text-center">Гипермаркет еды</p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5 menu lateral-padding-0 text-center">
                            <div class="dropdown">
                                <button class="btn btn-primary  btn-xs dropdown-toggle burger" type="button" data-toggle="dropdown">
                                    <i class="fa fa-bars"></i></button>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-header">Основное меню</li>
                                        <?php foreach($categories as $category) { ?>
                                        <li><a href="<?php echo $category['href']?>"><?php echo $category['name']?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-2 hidden-xs lateral-padding-0" id="mark-search">
                <?php echo $search; ?>
            </div>
            <div class="col-md-3 col-sm-5  col-xs-7 lateral-padding-0" style="float: right">
                <div class="col-md-6  col-sm-6 col-xs-6 telephone text-center lateral-padding-0">
                    <p class="text-center">8 (495) 22-33-444</p>
                    <p class="text-center">8 (925) 22-33-444</p>
                </div>

                <div class="col-md-2 col-sm-2 col-xs-2 enter lateral-padding-0">
                    <div class="hidden-sm hidden-md hidden-lg ">
                     <a id="mob-search" onclick="mobSearchToggle()"><i class="fa fa-search"></i></a>
                 </div>
                 <div class="dropdown">
                    <button class="btn btn-xs btn-block dropdown-toggle" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span></button>
                    <ul class="dropdown-menu">
                        <?php if ($logged) { ?>
                        <li><a href="<?php echo $account; ?>">Личный кабинет</a></li>
                        <li><a href="<?php echo $logout; ?>">Выход</a></li>
                        <?php } else { ?>
                        <li>
                            <p class="enter-modal-link" data-toggle="modal" data-target="#modal-quicksignup">Вход / Регистрация</p>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
         <!--
         </div> -->
         <div class="col-md-2 col-sm-2 col-xs-3 pull-right cart lateral-padding-0">
            <?php echo $cart ?>
        </div>
    </div>
</div>
</div>
<?php echo $quicksignup; ?>
<!--  Модальное окно -->
<div class="modal fade" id="modalProduct">
</div>
<script>
$( document ).ready(function() {
$("input[name$='telephone']").mask("8-000-000-00-00");
$("input[name$='fax']").mask("8-000-000-00-00");
$("input[name$='birthday']").mask("00.00.0000");
});
function mobSearchToggle() {
    var state = $('#search').css('display');
    if (state == 'none')
        $('#search').slideDown();
    else
        $('#search').slideUp();
}
</script>

</header>


