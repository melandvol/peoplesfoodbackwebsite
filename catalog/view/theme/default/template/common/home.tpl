<?php echo $header; ?>
<div class="container">
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div  class="<?php echo $class; ?>"><?php echo $content_top; ?></div>
    <?php echo $column_right; ?></div>
  </div>
  <div class="play-icon hidden-sm hidden-md hidden-lg">
    <a href='<?= HTTP_SERVER ?>index.php?route=tool/log_app/linkGooglePlay'>
      <img alt='Доступно в Google Play' src='catalog/view/theme/default/image/google-play-store.svg'/> Скачать
    </a>
  </div>
  <div class="hidden-xs hidden-sm col-md-12 clearfix hr-line"> </div>
  <!--Модуль с тремя картинками-->
  <script>
    $(document).ready(function(e) {
      $("img[usemap]").mapTrifecta({
        zoom: true,
        table: true,
        fill: true,
        fillColor: 'bfbfbf',
        fillOpacity: 0.15,
        stroke: false,
        strokeColor: 'bfbfbf',
        strokeOpacity: 1,
        strokeWidth: 1,
        fade: true,
        alwaysOn: false,
        neverOn: false,
        groupBy: false,
        wrapClass: true,
        shadow: false,
        shadowX: 0,
        shadowY: 0,
        shadowRadius: 10,
        shadowColor: '000000',
        shadowOpacity: 0.8,
        shadowPosition: 'outside',
        shadowFrom: false
      });

      var userAgent = navigator.userAgent || navigator.vendor || window.opera;
      if (!/android/i.test(userAgent)) {
          $(".play-icon").hide()
      }
    });
  </script>
  <div class="text-center" id="three-img-home">
   <img src="catalog/view/theme/default/image/three_img_dev.png" width="1000" height="643" usemap="#three">
   <map name="three">
    <area onclick="" data-mapid="1" shape="poly" coords="230,62,510,342,229,623,0,623,3,64,3,61"
    title="Приготовь сам" alt="Приготовь сам" />
    <area onclick="window.open('<?= HTTP_SERVER ?>index.php?route=product/filtr', '_self');" data-mapid="2" shape="poly" coords="230,1,230,61,510,341,789,60,788,0"
    title="Выбери еду" alt="Выбери еду" />
    <area onclick="window.open('<?= HTTP_SERVER ?>index.php?route=module/faq', '_self');" data-mapid="3" shape="poly" coords="510,342,789,622,998,623,999,61,791,59"
    title="Спроси нас" alt="Спроси нас" />
  </map>
</div>

<!--Скрипт для ресайза изображений-->
 <script src="catalog/view/javascript/map-trifecta/jquery.map-trifecta.js"></script>
<link href="catalog/view/theme/default/stylesheet/map-trifecta.css">
<?php echo $footer; ?>
