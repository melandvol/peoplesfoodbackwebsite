<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/moment.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css">

<div id="modal-quicksignup" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="error"></div>
		<div class="error-register"></div>
		<div class="modal-content">
			<div class="modal-header clearfix">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<p class="text-center">Закрыть</p>
					<i class="glyphicon glyphicon-remove-circle"></i></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-sm-5 text-center" id="quick-login">
								<h4>Вход</h4>
								<div class="form-group required">
									<input value="Логин" onBlur="if(this.value=='')this.value='Логин'" onFocus="if(this.value=='Логин')this.value='' " type="text" name="email"   id="input-email" class="form-control" />
								</div>
								<div class="form-group required">
									<input value="Пароль" onBlur="if(this.value=='')this.value='Пароль'" onFocus="if(this.value=='Пароль')this.value='' " type="password"  name="password"  id="input-password" class="form-control" />
								</div>
								<a href="<?php echo $forgotten; ?>" class="password">Забыли пароль?</a>
								<button type="button" class="btn btn-primary loginaccount"  data-loading-text="Загрузка...">Вход</button>
								<button id="mob-register" class="btn registration hidden-sm hidden-md hidden-lg">Регистрация</button>
							</div>

							<div class="col-sm-1 hidden-xs"></div>
							<div class="col-sm-1 line-shadow  hidden-xs"></div>

							<div class="col-sm-5 text-center hidden-xs" id="quick-register">
								<div id="ending-register">
									<h4>Регистрация</h4>
									<div class="form-group required">
										<input type="text" name="firstname" value="Имя" id="input-firstname" class="form-control" onBlur="if(this.value=='')this.value='Имя'" onFocus="if(this.value=='Имя')this.value=''"/>
									</div>

									<div class="form-group required">
										<input type="text" name="email" value="E-mail" id="input-email" class="form-control" onBlur="if(this.value=='')this.value='E-mail'" onFocus="if(this.value=='E-mail')this.value='' "/>
									</div>

									<div class="form-group required">
										<input type="text" name="telephone" placeholder="Телефон" id="input-telephone" class="form-control"/>
									</div>
									<div class="form-group">
										<input type="text" name="fax" placeholder="Дополнительный телефон" id="input-fax" class="form-control"/>
									</div>
									<div class="form-group required">
										<div class="input-group date">
											<input type="text" name="birthday" value=""  placeholder="День рождения" id="input-date-available" class="form-control" />
											<span class="input-group-btn">
												<button class="btn btn-default" style="padding: 7px 12px; background: #6ca824; color: white;" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>

									<div class="form-group required">
										<input type="password" name="password" value="Пароль" id="input-password" class="form-control" onBlur="if(this.value=='')this.value='Пароль'" onFocus="if(this.value=='Пароль')this.value='' "/>
									</div>
									<div class="form-group required">
										<input type="password" name="confirm" value="Пароль" id="input-confirm" class="form-control" onBlur="if(this.value=='')this.value='Пароль'" onFocus="if(this.value=='Пароль')this.value='' "/>
									</div>

									<button type="button" class="btn btn-primary pull-right createaccount" data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_continue; ?></button>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style>
	.quick_signup{
		cursor:pointer;
	}
	#modal-quicksignup .form-control{
		height:auto;
	}
</style>
<script type="text/javascript"><!--
$(document).delegate('.quick_signup', 'click', function(e) {
	$('#modal-quicksignup').modal('show');

});
$(function(){
	$("#mob-register").click(function() {
		$('#quick-login').addClass('hidden-xs');
		$('#modal-quicksignup .alert-danger').remove();
		$('#quick-register').removeClass('hidden-xs');
	});
});
$(document).mouseup(function (e){
	var div = $("#modal-quicksignup");
	if (!div.is(e.target) && div.has(e.target).length === 0 && !$('div').hasClass('picker-open')) {
		$('#modal-quicksignup .alert-danger').remove();
		$('#quick-login').removeClass('hidden-xs')
		$('#modal-quicksignup .form-group').removeClass('has-error');
		$('#quick-register').addClass('hidden-xs');
	}
});
//--></script>
<script type="text/javascript"><!--
$('#quick-register input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-register .createaccount').trigger('click');
	}
});
$('#quick-register .createaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/register',
		type: 'post',
		data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-register .createaccount').button('loading');
			$('#modal-quicksignup .alert-danger').remove();
		},
		complete: function() {
			$('#quick-register .createaccount').button('reset');
		},
		success: function(json) {
			$('#modal-quicksignup .form-group').removeClass('has-error');

			if(json['islogged']){
				window.location.href="index.php?route=account/account";
			}
			if (json['error_name']) {
				$('#quick-register #input-firstname').parent().addClass('has-error');
				$('#quick-register #input-firstname').focus();
				//return;
			}
			if (json['error_email']) {
				$('#quick-register #input-email').parent().addClass('has-error');
				$('#quick-register #input-email').focus();
				//return;
			}
			if (json['error_telephone']) {
				$('#quick-register #input-telephone').parent().addClass('has-error');
				$('#quick-register #input-telephone').focus();
				//return;
			}
			if (json['error_password']) {
				$('#quick-register #input-password').parent().addClass('has-error');
				$('#quick-register #input-password').focus();
				//return;
			}
			if (json['error_confirm']) {
				$('#quick-register #input-confirm').parent().addClass('has-error');
				$('#quick-register #input-confirm').focus();
				//return;
			}
			if (json['error_birthday']) {
				$('#quick-register #input-birthday-modal').parent().addClass('has-error');
				$('#quick-register #input-birthday-modal').focus();
				//return;
			}
			if (json['error']) {
				if(!$('*').is('#modal-quicksignup .alert-danger .register'))
					$('#modal-quicksignup .error-register').after('<div class="alert alert-danger register"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['now_login']) {
				$('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');

				$('.quick-login').remove();
			}
			if (json['success']) {
				$('#ending-register').hide();
				$('#modal-quicksignup .alert-danger').remove();
				$('.error-register').css('margin-bottom', '48px');
				$('#modal-quicksignup .error-register').html(json['heading_title']);
				success = json['text_message'];
				success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ "Завершить" +'</a></div></div>';
				$('#modal-quicksignup #quick-register').html(success);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#quick-login input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-login .loginaccount').trigger('click');
	}
});
$('#quick-login .loginaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/login',
		type: 'post',
		data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-login .loginaccount').button('loading');
			$('#modal-quicksignup .alert-danger').remove();
		},
		complete: function() {
			$('#quick-login .loginaccount').button('reset');
		},
		success: function(json) {
			$('#modal-quicksignup .form-group').removeClass('has-error');
			if(json['islogged']){
				window.location.href="index.php?route=account/account";
			}

			if (json['error']) {
				if(!$('*').is('#modal-quicksignup .alert-danger'))
					$('#modal-quicksignup .error').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');

				$('#quick-login #input-email').parent().addClass('has-error');
				$('#quick-login #input-password').parent().addClass('has-error');
				$('#quick-login #input-email').focus();
			}
			if(json['success']){
				loacation();
				$('#modal-quicksignup').modal('hide');
			}

		},
		//Error ajax
		error: function (jqXHR, exception) {
			console.log(jqXHR);
			// Your error handling logic here..
		}

	});
});
	/*$( document ).ready(function() {
		$(function() {
			$("#input-birthday").mask("9999.99.99", {placeholder: ""});
			$("#input-telephone").mask("+7(999) 999-9999",  {placeholder: "+7(000) 999-9999" });
		});
	});*/
	//--></script>
	<script type="text/javascript"><!--
	function loacation() {
		if (getURLVar('route') == 'account/logout')
			location = '';
		else
			location.reload();
	}
	//--></script>
	<script>
		$(document).on('click', '#myModal', function(){
			$('#modal-quicksignup').modal('show');
		});
	</script>
	<script type="text/javascript">
		$('.date').datetimepicker({
			pickTime: false
		});
		var zIndexModal = $('#modal-quicksignup').css('z-index');
		$('.bootstrap-datetimepicker-widget').css('z-index', zIndexModal + 1);
		var a;
	</script>
