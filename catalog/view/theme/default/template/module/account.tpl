<div class="list-group-account">
  <?php if (!$logged) { ?>
  <a href="<?php echo $login; ?>" class="list-group-item"><?php echo $text_login; ?></a>
  <a href="<?php echo $register; ?>" class="list-group-item"><?php echo $text_register; ?></a>
  <a href="<?php echo $forgotten; ?>" class="list-group-item"><?php echo $text_forgotten; ?></a>
  <?php } ?>
  <h4>Моя учетная запись</h4>
  <a href="<?php echo $account; ?>" class="list-group-item">Контактная информация</a>
  <?php if ($logged) { ?>
  <a href="<?php echo $edit; ?>" class="list-group-item"><?php echo $text_edit; ?></a>
  <a href="<?php echo $password; ?>" class="list-group-item"><?php echo $text_password; ?></a>
    <a href="<?php echo $address; ?>" class="list-group-item">Мои Адреса</a>
  <a href="<?php echo $wishlist; ?>" class="list-group-item">Мои закладки</a>
  <?php } ?>
  <h4>Мои заказы</h4>
  <a href="<?php echo $order; ?>" class="list-group-item"><?php echo $text_order; ?></a>
  <a href="<?php echo $reward; ?>" class="list-group-item"><?php echo $text_reward; ?></a>
  <a href="<?php echo $newsletter; ?>" class="list-group-item">E-Mail, SMS рассылка</a>
  <?php if ($logged) { ?>
  <a href="<?php echo $logout; ?>" class="list-group-item"><?php echo $text_logout; ?></a>
  <?php } ?>
</div>
