<div class="left-category">
  <?php foreach ($categories as $category) { ?>
  <a type="button" data-toggle=«buttons-radio» href="<?php echo $category['href']; ?>" class="btn btn-block <?php if ($category['category_id'] == $category_id) echo 'active' ?>"><?php echo $category['name']; ?></a>
  <?php if ($category['children']) { ?>
  <?php foreach ($category['children'] as $child) { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item <?php if ($child['category_id'] == $child_id) echo 'active'?> ">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } ?>
  <?php } ?>
  <?php } ?>
  <?php echo $filter ?>
</div>
<script type="text/javascript">
    if (($('.left-category').children('a').length == 0) && ($('.left-category').children('div').length == 0))
      $('.left-category').hide();

</script>
