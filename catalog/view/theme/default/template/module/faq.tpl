<?php echo $header; ?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div id="content">
                <div class="text-center"><h3>ВОПРОС - ОТВЕТ</h3></div>

                <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
                <?php } ?>
                <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                <?php } ?>

                <?php if ($answers): ?>
                    <div>
                        <?php foreach ($answers as $answer): ?>
                            <div>
                                <div id="heading<?=$answer['id']?>">
                                    <h4 class="panel-title"><?=$answer['title']?></h4>
                                </div>
                                <div class="admin-answer" id="collapse<?=$answer['id']?>">
                                    <div class="panel-body">
                                        <p>Ответ Администратора:</p>
                                        <?php if ($answer['description']): ?>
                                            <?=html_entity_decode($answer['description'])?>
                                        <?php else: ?>
                                            <?=$answer_empty?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div><br>
                        <?php endforeach; ?>
                    </div>
                <?php else: ?>
                    <p><?php echo $questions_empty; ?></p>
                <?php endif; ?>
                <form enctype="multipart/form-data" class="form-signin" method="POST" action="<?php echo $action ?>">
                    <div class="input-group send-question">
                        <textarea type="text" name = "faq_question[1][title]" class="form-control" placeholder="Задайте свой вопрос..."></textarea>
                        <span class="input-group-btn">
                            <button class="btn btn-success" type="submit">Спросить</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="text-center">
        <?php echo $pagination; ?>
    </div>
    <div class="text-center">
        <?php echo $results; ?>
    </div>
</div>
<?php echo $footer; ?>
