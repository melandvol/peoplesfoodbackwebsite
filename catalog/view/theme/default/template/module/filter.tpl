<div class="panel panel-default">
  <!-- <div class="panel-heading"><?php echo $heading_title; ?></div> -->
  <div class="list-group">
    <?php foreach ($filter_groups as $filter_group) { ?>
    <p class="list-group-item parent"><?php echo $filter_group['name']; ?></p>
    <div class="list-group-item">
      <div id="filter-group<?php echo $filter_group['filter_group_id']; ?>">
        <?php foreach ($filter_group['filter'] as $filter) { ?>
          <?php if(strnatcasecmp($filter_group['name'],'Выбор кухни') != 0) { ?>
            <!--Тут редактирую то что нужно для всего кроме выбора кухни-->
            <a class="list-group-item child" href="<?php echo $filter['action']; ?>" ><?php echo $filter['name']; ?></a>
          <?php } else { ?>
            <!--а тут редактирую то что нужно для выбора кухни, там добавится скрывающийся список-->
            <a class="list-group-item child" href="<?php echo $filter['action']; ?>" ><?php echo $filter['name']; ?></a>
          <?php } ?>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
