<?php echo $header; ?>
<div class="filter">
  <div class="container">
    <h3 class="text-center">выбери еду</h3>

    <ul class="hidden-xs nav nav-pills nav-category text-center">
      <?php foreach ($categories as $category) if(strnatcasecmp($category['name'],'фильтр') != 0){ ?>

      <li><a href="<?php echo $category['href'];?>"><?php echo $category['name']; ?></a></li>
      <?php } ?>
    </ul>
      <ul class="hidden-lg hidden-md hidden-sm nav nav-pills nav-mob" style="margin-top: 48px;" >
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Выберите товар<b class="caret"></b></a>
        <ul class="dropdown-menu">
         <?php foreach ($categories as $category) if(strnatcasecmp($category['name'],'фильтр') != 0){ ?>
         <li><a href="<?php echo $category['href'];?>"><?php echo $category['name']; ?></a></li>
         <?php } ?>
            <li><a href="<?php echo $special ?>" style=" color: #d21405;"> Товары по акции </a></li>
       </ul>
     </li>
   </ul>
   <div class="row menu-type">
    <?php foreach ($filter_groups as $filter_group) if($filter_group['filter_group_id'] != 1){ ?>
    <div class="col-sm-<?php if($filter_group['filter_group_id'] == 2 || $filter_group['filter_group_id'] == 3) echo 2; else echo 4;?>">
      <ul class="nav nav-list">
        <li class="nav-header"><?php echo $filter_group['name']; ?>:</li>
        <?php foreach ($filter_group['filter'] as $filter) { ?>
        <li>
          <div class="center-img">
           <img src="catalog/view/theme/default/image/filtr/filtr<?php echo $filter['filter_id'] ?>.png">
         </div>
         <a href="<?php echo $filter['action']?>"><?php echo $filter['name']; ?></a></li>
         <?php } ?>
       </ul>
     </div>
     <?php } ?>
   </div>
   <div class="row select-kitchen">
     <?php foreach ($filter_groups as $filter_group) if($filter_group['filter_group_id'] == 1){ ?>
     <div id="b1" class="col-md-8 col-sm-12">
      <div class="catalog-map hidden-xs">
        <img class="map-global" src="catalog\view\theme\default\image\filtr\map\map.png">
        <img id="mark" class="mark display-none" src="catalog\view\theme\default\image\filtr\map\icon.png" alt=""/>
      </div>
    </div>
    <div id="b2" class="col-md-4 col-sm-12">
     <p class="nav-header"><?php echo $filter_group['name']; ?>:</p>
     <ul class="nav nav-list select-kitchen">
      <?php foreach ($filter_group['filter'] as $filter) { ?>
      <li ><a href="<?php echo $filter['action']?>" id="kitchen<?php echo $filter['filter_id'] ?>" ><?php echo $filter['name']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php }?>
</div>
</div>
</div>
<?php echo $footer; ?>