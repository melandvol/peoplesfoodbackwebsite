<?php foreach($product_ingredients as $product_ingredients) { ?>
    <p><?php echo $product_ingredients['name'] ?></p>
        <?php foreach($product_ingredients['ingredients'] as $ingredients) { ?>
        <li class="ingredients-menu clearfix">
            <p class="pull-left" href="#"><?php echo $ingredients['name']?></p>
            <?php $ingredients['main_cast'] == 1 ? $main = true: $main = false ?>
            <button class="btn pull-right" onclick="add_rem_ingredient('modal',<?php echo $ingredients['price']?>,false,<?php echo $ingredients['ingredients_id']?>,<?php echo $main ?>)">-</button>
            <button class="btn pull-right" onclick="add_rem_ingredient('modal',<?php echo $ingredients['price']?>,true,<?php echo $ingredients['ingredients_id']?>,<?php echo $main ?>)">+</button>
            <input name="ingredients[modal][quantity][<?php echo $ingredients['ingredients_id']?>]" type="text" maxlength="5" class="pull-right" style="background: #6ca824;" value="<?php echo $ingredients['quantity']?>" readonly>
            <span id="hide"></span><input name="ingredients[modal][price][<?php echo $ingredients['ingredients_id']?>]" type="text" maxlength="7" class="pull-right" id="txt" value="+<?php echo $ingredients['price']?>р." style="background: #eb9316;" readonly>
        </li>
    <?php } ?>
<?php } ?>