
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header clearfix">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        <p class="text-center">Закрыть</p>
        <i class="glyphicon glyphicon-remove-circle"></i></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-7 hidden-xs" >
              <img src="<?php echo $product['thumb'] ?>" class="img-responsive modal-img">
            </div>
            <div class="col-sm-1 hidden-xs line-shadow"></div>
            <div class="col-sm-4 col-xs-12 product-thumb modal-thumb">
              <div class="hidden-md hidden-sm hidden-lg">
                <img src="<?php echo $product['thumb'] ?>" class="img-responsive modal-img">
              </div>
              <h4 class="modal-title"><?php echo $product['name']?></h4>
              <?php echo $product['description'] ?>
              <!-- Количество пицц и ингридиенты -->
              <div class="form-inline clearfix">
                <input type="text" maxlength="4" name="quantity[modal]" value="1" class="text-center modal-input-fix" id="number-of-productmodal">
                <button type="button" class="btn" onclick="add_rem_product('modal',<?php echo $product['price']?>,false,<?php echo $product['special']?>)"><span class="glyphicon glyphicon-minus btn-sm"></span></button>
                <button type="button" class="btn" onclick="add_rem_product('modal',<?php echo $product['price']?>,true,<?php echo $product['special']?>)"><span class="glyphicon glyphicon-plus btn-sm"></span></button>
                <div class="btn-group dropup">
                  <?php if($product['product_ingredients']) { ?>
                  <a href="" data-toggle="dropdown" onclick="resizeIngridientsModal()">Состав</a>
                  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" onclick="resizeIngridientsModal()">+</button>
                  <ul class="dropdown-menu noclose" role="menu" id="product-ingredients-modal">
                    <?php foreach($product['product_ingredients'] as $product_ingredients) { ?>
                    <p><?php echo $product_ingredients['name'] ?></p>
                    <?php foreach($product_ingredients['ingredients'] as $ingredients) { ?>
                    <li class="ingredients-menu clearfix">
                      <p class="pull-left" href="#"><?php echo $ingredients['name']?></p>
                      <?php $ingredients['main_cast'] == 1 ? $main = true: $main = false ?>
                      <button class="btn pull-right" onclick="add_rem_ingredient('modal',<?php echo $ingredients['price']?>,false,<?php echo $ingredients['ingredients_id']?>,<?php echo $main ?>)">-</button>
                      <button class="btn pull-right" onclick="add_rem_ingredient('modal',<?php echo $ingredients['price']?>,true,<?php echo $ingredients['ingredients_id']?>,<?php echo $main ?>)">+</button>
                      <input name="ingredients[modal][quantity][<?php echo $ingredients['ingredients_id']?>]" type="text" maxlength="5" class="pull-right" style="background: #6ca824;" value="<?php echo $ingredients['quantity']?>" readonly>
                      <span id="hide"></span><input name="ingredients[modal][price][<?php echo $ingredients['ingredients_id']?>]" type="text" maxlength="7" class="pull-right" id="txt" value="+<?php echo $ingredients['price']?>р." style="background: #eb9316;" readonly>
                    </li>
                    <?php } ?>
                    <?php } ?>
                  </ul>
                  <?php } ?>
                  <?php if (!$product['option'] && empty(!$product['weight'])) { ?>
                  <p class="weight" id="weight-modal"><?php echo $product['weight']?></p>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group clearfix">
                <!-- Это размер или тип теста пиццы если есть-->
                <?php if ($product['option']) { ?>
                <input style="display: none" name="product_my_option_value_id[modal]" value="<?php if(isset($product['option']['product']['product_my_option_value_id'])) echo $product['option']['product']['product_my_option_value_id'] ?>" />
                <?php foreach ($product['option'] as $option_arr_key => $option_arr) if($option_arr_key != 'product'){ ?>
                <?php if($option_arr_key == 'size_it' || $option_arr_key == 'size_am') { ?>
                <div class="form-group clearfix">
                  <?php } else if($option_arr_key == 'dough') { ?>
                  <div class="form-group center-block clearfix">
                    <?php }?>
                    <select name="product_option_name[modal][<?php echo $option_arr_key?>]" id="<?php echo $option_arr_key?>-modal" class="form-control" onchange="changePizza('modal',<?php echo $product['product_id']; ?>);">
                      <?php foreach ($option_arr as $option_arr) { ?>
                      <option value="<?php echo $option_arr['option_value_my_id']; ?>"><?php echo $option_arr['name'];?></option>
                      <?php } ?>
                    </select>
                    <?php if($option_arr_key == 'size_it' || $option_arr_key == 'size_am') { ?>
                    <p id="weight-modal"><?php echo $product['weight']?></p>
                    <?php }?>

                  </div>
                  <?php }?>
                  <?php }?>

                </div>
                <div class="order-group clearfix">
                  <div class="button-group">
                    <!-- Это кнопки: купить, в закладки-->
                    <button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('<?php echo $product['product_id']; ?>',true);"><i class="fa fa-heart"></i></button>
                    <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>','modal');"><span class="">Купить</span></button>
                  </div>
                  <!-- Это цена продукта-->
                  <?php if ($product['price']) { ?>
                  <?php if ($product['special'] == 0) { ?>
                  <p class="price" id="price-modal"><?php echo $product['price']; ?>р.</p>
                  <?php } else { ?>
                  <p class="price-special">
                    <span class="price-new" id="price-new-modal"><?php echo $product['special']; ?></span>
                    <span class="price-old" id="price-old-modal"><?php echo $product['price']; ?>р.</span>
                  </p>
                  <?php } ?>

                  <?php } ?>
                </div>
                <input style="display: none" name="product_id[modal]" value="<?php echo $product['product_id'] ?>" />
              <!-- <div class="share-friends">
                  <p>поделиться с другом:</p>
                  <div class="form-inline text-center">
                    <img src="catalog/view/theme/default/image/filtr/social/vk.png"" alt="Вконтакте" title="Вконтакте">
                    <img src="catalog/view/theme/default/image/filtr/social/fb.png"" alt="Facebook" title="Facebook">
                    <img src="catalog/view/theme/default/image/filtr/social/ok.png"" alt="Одноклассники" title="Одноклассники">
                    <img src="catalog/view/theme/default/image/filtr/social/msg.png"" alt="хз че" title="хз че">
                    <img src="catalog/view/theme/default/image/filtr/social/twtr.png"" alt="Twitter" title="Twitter">
                    <img src="catalog/view/theme/default/image/filtr/social/viber.png"" alt="Viber" title="Viber">
                    <img src="catalog/view/theme/default/image/filtr/social/wtsup.png"" alt="WhatsUp" title="WhatsUp">
                    <img src="catalog/view/theme/default/image/filtr/social/skype.png"" alt="Skype" title="Skype">
                  </div>
                </div> -->
              </div>
            </div>
          </div>
        </div>
        <div class="success"></div>
      </div>
    </div>

    <script>
      $(document).ready(function() {
        modal_ingredient = 0;
      // Для кнопок +-, а то почему то из CSS не скрываются
      // Для удержания меню открытым
      // $(document).on('click', '.dropdown-menu', function (event) {
      //   event.preventDefault();
      //   event.stopPropagation();
      // });
      $( "#product-ingredients-modal li" ).hover(
        function() {
          $( this ).children('button').css('display', 'inline-block');
          $( this ).children('span:last').css('display', 'none');
        }, function() {
          $( this ).children('button').css('display', 'none');
          $( this ).children('span:last').css('display', 'inline-block');
        }
        );
      product_option_modal = JSON.parse('<?php if(isset($json_option_modal)) echo $json_option_modal ?>');
    });
  </script>
