<?php echo $header; ?>
<div class="container">
<!--   <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> -->
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
      <div class="row">
        <div class="col-sm-3">
          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
        </div>
        <div class="col-sm-3">
          <select name="category_id" class="form-control">
            <option value="0"><?php echo $text_category; ?></option>
            <?php foreach ($categories as $category_1) { ?>
            <?php if ($category_1['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if ($category_2['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if ($category_3['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-3">
          <p>
            <label class="checkbox-inline">
              <?php if ($description) { ?>
              <input type="checkbox" name="description" value="1" id="description" checked="checked" />
              <?php } else { ?>
              <input type="checkbox" name="description" value="1" id="description" />
              <?php } ?>
              <?php echo $entry_description; ?></label>
          </p>
          </div>
        <div class="col-sm-3">
          <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
        </div>
        </div>


          <h2><?php echo $text_search; ?></h2>
          <?php if ($products) { ?>
      <div class="row category-sort">
        <div class="col-md-4">
          <div class="btn-group hidden-xs hidden-lg hidden-sm hidden-md">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>
        <div class="col-md-2 col-xs-5 text-right text-uppercase">
          <label class="control-label" for="input-sort">Сортировать по:</label>
        </div>
        <!--РАЗЛИЧНЫЕ СОРТИРОВКИ-->
        <?php foreach ($sorts as $sorts) if($sorts['group'] == 'price'){ ?>

        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <div class="col-md-1 text-right text-uppercase">
          <a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a>
        </div>
        <?php } else if($sort!='p.price' && $sorts['value'] == 'p.price-ASC'){ ?>
        <div class="col-md-1 col-xs-3 text-right text-uppercase">
          <a href="<?php echo $sorts['href']; ?>">Цене</a>
        </div>
        <?php } ?>

        <?php } else if($sorts['group'] == 'ABC'){ ?>

        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <div class="col-md-2 text-right text-uppercase">
          <a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a>
        </div>
        <?php } else if($sort!='pd.name' && $sorts['value'] == 'pd.name-ASC'){ ?>
        <div class="col-md-2 col-xs-4 text-right text-uppercase">
          <a href="<?php echo $sorts['href']; ?>">По алфавиту</a>
        </div>
        <?php } ?>

        <?php } else if($sorts['group'] == 'rating'){ ?>

        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <div class="col-md-2 text-right еу">
          <a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a>
        </div>
        <?php } else if($sort!='rating' && $sorts['value'] == 'rating-ASC'){ ?>
        <div class="col-md-2 text-right">
          <a href="<?php echo $sorts['href']; ?>">Популярности</a>
        </div>
        <?php } ?>

        <?php } ?>

        <!-- ПОКАЗАТЬ по 15
        <div class="col-md-1 text-right">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="col-md-2 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      -->
      </div>
      <br/>
      <div class="row category">
        <?php foreach ($products as $key => $product ) { ?>
        <!-- Это массив из продуктов -->
        <div class="product-layout product-list col-xs-12">
          <div class="product-thumb">
            <!-- Это фото продукта-->
            <div class="image"><a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>);"  ><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive"/></a></div>
            <div>
              <input style="display: none" name="product_id[<?php echo $key ?>]" value="<?php echo $product['product_id'] ?>" />
              <div class="caption">
                <!-- Это название продукта-->
                <h4><a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>)"><?php echo $product['name']; ?></a></h4>
                <!-- Это описание продукта-->
                <p><?php echo $product['description']; ?></p>
                <!-- Это рейтинг,я пока в нем не разбирался-->
                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
                <!-- Количество пицц и ингридиенты -->
                <div class="form-inline clearfix">
                  <input type="text" maxlength="4" name="quantity[<?php echo $key ?>]" value="1" class="text-center" id="number-of-product<?php echo $key?>">
                  <button type="button" class="btn" onclick="add_rem_product(<?php echo $key ?>,<?php echo $product['price']?>,false,<?php echo $product['special']?>)"><span class="glyphicon glyphicon-minus btn-sm"></span></button>
                  <button type="button" class="btn" onclick="add_rem_product(<?php echo $key ?>,<?php echo $product['price']?>,true,<?php echo $product['special']?>)"><span class="glyphicon glyphicon-plus btn-sm"></span></button>
                  <div class="btn-group dropup">
                    <?php if($product['product_ingredients']) { ?>
                    <a href="" data-toggle="dropdown">Добавить</a>
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">+</button>
                    <ul class="dropdown-menu" role="menu">
                      <?php foreach($product['product_ingredients'] as $product_ingredients) { ?>
                      <p><?php echo $product_ingredients['name'] ?></p>
                      <?php foreach($product_ingredients['ingredients'] as $ingredients) { ?>
                      <li class="ingredients-menu clearfix">
                        <p class="pull-left" href="#"><?php echo $ingredients['name']?></p>
                        <?php $ingredients['main_cast'] == 1 ? $main = true: $main = false ?>
                        <button class="btn pull-right" onclick="add_rem_ingredient(<?php echo $key ?>,<?php echo $ingredients['price']?>,false,<?php echo $ingredients['ingredients_id']?>,<?php echo $main ?>)">-</button>
                        <button class="btn pull-right" onclick="add_rem_ingredient(<?php echo $key ?>,<?php echo $ingredients['price']?>,true,<?php echo $ingredients['ingredients_id']?>,<?php echo $main ?>)">+</button>
                        <input name="ingredients[<?php echo $key ?>][quantity][<?php echo $ingredients['ingredients_id']?>]" type="text" maxlength="5" class="pull-right" style="background: #6ca824;" value="<?php echo $ingredients['quantity']?>" readonly>
                        <span id="hide"></span><input name="ingredients[<?php echo $key ?>][price][<?php echo $ingredients['ingredients_id']?>]" type="text" maxlength="7" class="pull-right" id="txt" value="+<?php echo $ingredients['price']?>р." style="background: #eb9316;" readonly>
                      </li>
                      <?php } ?>
                      <?php } ?>
                    </ul>
                    <?php } ?>
                    <?php if (!$product['option']) { ?>
                    <p id="weight-<?php echo $key?>"><?php echo $product['weight']?></p>
                    <?php } ?>
                  </div>
                </div>
                <!-- Это размер или тип теста пиццы если есть-->
                <?php if ($product['option']) { ?>

                <input style="display: none" name="product_my_option_value_id[<?php echo $key ?>]" value="<?php if(isset($product['option']['product']['product_my_option_value_id'])) echo $product['option']['product']['product_my_option_value_id'] ?>" />

                <?php foreach ($product['option'] as $option_arr_key => $option_arr) if($option_arr_key != 'product'){ ?>
                <?php if($option_arr_key == 'size_it' || $option_arr_key == 'size_am') { ?>
                <div class="form-group clearfix">
                  <?php } else if($option_arr_key == 'dough') { ?>
                  <div class="form-group center-block">
                    <?php }?>
                    <select name="product_option_name[<?php echo $key?>][<?php echo $option_arr_key?>]" id="<?php echo $option_arr_key.'-'.$key?>"  class="form-control" onchange="selectOption('<?php echo $key?>');">
                      <?php foreach ($option_arr as $option_arr) { ?>
                      <option value="<?php echo $option_arr['option_value_my_id']; ?>"><?php echo $option_arr['name'];?></option>
                      <?php } ?>
                    </select>
                    <?php if($option_arr_key == 'size_it' || $option_arr_key == 'size_am') { ?>
                    <p id="weight-<?php echo $key?>"><?php echo $product['weight']?></p>
                    <?php }?>
                  </div>
                  <?php }?>
                  <?php }?>

                </div>
                <div class="order-group clearfix">
                  <div class="button-group">
                    <!-- Это кнопки: купить, в закладки-->
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                    <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', <?php echo $key?>);"><span class=""><?php echo $button_cart; ?></span></button>
                  </div>
                  <!-- Это цена продукта-->
                  <?php if ($product['price']) { ?>
                  <?php if ($product['special'] == 0) { ?>
                  <p class="price" id="price-<?php echo $key?>"><?php echo $product['price']; ?>р.</p>
                  <?php } else { ?>
                  <p class="price-special">
                    <span class="price-new" id="price-new-<?php echo $key?>"><?php echo $product['special']; ?>р.</span>
                    <span class="price-old" id="price-old-<?php echo $key?>"><?php echo $product['price']; ?>р.</span>
                  </p>
                  <?php } ?>

                  <?php } ?>
                </div>
              </div>
            </div>
          </div>


          <?php } ?>
           </div>
           <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
          </div>
          <?php } else { ?>
          <p><?php echo $text_empty; ?></p>
          <?php } ?>
          <?php echo $content_bottom; ?></div>
          <?php echo $column_right; ?></div>
        </div>
        <script type="text/javascript"><!--
        $('#button-search').bind('click', function() {
         url = 'index.php?route=product/search';

         var search = $('#content input[name=\'search\']').prop('value');

         if (search) {
          url += '&search=' + encodeURIComponent(search);
        }

        var category_id = $('#content select[name=\'category_id\']').prop('value');

        if (category_id > 0) {
          url += '&category_id=' + encodeURIComponent(category_id);
        }

        var filter_description = $('#content input[name=\'description\']:checked').prop('value');

        if (filter_description) {
          url += '&description=true';
        }

        location = url;
      });

        $('#content input[name=\'search\']').bind('keydown', function(e) {
         if (e.keyCode == 13) {
          $('#button-search').trigger('click');
        }
      });

        $('select[name=\'category_id\']').trigger('change');

      --></script>
  <script><!--
    $(document).ready(function () {
        product_option = JSON.parse('<?php if(isset($json_option)) echo $json_option ?>');
    });
   --></script>

      <?php echo $footer; ?>
