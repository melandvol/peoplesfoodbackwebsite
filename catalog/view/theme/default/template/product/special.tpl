<?php echo $header; ?>
<script src="catalog/view/javascript/common.js"></script>
<div class="container">

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="text-center"><h3><?php echo $heading_title; ?></h3></div>
      <?php if ($products) { ?>

      <br />
      <div class="row">
        <?php foreach ($products as $key => $product ) { ?>
        <!-- Это массив из продуктов -->
        <div class="product-layout product-list col-xs-12">
          <div class="product-thumb">
            <!-- Это фото продукта-->
            <div class="image"><a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>);"  ><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive"/></a></div>
            <div>
              <input style="display: none" name="product_id[<?php echo $key ?>]" value="<?php echo $product['product_id'] ?>" />
              <div class="caption">
                <!-- Это название продукта-->
                <h4><a style="cursor:pointer;" onclick="getProduct(<?php echo $product['product_id'] ?>)"><?php echo $product['name']; ?></a></h4>
                <!-- Это описание продукта-->
                <p><?php echo $product['description']; ?></p>
                <!-- Это рейтинг,я пока в нем не разбирался-->
                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
                <!-- Количество пицц и ингридиенты -->
                <div class="form-inline clearfix">
                  <input type="text" maxlength="4" name="quantity[<?php echo $key ?>]" value="1" class="text-center" id="number-of-product<?php echo $key?>">
                  <button type="button" class="btn" onclick="add_rem_product(<?php echo $key ?>,<?php echo $product['price']?>,false,<?php echo $product['special']?>)"><span class="glyphicon glyphicon-minus btn-sm"></span></button>
                  <button type="button" class="btn" onclick="add_rem_product(<?php echo $key ?>,<?php echo $product['price']?>,true,<?php echo $product['special']?>)"><span class="glyphicon glyphicon-plus btn-sm"></span></button>
                  <div class="btn-group dropup">
                    <?php if($product['product_ingredients']) { ?>
                    <a href="" data-toggle="dropdown">Добавить</a>
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">+</button>
                    <ul class="dropdown-menu" role="menu">
                      <?php foreach($product['product_ingredients'] as $product_ingredients) { ?>
                      <p><?php echo $product_ingredients['name'] ?></p>
                      <?php foreach($product_ingredients['ingredients'] as $ingredients) { ?>
                      <li class="ingredients-menu clearfix">
                        <p class="pull-left" href="#"><?php echo $ingredients['name']?></p>
                        <?php $ingredients['main_cast'] == 1 ? $main = true: $main = false ?>
                        <button class="btn pull-right" onclick="add_rem_ingredient(<?php echo $key ?>,<?php echo $ingredients['price']?>,false,<?php echo $ingredients['ingredients_id']?>,<?php echo $main ?>)">-</button>
                        <button class="btn pull-right" onclick="add_rem_ingredient(<?php echo $key ?>,<?php echo $ingredients['price']?>,true,<?php echo $ingredients['ingredients_id']?>,<?php echo $main ?>)">+</button>
                        <input name="ingredients[<?php echo $key ?>][quantity][<?php echo $ingredients['ingredients_id']?>]" type="text" maxlength="5" class="pull-right" style="background: #6ca824;" value="<?php echo $ingredients['quantity']?>" readonly>
                        <span id="hide"></span><input name="ingredients[<?php echo $key ?>][price][<?php echo $ingredients['ingredients_id']?>]" type="text" maxlength="7" class="pull-right" id="txt" value="+<?php echo $ingredients['price']?>р." style="background: #eb9316;" readonly>
                      </li>
                      <?php } ?>
                      <?php } ?>
                    </ul>
                    <?php } ?>
                    <?php if (!$product['option'] && empty(!$product['weight'])) { ?>
                    <p class="weight" id="weight-<?php echo $key?>"><?php echo $product['weight']?></p>
                    <?php } ?>
                  </div>
                </div>
                <!-- Это размер или тип теста пиццы если есть-->
                <?php if ($product['option']) { ?>

                <input style="display: none" name="product_my_option_value_id[<?php echo $key ?>]" value="<?php if(isset($product['option']['product']['product_my_option_value_id'])) echo $product['option']['product']['product_my_option_value_id'] ?>" />

                <?php foreach ($product['option'] as $option_arr_key => $option_arr) if($option_arr_key != 'product'){ ?>
                <?php if($option_arr_key == 'size_it' || $option_arr_key == 'size_am') { ?>
                <div class="form-group clearfix">
                  <?php } else if($option_arr_key == 'dough') { ?>
                  <div class="form-group center-block">
                    <?php }?>
                    <select name="product_option_name[<?php echo $key?>][<?php echo $option_arr_key?>]" id="<?php echo $option_arr_key.'-'.$key?>"  class="form-control" onchange="selectOption('<?php echo $key?>');">
                      <?php foreach ($option_arr as $option_arr) { ?>
                      <option value="<?php echo $option_arr['option_value_my_id']; ?>"><?php echo $option_arr['name'];?></option>
                      <?php } ?>
                    </select>
                    <?php if($option_arr_key == 'size_it' || $option_arr_key == 'size_am') { ?>
                    <p id="weight-<?php echo $key?>"><?php echo $product['weight']?></p>
                    <?php }?>
                  </div>
                  <?php }?>
                  <?php }?>

                </div>
                <div class="order-group clearfix">
                  <div class="button-group">
                    <!-- Это кнопки: купить, в закладки-->
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                    <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', <?php echo $key?>);"><span class=""><?php echo $button_cart; ?></span></button>
                  </div>
                  <!-- Это цена продукта-->
                  <?php if ($product['price']) { ?>
                  <?php if ($product['special'] == 0) { ?>
                  <p class="price" id="price-<?php echo $key?>"><?php echo $product['price']; ?>р.</p>
                  <?php } else { ?>
                  <p class="price-special">
                    <span class="price-new" id="price-new-<?php echo $key?>"><?php echo $product['special']; ?>р.</span>
                    <span class="price-old" id="price-old-<?php echo $key?>"><?php echo $product['price']; ?>р.</span>
                  </p>
                  <?php } ?>

                  <?php } ?>
                </div>
              </div>
            </div>
          </div>


          <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
  <script>
    $(document).ready(function() {
      product_option = JSON.parse('<?php if(isset($json_option)) echo $json_option ?>');
    });
  </script>
<?php echo $footer; ?>