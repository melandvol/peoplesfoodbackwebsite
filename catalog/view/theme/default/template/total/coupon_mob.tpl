<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"><a href="#collapse-coupon-mob" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $heading_title; ?> <i class="fa fa-caret-down"></i></a></h4>
  </div>
  <div id="collapse-coupon-mob" class="panel-collapse collapse">
    <div class="panel-body">
      <div class="input-group">
        <label class="control-label sr-only" for="input-coupon-mob" ><?php echo $entry_coupon; ?></label>
        <input type="text" name="coupon_mob" value="<?php echo $coupon; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-coupon-mob" class="form-control" />
        <span class="input-group-btn">
          <input type="button" value="Применить" id="button-coupon-mob" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary" />
        </span></div>
        <script type="text/javascript"><!--
        $('#button-coupon-mob').on('click', function() {
         $.ajax({
          url: 'index.php?route=total/coupon/coupon',
          type: 'post',
          data: 'coupon=' + encodeURIComponent($('input[name=\'coupon_mob\']').val()),
          dataType: 'json',
          beforeSend: function() {
           $('#button-coupon-mob').button('loading');
         },
         complete: function() {
           $('#button-coupon-mob').button('reset');
         },
         success: function(json) {
           $('.alert').remove();

           if (json['error']) {
            $('.error-coup').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

            $('html, body').animate({ scrollTop: 0 }, 'slow');
          }

          if (json['redirect']) {
            location = json['redirect'];
          }
        }
      });
       });
       //--></script>
     </div>
   </div>
 </div>
