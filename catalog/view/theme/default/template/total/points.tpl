<script src="catalog/view/javascript/jquery/jquery.mask.js" type="text/javascript"></script>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><a href="#collapse-points" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $heading_title; ?> <i class="fa fa-caret-down"></i></a></h4>
    </div>
    <div id="collapse-points" class="panel-collapse collapse" style="border-collapse: collapse;">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-4">
                   <label class="control-label" for="input-points">Количетво рублей</label>

               </div>
               <div class="col-sm-4">
                   <label class="control-label" for="input-money">Количетво пифов</label>
               </div>
           </div>
           <div class="row">
               <div class="col-sm-2 text-left">
                <p class="cart-pif-about">1 рубль = 5 пифов</p>
            </div>

            <div class="col-sm-4">
                <input type="text" name="money" value="<?php if($points) echo ceil($points/5); ?>" placeholder="Введите количетво рублей" id="input-points" class="form-control" style="display: block;"/>

            </div>
            <div class="col-sm-4">
               <input type="text" name="points" value="<?php echo $points; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-money" class="form-control" style="display: block;"/>
           </div>
           <div class="col-sm-2">
            <input type="button" value="<?php echo $button_coupon; ?>" id="button-points" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary" style="float:right;" />
        </div>
    </div>
    <script type="text/javascript"><!--
    $('#input-points').mask("000000");
    $('#input-money').mask("000000");
    $('#input-points').on('input keyup', function(e) {
        $('#input-money').val($(this).val()*5);
    });
    $('#input-money').on('input keyup', function(e) {
     $('#input-points').val($(this).val()/5);
    });
    $('#button-points').on('click', function() {
        $.ajax({
            url: 'index.php?route=total/points/points',
            type: 'post',
            data: 'points=' + encodeURIComponent($('input[name=\'points\']').val())+'&price_total=' + encodeURIComponent($('input[name^=\'price_total\']').val()),
            dataType: 'json',
            beforeSend: function() {
                $('#button-points').button('loading');
            },
            complete: function() {
                $('#button-points').button('reset');
            },
            success: function(json) {
                $('.alert').remove();

                if (json['error']) {
                    $('.error-coup').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }

                if (json['redirect']) {
                    location = json['redirect'];
                }
            }
        });
    });
    //--></script>
</div>
</div>
</div>
