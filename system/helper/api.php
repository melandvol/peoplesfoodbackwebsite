<?php

if (!function_exists('set_error')) {
    function set_error($string)
    {
        http_response_code(400);
        return $string;
    }
}
if (!function_exists('set_error_login')) {
    function set_error_login($string = "")
    {
        http_response_code(401);
        return $string;
    }
}
if (!function_exists('format_date')) {
    function format_date($date,$format_input,$format_output)
    {
        $date = DateTime::createFromFormat($format_input, $date);

        return $date->format($format_output);
    }
}