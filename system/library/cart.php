<?php
class Cart {
	private $data = array();

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->tax = $registry->get('tax');
		$this->weight = $registry->get('weight');

		// Remove all the expired carts with no customer ID
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND date_added < DATE_SUB(NOW(), INTERVAL 1 HOUR)");

		if ($this->customer->getId()) {
			// We want to change the session ID on all the old items in the customers cart
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET session_id = '" . $this->db->escape($this->session->getId()) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

			// Once the customer is logged in we want to update the customer ID on all items he has
			$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

			foreach ($cart_query->rows as $cart) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart['cart_id'] . "'");

				// The advantage of using $this->add is that it will check if the products already exist and increaser the quantity if necessary.
				$this->add($cart['product_id'], $cart['quantity'], json_decode($cart['option']), $cart['recurring_id'],$cart['product_my_option_value_id'],$cart['ingredients']);
			}
		}
	}

	public function getProducts() {
		$product_data = array();

		$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

		foreach ($cart_query->rows as $cart) {

			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store p2s LEFT JOIN " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p2s.product_id = '" . (int)$cart['product_id'] . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");

			if ($product_query->num_rows && ($cart['quantity'] > 0)) {


				$price 				= $product_query->row['price'];
				$weight 			= $product_query->row['weight'];
				$weight_class_id 	= $product_query->row['weight_class_id'];
				$articul 			= $product_query->row['articul'];

				//Pizza option

				$my_option_data = array();

				if($cart['product_my_option_value_id'] != 0) {

					$new_param_query = $this->db->query("SELECT p_m_o_v.points, p_m_o_v.price, p_m_o_v.weight, p_m_o_v.articul FROM " . DB_PREFIX . "product_my_option_value p_m_o_v WHERE p_m_o_v.product_id = '" . (int)$cart['product_id'] . "' and p_m_o_v.product_my_option_value_id = '" . $cart['product_my_option_value_id'] . "'");

					if ($new_param_query->num_rows > 0) {
						$price 				= $new_param_query->row['price'];
						$weight 			= $new_param_query->row['weight'];
						$weight_class_id 	= 1;
						$articul 			= $new_param_query->row['articul'];

						//$sql = "SELECT IF(m_o_v_d.name LIKE '%1/2%' OR m_o.type = 'dough', m_o_v_d.option_value_my_id, '') as option_value_my_id, m_o_v_d.name ,m_o.type FROM " . DB_PREFIX . "product_my_option_value p_m_o_v LEFT JOIN  " . DB_PREFIX . "option_comb o_c ON (o_c.option_comb_id = p_m_o_v.option_comb_id) LEFT JOIN  " . DB_PREFIX . "my_option m_o ON (m_o.option_my_id = o_c.option_my_id) LEFT JOIN  " . DB_PREFIX . "my_option_value_description m_o_v_d ON (m_o_v_d.option_my_id = o_c.option_my_id and m_o_v_d.option_value_my_id = o_c.option_value_my_id) WHERE p_m_o_v.product_id = '" . (int)$cart['product_id'] . "' and p_m_o_v.product_my_option_value_id = '" . $cart['product_my_option_value_id'] . "'";
						$my_option_query = $this->db->query("SELECT m_o_v_d.name ,m_o.type, m_o_v_d.option_value_my_id FROM " . DB_PREFIX . "product_my_option_value p_m_o_v LEFT JOIN  " . DB_PREFIX . "option_comb o_c ON (o_c.option_comb_id = p_m_o_v.option_comb_id) LEFT JOIN  " . DB_PREFIX . "my_option m_o ON (m_o.option_my_id = o_c.option_my_id) LEFT JOIN  " . DB_PREFIX . "my_option_value_description m_o_v_d ON (m_o_v_d.option_my_id = o_c.option_my_id and m_o_v_d.option_value_my_id = o_c.option_value_my_id) WHERE p_m_o_v.product_id = '" . (int)$cart['product_id'] . "' and p_m_o_v.product_my_option_value_id = '" . $cart['product_my_option_value_id'] . "'");

						if($my_option_query->num_rows > 0) {
							$my_option_data = $my_option_query->rows;
						} else {
							$this->remove($cart['cart_id']);
							continue;
						}
					}
					else {
						$this->remove($cart['cart_id']);
						continue;
					}
				}
				//Ингредиенты
				$ingredients = array();
				$ingredients_price = 0;
				if (isset($cart['ingredients']) && (json_decode($cart['ingredients']))) {

					$query = $this->db->query("SELECT p_i.ingredients_id, p_i.quantity,i.price,p_i.main_cast,i.name FROM " . DB_PREFIX . "product_ingredients AS p_i LEFT JOIN	" . DB_PREFIX . "ingredients AS i ON (p_i.ingredients_id = i.ingredients_id) WHERE product_id = '" . (int)$cart['product_id'] . "'");

					if($query->num_rows > 0) {
						$ingredients_array = (array)json_decode($cart['ingredients']);
						
						foreach ($query->rows as $ingredient_bd) {
							foreach ($ingredients_array as $ingredients_in_id => $ingredients_in_quantity) {
								if ($ingredient_bd['ingredients_id'] == $ingredients_in_id) {
									$ingredient_bd['quantity'] = $ingredients_in_quantity;

									if($ingredient_bd['main_cast'] == '1')
										$ingredients_price += (($ingredient_bd['quantity'] != 0) ? $ingredient_bd['quantity'] - 1 : 0) * $ingredient_bd['price'];
									else
										$ingredients_price += ($ingredient_bd['quantity']) * $ingredient_bd['price'];
									break;
								}
							}
							array_push($ingredients, $ingredient_bd);
						}
					}
				}

				// Product Discounts
				$discount_quantity = 0;

				$cart_2_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

				foreach ($cart_2_query->rows as $cart_2) {
					if ($cart_2['product_id'] == $cart['product_id']) {
						$discount_quantity += $cart_2['quantity'];
					}
				}

				$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

				if ($product_discount_query->num_rows) {
					$price = $product_discount_query->row['price'];
				}

				// Product Specials
				$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

				if ($product_special_query->num_rows) {
					$price = $product_special_query->row['price'];
				}

				// Reward Points
				$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($product_reward_query->num_rows) {
					$reward = $product_reward_query->row['points'];
				} else {
					$reward = 0;
				}

				$recurring_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recurring r LEFT JOIN " . DB_PREFIX . "product_recurring pr ON (r.recurring_id = pr.recurring_id) LEFT JOIN " . DB_PREFIX . "recurring_description rd ON (r.recurring_id = rd.recurring_id) WHERE r.recurring_id = '" . (int)$cart['recurring_id'] . "' AND pr.product_id = '" . (int)$cart['product_id'] . "' AND rd.language_id = " . (int)$this->config->get('config_language_id') . " AND r.status = 1 AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($recurring_query->num_rows) {
					$recurring = array(
						'recurring_id'    => $cart['recurring_id'],
						'name'            => $recurring_query->row['name'],
						'frequency'       => $recurring_query->row['frequency'],
						'price'           => $recurring_query->row['price'],
						'cycle'           => $recurring_query->row['cycle'],
						'duration'        => $recurring_query->row['duration'],
						'trial'           => $recurring_query->row['trial_status'],
						'trial_frequency' => $recurring_query->row['trial_frequency'],
						'trial_price'     => $recurring_query->row['trial_price'],
						'trial_cycle'     => $recurring_query->row['trial_cycle'],
						'trial_duration'  => $recurring_query->row['trial_duration']
					);
				} else {
					$recurring = false;
				}
				if((($price + $ingredients_price) * $cart['quantity']) == 0) {
					$this->remove($cart['cart_id']);
					continue;
				}

				$product_data[] = array(
					'cart_id'         => $cart['cart_id'],
					'product_id'      => $product_query->row['product_id'],
					'articul'      	  => $articul,
					'name'            => $product_query->row['name'],
					'product_my_option_value_id' => $cart['product_my_option_value_id'],
					'my_option'       => $my_option_data,
					'ingredients'     => $ingredients,
					'quantity'        => $cart['quantity'],
					'points'          => ($product_query->row['points'] ? ($product_query->row['points'] ) * $cart['quantity'] : 0),
					'weight'          => $weight,
					'weight_class_id' => $weight_class_id,
					'image'           => $product_query->row['image'],
					'price'           => ($price + $ingredients_price),
					'total'           => ($price + $ingredients_price) * $cart['quantity'],

					//Ненужное
					'model'           => '',
					'shipping'        => '',
					'option'          => array(),
					'download'        => array(),
					'minimum'         => 1,
					'subtract'        => '',
					'stock'           => true,
					'reward'          => 0,
					'tax_class_id'    => '',
					'length'          => '',
					'width'           => '',
					'height'          => '',
					'length_class_id' => '',
					'recurring'       => false
				);
			} else {
				$this->remove($cart['cart_id']);
			}
		}


		return $product_data;
	}

	public function add($product_id, $quantity = 1, $option = array(), $recurring_id = 0,$product_my_option_value_id = 0,$ingredients = array()) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "' AND `product_my_option_value_id` = '" . (int)$product_my_option_value_id . "' AND `ingredients` = '" . $this->db->escape(json_encode($ingredients)) . "'");

		if (!$query->row['total']) {
			$sql = "INSERT " . DB_PREFIX . "cart SET customer_id = '" . (int)$this->customer->getId() . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int)$product_id . "', recurring_id = '" . (int)$recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', `ingredients` = '" . $this->db->escape(json_encode($ingredients)) . "', product_my_option_value_id = '" . (int)$product_my_option_value_id . "', quantity = '" . (int)$quantity . "', date_added = NOW()";
			$this->db->query("INSERT " . DB_PREFIX . "cart SET customer_id = '" . (int)$this->customer->getId() . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int)$product_id . "', recurring_id = '" . (int)$recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', `ingredients` = '" . $this->db->escape(json_encode($ingredients)) . "', product_my_option_value_id = '" . (int)$product_my_option_value_id . "', quantity = '" . (int)$quantity . "', date_added = NOW()");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int)$quantity . ") WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "' AND `ingredients` = '" . $this->db->escape(json_encode($ingredients)) . "' AND `product_my_option_value_id` = '" . (int)$product_my_option_value_id . "'");
		}
	}

	public function update($cart_id, $quantity) {
		$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = '" . (int)$quantity . "' WHERE cart_id = '" . (int)$cart_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function remove($cart_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function clear() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function getRecurringProducts() {
		$product_data = array();

		foreach ($this->getProducts() as $value) {
			if ($value['recurring']) {
				$product_data[] = $value;
			}
		}

		return $product_data;
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function getTotalApp($products) {
		$total = 0;

		foreach ($products as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function getTaxes() {
		$tax_data = array();

		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}

		return $tax_data;
	}

	public function getTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts() {
		return count($this->getProducts());
	}

	public function hasRecurringProducts() {
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		$stock = true;

		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {
				$stock = false;
			}
		}

		return $stock;
	}

	public function hasShipping() {
		$shipping = false;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$shipping = true;

				break;
			}
		}

		return $shipping;
	}

	public function hasDownload() {
		$download = false;

		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				$download = true;

				break;
			}
		}

		return $download;
	}
}
