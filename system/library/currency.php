<?php
class Currency {
	private $code;
	private $currencies = array();

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->language = $registry->get('language');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency");

		foreach ($query->rows as $result) {
			$this->currencies[$result['code']] = array(
				'currency_id'   => $result['currency_id'],
				'title'         => $result['title'],
				'symbol_left'   => $result['symbol_left'],
				'symbol_right'  => $result['symbol_right'],
				'decimal_place' => $result['decimal_place'],
				'value'         => $result['value']
			);
		}

		if (isset($this->request->get['currency']) && (array_key_exists($this->request->get['currency'], $this->currencies))) {
			$this->set($this->request->get['currency']);
		} elseif ((isset($this->session->data['currency'])) && (array_key_exists($this->session->data['currency'], $this->currencies))) {
			$this->set($this->session->data['currency']);
		} elseif ((isset($this->request->cookie['currency'])) && (array_key_exists($this->request->cookie['currency'], $this->currencies))) {
			$this->set($this->request->cookie['currency']);
		} else {
			$this->set($this->config->get('config_currency'));
		}
	}

	public function set($currency) {
		$this->code = $currency;

		if (!isset($this->session->data['currency']) || ($this->session->data['currency'] != $currency)) {
			$this->session->data['currency'] = $currency;
		}

		if (!isset($this->request->cookie['currency']) || ($this->request->cookie['currency'] != $currency)) {
			setcookie('currency', $currency, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
		}
	}
	public function formatWeight($weight,$weight_class_id) {
		switch ($weight_class_id){
			case 1: {
				$result = 'Вес: '.$weight.'г.';
				break;
			}
			case 2: {
				$result = 'Вес: '.$weight.'кг.';
				break;
			}
			case 3: {
				$result = 'Кол-во: '.$weight.'шт.';
				break;
			}
			case 4: {
				$result = 'Объём: '.$weight.'л.';
				break;
			}
			case 5: {
				$result = '';
				break;
			}
			default: {
					$result = 'Вес: '.$weight.'г.';
					break;
				}
		}
		return $result;
	}
	public function format($number, $currency = '', $value = '', $format = true) {
		if ($currency && $this->has($currency)) {
			$symbol_left   = $this->currencies[$currency]['symbol_left'];
			$symbol_right  = $this->currencies[$currency]['symbol_right'];
			$decimal_place = $this->currencies[$currency]['decimal_place'];
		} else {
			$symbol_left   = $this->currencies[$this->code]['symbol_left'];
			$symbol_right  = $this->currencies[$this->code]['symbol_right'];
			$decimal_place = $this->currencies[$this->code]['decimal_place'];

			$currency = $this->code;
		}

		if ($value) {
			$value = $value;
		} else {
			$value = $this->currencies[$currency]['value'];
		}

		if ($value) {
			$value = (float)$number * $value;
		} else {
			$value = $number;
		}

		$string = '';

		if (($symbol_left) && ($format)) {
			$string .= $symbol_left;
		}

		if ($format) {
			$decimal_point = $this->language->get('decimal_point');
		} else {
			$decimal_point = '.';
		}

		if ($format) {
			$thousand_point = $this->language->get('thousand_point');
		} else {
			$thousand_point = '';
		}

		$string .= number_format(round($value, (int)$decimal_place), (int)$decimal_place, $decimal_point, $thousand_point);

		if (($symbol_right) && ($format)) {
			$string .= $symbol_right;
		}

		return $string;
	}

	public function convert($value, $from, $to) {
		if (isset($this->currencies[$from])) {
			$from = $this->currencies[$from]['value'];
		} else {
			$from = 1;
		}

		if (isset($this->currencies[$to])) {
			$to = $this->currencies[$to]['value'];
		} else {
			$to = 1;
		}

		return $value * ($to / $from);
	}

	public function getId($currency = '') {
		if (!$currency) {
			return $this->currencies[$this->code]['currency_id'];
		} elseif ($currency && isset($this->currencies[$currency])) {
			return $this->currencies[$currency]['currency_id'];
		} else {
			return 0;
		}
	}

	public function getSymbolLeft($currency = '') {
		if (!$currency) {
			return $this->currencies[$this->code]['symbol_left'];
		} elseif ($currency && isset($this->currencies[$currency])) {
			return $this->currencies[$currency]['symbol_left'];
		} else {
			return '';
		}
	}

	public function getSymbolRight($currency = '') {
		if (!$currency) {
			return $this->currencies[$this->code]['symbol_right'];
		} elseif ($currency && isset($this->currencies[$currency])) {
			return $this->currencies[$currency]['symbol_right'];
		} else {
			return '';
		}
	}

	public function getDecimalPlace($currency = '') {
		if (!$currency) {
			return $this->currencies[$this->code]['decimal_place'];
		} elseif ($currency && isset($this->currencies[$currency])) {
			return $this->currencies[$currency]['decimal_place'];
		} else {
			return 0;
		}
	}

	public function getCode() {
		return $this->code;
	}

	public function getValue($currency = '') {
		if (!$currency) {
			return $this->currencies[$this->code]['value'];
		} elseif ($currency && isset($this->currencies[$currency])) {
			return $this->currencies[$currency]['value'];
		} else {
			return 0;
		}
	}

	public function has($currency) {
		return isset($this->currencies[$currency]);
	}
	
	public function validDate($date,$format) { // проверка на правильность формата даты
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) === $date;
	}
	
	//Ingredients
	public function priceIngredients($data) {
		$ingredients = "";
		$ingredients_price = 0;
		foreach ($data as $ingredients_array) {
			if ($ingredients_array['main_cast'] == '1') {
				$ingredients_price += (((int)$ingredients_array['quantity'] != 0) ? (int)$ingredients_array['quantity'] - 1 : 0) * (int)$ingredients_array['price'];
				$ingredients = "Состав: ";
				$ingredients .= "x" . (int)$ingredients_array['quantity'] . " " . $ingredients_array['name'] . " ;";
			} else if ($ingredients_array['main_cast'] == 0) {
				$ingredients_price += (int)($ingredients_array['quantity']) * (int)$ingredients_array['price'];

				$ingredients .= "Добавить: ";
				if ($ingredients_array['quantity'] > 0)
					$ingredients .= "x" . (int)$ingredients_array['quantity'] . " " . $ingredients_array['name'] . " ;";
			}
		}
		return $ingredients_price;
	}
	public function nameIngredients($data,$sAdd,$sRemove) {
		$ingredients['add'] = $sAdd;
		$ingredients['remove'] = $sRemove;

		$add = false;
		$remove = false;
		foreach ($data as $ingredients_array) {
			if ($ingredients_array['main_cast'] == '1') {
				if ($ingredients_array['quantity'] == 0) {
					$ingredients['remove'] .= " " . $ingredients_array['name'] . ";";
					$remove = true;
				}
				else if($ingredients_array['quantity'] > 1) {
					$ingredients['add'] .= " " . $ingredients_array['name'] . " x" . (int)$ingredients_array['quantity'] . ";";
					$add = true;
				}
			} else if ($ingredients_array['main_cast'] == 0) {

				if ($ingredients_array['quantity'] > 0) {
					$ingredients['add'] .= " " . $ingredients_array['name'] . " x" . (int)$ingredients_array['quantity'] . ";";
					$add = true;
				}
			}
		}
		if(!$add)
			$ingredients['add'] = "";

		if(!$remove)
			$ingredients['remove'] = "";

		return $ingredients;
	}
	public function validTimeZone() { // проверка на время работы компании
		if(ini_get('date.timezone') != "Europe/Moscow");
		date_default_timezone_set('Europe/Moscow');

		$week 		= date("w");
        $month 		= date("n");
        $day 		= date("j");
		$date_now 	= new DateTime(date("H:i"));

		if($month == 12 && $day == 31)
        {
            $start_time = new DateTime("18:00");
            if($date_now > $start_time)
            {
                return "С Новым годом! Извините! Принимаем заказы с 02.01.19 с 9:00 утра";
            }
        }
        else if($month == 1)
        {
            $start_time = new DateTime("09:00");
            if( $day == 1)
            {
                return "С Новым годом! Извините! Принимаем заказы с 02.01.19 с 9:00 утра";
            }
            else  if( $day == 2 && $date_now < $start_time)
            {
                return "С Новым годом! Извините! Принимаем заказы с 02.01.19 с 9:00 утра";
            }
        }


		switch ($week) {
			case 1: {  // Понедельник

				$start_time = new DateTime("05:00");
				$end_time 	= new DateTime("08:00");

				break;
			}
			case 2: {  // Вторник

				$start_time = new DateTime("05:00");
				$end_time 	= new DateTime("08:00");
				
				break;
			}
			case 3: {  // Среда

				$start_time = new DateTime("05:00");
				$end_time 	= new DateTime("08:00");
				
				break;
			}
			case 4: {  // Четверг

				$start_time = new DateTime("05:00");
				$end_time 	= new DateTime("08:00");
				
				break;
			}
			case 5: {  // Пятница

				$start_time = new DateTime("05:00");
				$end_time 	= new DateTime("08:00");
				
				break;
			}
			case 6: {  // Суббота

				$start_time = new DateTime("07:00");
				$end_time 	= new DateTime("07:15");
				
				break;
			}
			case 0: {  // Воскресенье

				$start_time = new DateTime("07:00");
				$end_time 	= new DateTime("07:15");
				
				break;
			}
		}

		if ($date_now > $start_time && $date_now < $end_time)
			return "Время работы компании с " . $start_time->format("H:i") . " до ". $end_time->format("H:i");
		else
			return false;
	}
	public function validPrice ($currPrice){
		
		$price = 340;

		$week 		= date("w");
		$date_now 	= new DateTime(date("H:i"));
		$isDay = false;
		switch ($week) {
			case 1:
			case 2: 
			case 3: 
			case 4: 
			case 5: {  // Пятница

				$start_time = new DateTime("00:01");
				$end_time 	= new DateTime("05:00");
				$isDay = true;
				break;
			}
		}

		if ($isDay && $date_now > $start_time && $date_now < $end_time){
			$price = 509;
		}

		if( $currPrice < $price ) {
				return "Сумма заказа должна быть не меньше " . $price . " рублей";
		}
		return false;
	}
}
