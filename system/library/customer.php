<?php
class Customer {
	private $customer_id;
	private $firstname;
	private $lastname;
	private $customer_group_id;
	private $email;
	private $telephone;
	private $fax;
	private $newsletter;
	private $newsletter_sms;
	private $address_id;
	private $isCard;
	private $card_info;

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['customer_id'])) {
			$customer_query = $this->db->query("SELECT * FROM peoples_users WHERE id = '" . (int)$this->session->data['customer_id'] . "'");

			if ($customer_query->num_rows) {
				$this->customer_id 			= $customer_query->row['id'];
				$this->firstname 			= $customer_query->row['fio'];
				$this->customer_group_id 	= 0;
				$this->email 				= $customer_query->row['email'];
				$this->telephone 			= $customer_query->row['phone'];
				$this->fax 					= $customer_query->row['phone_dop'];
				$this->newsletter 			= $customer_query->row['rss_email'];
				$this->newsletter_sms 		= $customer_query->row['rss_sms'];
				$this->address_id 			= $customer_query->row['address_id'];
				$this->birthday 	 	 	= $customer_query->row['date_birthday'];
				$this->isCard			 	= false;
				$this->card_info  			= $this->getCardInfoLocal();
				//$this->db->query("UPDATE " . DB_PREFIX . "customer SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$this->session->data['customer_id'] . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

				if (!$query->num_rows) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "customer_ip SET customer_id = '" . (int)$this->session->data['customer_id'] . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', date_added = NOW()");
				}
			} else {
				$this->logout();
			}
		}
	}

	public function login($email, $password, $override = false) {
		if ($override) {
			$customer_query = $this->db->query("SELECT * FROM peoples_users WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		} else {
			$customer_query = $this->db->query("SELECT * FROM peoples_users WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND pass = '" . $this->db->escape($password) . "'");
		}

		if ($customer_query->num_rows > 0) {
			$this->session->data['customer_id'] = $customer_query->row['id'];

			$this->customer_id 			= $customer_query->row['id'];
			$this->firstname 			= $customer_query->row['fio'];
			$this->customer_group_id 	= 0;
			$this->email 				= $customer_query->row['email'];
			$this->telephone 			= $customer_query->row['phone'];
			$this->fax 					= $customer_query->row['phone_dop'];
			$this->newsletter 			= $customer_query->row['rss_email'];
			$this->newsletter_sms 		= $customer_query->row['rss_sms'];
			$this->address_id 			= $customer_query->row['address_id'];
			$this->birthday 	 	 	= $customer_query->row['date_birthday'];
			$this->card_info 			= $this->getCardInfoLocal();
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

			return true;
		} else {
			return false;
		}
	}
	public function loginById($customer_id) {
		$customer_query = $this->db->query("SELECT * FROM peoples_users WHERE id = '" . (int)$customer_id . "'");
		if ($customer_query->num_rows) {
			$this->session->data['customer_id'] = $customer_query->row['id'];
			$this->customer_id 			= $customer_query->row['id'];
			$this->firstname 			= $customer_query->row['fio'];
			$this->customer_group_id 	= 0;
			$this->email 				= $customer_query->row['email'];
			$this->telephone 			= $customer_query->row['phone'];
			$this->fax 					= $customer_query->row['phone_dop'];
			$this->newsletter 			= $customer_query->row['rss_email'];
			$this->newsletter_sms 		= $customer_query->row['rss_sms'];
			$this->address_id 			= $customer_query->row['address_id'];
			$this->birthday 	 	 	= $customer_query->row['date_birthday'];
			$this->card_info 			= $this->getCardInfoLocal();
			
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

			$query_return = array(
				'customer_id'			=> $customer_query->row['id'],
				'firstname'				=> $customer_query->row['fio'],
				'customer_group_id'		=> 0,
				'email'					=> $customer_query->row['email'],
				'telephone'				=> $customer_query->row['phone'],
				'fax'					=> $customer_query->row['phone_dop'],
				'newsletter'			=> $customer_query->row['rss_email'],
				'newsletter_sms'		=> $customer_query->row['rss_sms'],
				'address_id'			=> $customer_query->row['address_id'],
				'birthday' 	 	 		=> $customer_query->row['date_birthday'],
				'custom_field' 	 	 	=> '',
			);
			return $query_return;
		} else {
			return false;
		}
	}
	public function getTotalCustomer($customer_id) {
		$customer_query = $this->db->query("SELECT id,email,address_id,fio FROM peoples_users WHERE id = '" . (int)$customer_id . "'");
		if ($customer_query->num_rows) {
			$this->customer_id		 = $customer_query->row['id'];
			$this->firstname		 = $customer_query->row['fio'];
			$this->email		 	 = $customer_query->row['email'];
			$this->address_id		 = $customer_query->row['address_id'];
			$this->card_info 		 = $this->getCardInfoLocal();
			return true;
		}
		else
			return false;
	}

	public function logout() {
		unset($this->session->data['customer_id']);

		$this->customer_id = '';
		$this->customer_id_second = '';
		$this->firstname = '';
		$this->lastname = '';
		$this->customer_group_id = '';
		$this->email = '';
		$this->telephone = '';
		$this->fax = '';
		$this->newsletter = '';
		$this->newsletter_sms = '';
		$this->address_id = '';
		$this->card_info = '';
		$this->birthday	= '';
	}

	private function getCardInfoLocal() {
		$card_query = $this->db->query("SELECT id_cart,status_cart_id FROM peoples_users WHERE id = '" . (int)$this->customer_id . "'");
		if($card_query->num_rows > 0) {
			if($card_query->row['status_cart_id'] == 2) {
				$card_info = $this->db->query("SELECT * FROM peoples_id_cart_balls WHERE id_cart = '" . $this->db->escape($card_query->row['id_cart']) . "'");
				if($card_info->num_rows > 0) {

					$card_info_return = array(
						'status' 		=> 2,
						'card' 			=> $card_query->row['id_cart'],
						'procent' 		=> $card_info->row['procent'],
						'total_price' 	=> 0,
						'points' 		=> $card_info->row['balls']
					);
					$this->isCard = true;
					$this->status_card = 2;

					return $card_info_return;
				}
				else
					return false;
				return $card_query->row;
			} else if($card_query->row['status_cart_id'] == 1) {

				$card_info_return = array(
						'status' 		=> 1,
						'card' 			=> "",
						'procent' 		=> 0,
						'total_price' 	=> 0,
						'points' 		=> 0
					);
				$this->isCard = true;
				$this->status_card = 1;

				return $card_info_return;

			} else if($card_query->row['status_cart_id'] == 0) {
				if($card_query->row['id_cart']) {
					$card_info = $this->db->query("SELECT * FROM peoples_id_cart_balls WHERE id_cart = '" . $this->db->escape($card_query->row['id_cart']) . "'");
					if ($card_info->num_rows > 0) {
						$this->db->query("UPDATE peoples_users SET status_cart_id = '2' WHERE id = '" . (int)$this->customer_id . "'");

						$card_info_return = array(
							'status' => 2,
							'card' => $card_query->row['id_cart'],
							'procent' => $card_info->row['procent'],
							'total_price' => 0,
							'points' => $card_info->row['balls']
						);
						$this->isCard = true;

						return $card_info_return;
					} else
						return false;
				} else
					return false;
			} else
				return false;
		} else
			return false;



		$card_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_card WHERE customer_id = '" . (int)$this->customer_id . "'");
		if ($card_query->num_rows > 0) {
			if($card_query->row['status'] == 2) {
				$this->isCard = true;
				return $card_query->row;
			}
			else
				return false;
		}
		else
			return false;
	}

	public function isLogged() {
		return $this->customer_id;
	}

	public function getId() {
		return $this->customer_id;
	}

	public function getFirstName() {
		return $this->firstname;
	}

	public function getLastName() {
		return $this->lastname;
	}

	public function getGroupId() {
		return $this->customer_group_id;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getTelephone() {
		return $this->telephone;
	}

	public function getFax() {
		return $this->fax;
	}

	public function getBirthday() {
		return $this->birthday;
	}

	public function getNewsletter() {
		return $this->newsletter;
	}

	public function getNewsletterSms() {
		return $this->newsletter_sms;
	}

	public function getAddressId() {
		return $this->address_id;
	}

	public function getBalance() {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$this->customer_id . "'");

		return $query->row['total'];
	}

	public function getRewardPoints() {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$this->customer_id . "'");

		return $query->row['total'];
	}
	public function isCard() {
		return $this->isCard;
	}
	public function getCardInfo() {
		return $this->card_info;
	}
}
