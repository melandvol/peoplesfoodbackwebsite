<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button onclick="$('#content #apply').attr('value', '1'); $('#' + $('#content form').attr('id')).submit();" data-toggle="tooltip" title="Применить" class="btn btn-success"><i class="fa fa-save"></i></button>
        <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <!--<<li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>-->
            <li><a href="#tab-links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
            <!--<li><a href="#tab-attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>-->
            <!--<li><a href="#tab-option" data-toggle="tab"><?php echo $tab_option; ?></a></li>-->
            <!--<li><a href="#tab-recurring" data-toggle="tab"><?php echo $tab_recurring; ?></a></li>-->
            <li><a href="#tab-discount" data-toggle="tab" id="remove1" style="display:none" ><?php echo $tab_discount; ?></a></li>
            <li><a href="#tab-special" data-toggle="tab" id="remove2" <?php if($is_pizza) echo 'style="display:none"'?>><?php echo $tab_special; ?></a></li>
            <!--<li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>-->
            <li><a href="#tab-reward" data-toggle="tab" id="remove3" <?php if($is_pizza) echo 'style="display:none"'?>><?php echo $tab_reward; ?></a></li>
            <!--<li><a href="#tab-design" data-toggle="tab"><?php echo $tab_design; ?></a></li>-->
            <li><a href="#tab-pizza" data-toggle="tab" >Пицца</a></li>
            <li><a href="#tab-ingredients" data-toggle="tab" style="display: <?php echo $display_ingredients; ?>" id="tab-ingredients-none">Ингредиенты</a></li>
          </ul>
          <div class="tab-content">
            <!-- tab-general open-->
            <div class="tab-pane active" id="tab-general" >
              <ul class="nav nav-tabs" id="language" style="display: none">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_name[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-category"><span data-toggle="tooltip" title="<?php echo $help_category; ?>">Показывать в категории</span></label>
                    <div class="col-sm-10">
                      <input id="product-category-text" type="<?php if($product_categories) echo 'hidden'; else echo 'text' ?>" name="category" value="" placeholder="<?php echo $entry_category; ?>" id="input-category" class="form-control" />
                      <div id="product-category" class="well well-sm" style="height: 40px; overflow: auto;">
                        <?php foreach ($product_categories as $product_category) { ?>
                        <div id="product-category<?php echo $product_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_category['name']; ?>
                          <input type="hidden" name="product_category[]" value="<?php echo $product_category['category_id']; ?>" />
                        </div>
                        <?php } ?>
                      </div>
                      <?php if ($error_category) { ?>
                      <div class="text-danger"><?php echo $error_category; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-10">
                      <div class="alert alert-warning">
                        <strong>Внимание!</strong> Загружать картинки, только с прозрачным фоном!
                      </div>
                    </div>
                    <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                    <div class="col-sm-10">
                      <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                      <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-articul">Артикул</label>
                    <div class="col-sm-10">
                      <input type="text" name="articul" value="<?php echo $articul; ?>" <?php if(!$is_pizza) { ?> placeholder="Артикул" <?php } ?> id="input-articul" class="form-control" <?php if($is_pizza) echo 'readonly placeholder="Вы используете опцию"'?>/>
                      <?php if ($error_articul) { ?>
                        <div class="text-danger"><?php echo $error_articul; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="price" value="<?php echo $price; ?>" <?php if(!$is_pizza) { ?> placeholder="Цена" <?php } ?> id="input-price" class="form-control" <?php if($is_pizza) echo 'readonly placeholder="Вы используете опцию"'?> />
                      <?php if ($error_price) { ?>
                        <div class="text-danger"><?php echo $error_price; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-weight-class">Единица измерения</label>
                    <div class="col-sm-10">
                      <select name="weight_class_id" id="input-weight-class" class="form-control" <?php if($is_pizza) echo 'disabled'?>>
                      <?php foreach ($weight_classes as $weight_class) { ?>
                      <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                      <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title'].", ".$weight_class['unit']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title'].", ".$weight_class['unit']; ?></option>
                      <?php } ?>
                      <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group" required>
                    <label class="col-sm-2 control-label" for="input-weight">Значение</label>
                    <div class="col-sm-10">
                      <input type="text" name="weight" value="<?php echo $weight; ?>" <?php if(!$is_pizza) { ?> placeholder="Вес" <?php } ?> id="input-weight" class="form-control" <?php if($is_pizza) echo 'readonly placeholder="Вы используете опцию"'?>/>
                      <?php if ($error_weight) { ?>
                      <div class="text-danger"><?php echo $error_weight; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-weight-limit">Ограничение по весу<span data-toggle="tooltip" title="Если стоит 0 то ограничения нету"></span></label>
                    <div class="col-sm-10">
                      <input type="text" name="weight_limit" value="<?php echo $weight_limit; ?>" placeholder="Ограничение по весу" id="input-weight-limit" class="form-control"/>
                      <?php if ($error_weight_limit) { ?>
                      <div class="text-danger"><?php echo $error_weight_limit; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                    <div class="col-sm-10">
                      <input type="checkbox" id="input-status" class="form-control" name="status" <?php if($status) echo "checked"; ?>>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="product_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-date-available">Дата создания</label>
                    <div class="col-sm-3">
                      <div class="input-group date">
                        <input type="text" name="date_available" value="<?php echo $date_available; ?>" placeholder="<?php echo $entry_date_available; ?>" data-date-format="YYYY-MM-DD" id="input-date-available" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-only-cash">Только наличкой</label>
                    <div class="col-sm-10">
                      <input type="checkbox" id="input-only-cash" class="form-control" name="only_cash" <?php if($only_cash) echo "checked"; ?>>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <!-- tab-general close-->
            <!-- tab-data open-->
            <div class="tab-pane" id="tab-data">
             <!--SEO
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                  <?php if ($error_keyword) { ?>
                  <div class="text-danger"><?php echo $error_keyword; ?></div>
                  <?php } ?>
                </div>
              </div>
              -->

            </div>
            <!-- tab-data close-->
            <!-- tab-pizza open-->
            <?php $pizza_value_row = 0; ?>
            <?php $pizza_am_value_row = 0; ?>
            <div class="tab-pane" id="tab-pizza">
              <div class="table-responsive">
                <?php if ($error_pizza_option) { ?>
                  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_pizza_option?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                  </div>
                <?php } ?>
                <!--Пицца Итальянская-->
                <?php if( $is_italy ) { ?>
                <table class="table table-bordered table-hover" id="option_pizza-row">
                  <caption><h3>Итальянская пицца</h3></caption>
                  <thead>
                  <tr>
                    <td class="text-left">Размер</td>
                    <td class="text-left">Тип теста</td>
                    <td class="text-left">Артикул</td>
                    <?php if(!empty($product_specials)) { ?>
                      <td class="text-left" id="price-special"><strike>Цена</strike> <p style="color:red ">Цена с учетом акции</p></td>
                    <?php } else { ?>
                      <td class="text-left" id="price-special">Цена</td>
                    <?php } ?>
                    <td class="text-left">Баллы</td>
                    <td class="text-left">Вес</td>
                    <td class="text-left">Ред.</td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(isset($pizza_options)) { ?>
                    <?php foreach ($pizza_options as $pizza_option) { ?>
                      <tr id="option-pizza-row<?php echo $pizza_value_row;?>">
                        <?php foreach ($pizza_option['pizza_option_comb_data'] as $key => $pizza_option_comb_data) { ?>
                          <td width="150px">
                            <select name="pizza_option[<?php echo $pizza_value_row;?>][pizza_option_comb_data][<?php echo $key ?>][<?php echo $pizza_option_comb_data['option_my_id'];?>]" class="form-control">
                                <?php foreach ($option_my_id[$pizza_option_comb_data['option_my_id']] as $option_value) { ?>
                                  <option value="<?php echo $option_value['option_value_my_id'] ?>" <?php if($option_value['option_value_my_id'] == $pizza_option_comb_data['option_value_my_id']) echo 'selected' ?>><?php echo $option_value['name'] ?></option>
                                <?php } ?>
                            </select>
                          </td>
                        <?php } ?>
                          <td class="text-right"><input required type="text" name="pizza_option[<?php echo $pizza_value_row;?>][articul]" value="<?php echo $pizza_option['articul']?>" placeholder="Артикул" class="form-control" /></td>
                          <td class="text-right"><input required type="text" name="pizza_option[<?php echo $pizza_value_row;?>][price]" value="<?php echo $pizza_option['price']?>" placeholder="Цена" class="form-control" /></td>
                          <td class="text-right"><input required type="text" name="pizza_option[<?php echo $pizza_value_row;?>][points]" value="<?php echo $pizza_option['points']?>" placeholder="Баллы" class="form-control" /></td>
                          <td class="text-right"><input required type="text" name="pizza_option[<?php echo $pizza_value_row;?>][weight]" value="<?php echo $pizza_option['weight']?>" placeholder="Вес" class="form-control" /></td>
                          <td class="text-left"><button type="button" onclick="$(this).tooltip('destroy');$('#option-pizza-row<?php echo $pizza_value_row;?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                        </tr>
                        <?php $pizza_value_row++;?>
                    <?php } ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="6"></td>
                    <td class="text-left"><button type="button" onclick="addPizzaValue();" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                  <div style="display:none;" id="option_pizza">
                    <?php if(isset($size_pizza) && is_array($size_pizza)) : ?>
                    <?php foreach ($size_pizza as $size_pizza) { ?>
                      <option value="<?php echo $size_pizza['id_size_pizza']; ?>"><?php echo $size_pizza['name'];?></option>
                    <?php } ?>
                    <?php endif; ?>
                  </div>
                  <div style="display:none;" id="option_pizza_dough">
                    <?php if(isset($dough_pizza) && is_array($dough_pizza)) : ?>
                    <?php foreach ($dough_pizza as $dough_pizza) { ?>
                    <option value="<?php echo $dough_pizza['id_dough_pizza']; ?>"><?php echo $dough_pizza['name'];?></option>
                    <?php } ?>
                    <?php endif; ?>
                  </div>
                </table>
                <!--Пицца Американская-->
                <?php } if( $is_america ) { ?>
                <table class="table table-bordered table-hover" id="option_pizza-am-row">
                  <caption><h3>Американская пицца</h3></caption>
                  <thead>
                  <tr>
                    <td class="text-left">Размер</td>
                    <td class="text-left">Артикул</td>
                    <?php if(!empty($product_specials)) { ?>
                      <td class="text-left" id="price-special"><strike>Цена</strike> <p style="color:red ">Скидка 10 %</p></td>
                    <?php } else { ?>
                      <td class="text-left" id="price-special">Цена</td>
                    <?php } ?>
                    <td class="text-left">Баллы</td>
                    <td class="text-left">Вес</td>
                    <td class="text-left">Ред.</td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(isset($pizza_options)){ ?>
                  <?php foreach ($pizza_options as $pizza_option) { ?>
                  <tr id="option-pizza-am-row<?php echo $pizza_am_value_row;?>">
                    <?php foreach ($pizza_option['pizza_option_comb_data'] as $key => $pizza_option_comb_data) { ?>
                      <td>
                        <select name="pizza_option[<?php echo $pizza_am_value_row;?>][pizza_option_comb_data][<?php echo $key ?>][<?php echo $pizza_option_comb_data['option_my_id'];?>]" class="form-control">
                          <?php foreach ($option_my_id[$pizza_option_comb_data['option_my_id']] as $option_value) { ?>
                            <option value="<?php echo $option_value['option_value_my_id'] ?>" <?php if($option_value['option_value_my_id'] == $pizza_option_comb_data['option_value_my_id']) echo 'selected' ?>><?php echo $option_value['name'] ?></option>
                          <?php } ?>
                        </select>
                      </td>
                    <?php } ?>
                    <td class="text-right"><input required type="text" name="pizza_option[<?php echo $pizza_am_value_row;?>][articul]" value="<?php echo $pizza_option['articul']?>" placeholder="Артикул" class="form-control" /></td>
                    <td class="text-right"><input required type="text" name="pizza_option[<?php echo $pizza_am_value_row;?>][price]" value="<?php echo $pizza_option['price']?>" placeholder="Цена" class="form-control" /></td>
                    <td class="text-right"><input required type="text" name="pizza_option[<?php echo $pizza_am_value_row;?>][points]" value="<?php echo $pizza_option['points']?>" placeholder="Баллы" class="form-control" /></td>
                    <td class="text-right"><input required type="text" name="pizza_option[<?php echo $pizza_am_value_row;?>][weight]" value="<?php echo $pizza_option['weight']?>" placeholder="Вес" class="form-control" /></td>
                    <td class="text-left"><button type="button" onclick="$(this).tooltip('destroy');$('#option-pizza-am-row<?php echo $pizza_am_value_row;?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  <?php $pizza_am_value_row++;?>
                  <?php } ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="5"></td>
                    <td class="text-left"><button type="button" onclick="addPizzaAmValue();" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                </table>
                <?php } ?>
                <div style="display:none;" id="option_pizza_size_it">
                  <?php foreach ($option_my_id[1] as $option_value) { ?>
                  <option value="<?php echo $option_value['option_value_my_id'] ?>"><?php echo $option_value['name'] ?></option>
                  <?php } ?>
                </div>
                <div style="display:none;" id="option_pizza_size_am">
                  <?php foreach ($option_my_id[2] as $option_value) { ?>
                  <option value="<?php echo $option_value['option_value_my_id'] ?>"><?php echo $option_value['name'] ?></option>
                  <?php } ?>
                </div>
                <div style="display:none;" id="option_pizza_dough_it">
                  <?php foreach ($option_my_id[3] as $option_value) { ?>
                  <option value="<?php echo $option_value['option_value_my_id'] ?>"><?php echo $option_value['name'] ?></option>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- tab-pizza close-->
            <!-- tab-links open-->
            <div class="tab-pane" id="tab-links">
              <!--связь с пиццей-->

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="<?php echo $help_filter; ?>"><?php echo $entry_filter; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="filter" value="" placeholder="<?php echo $entry_filter; ?>" id="input-filter" class="form-control" />
                  <div id="product-filter" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_filters as $product_filter) { ?>
                    <div id="product-filter<?php echo $product_filter['filter_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_filter['name']; ?>
                      <input type="hidden" name="product_filter[]" value="<?php echo $product_filter['filter_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-2">
                </div>
                <div class="col-sm-10">
                  <div class="alert alert-warning">
                    <strong>Внимание!</strong> Используется только для товаров, которые продаются в одном экземпляре(Например ID карта)
                  </div>
                </div>
                <label class="col-sm-2 control-label" for="input-single-exemplar">Единственный экземляр</label>
                <div class="col-sm-10">
                  <input type="checkbox" id="input-single-exemplar" class="form-control" name="exemplar" <?php if($single_exemplar) echo "checked"; ?>>
                </div>
              </div>

              <div class="form-group" style="display: none">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $product_store)) { ?>
                        <input type="checkbox" name="product_store[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="product_store[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $product_store)) { ?>
                        <input type="checkbox" name="product_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="product_store[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- tab-links close-->
            <!-- tab-ingredients open-->
            <div class="tab-pane" id="tab-ingredients">
                <div class="table-responsive">
                <?php $ingredients_row = 0;?>
                  <?php if ($error_ingredients) { ?>
                     <div class="text-danger"><?php echo $error_ingredients; ?></div>
                  <?php } ?>
                <table class="table table-bordered table-hover" id="ingredients_table">
                  <caption><h3 >Ингредиенты </h3></caption>
                  <thead>
                  <tr>
                    <td class="text-left">Ингредиент</td>
                    <td class="text-left">Размер пиццы</td>
                    <td class="text-left">Артикул</td>
                    <td class="text-left"><span data-toggle="tooltip" title="Это число прибавляется к основной сумме,если это не состав">Цена</span></td>
                    <td class="text-left">Вес</td>
                    <td class="text-left"><span data-toggle="tooltip" title="За это клиент не будет платить, только если он выберет количество больше 1">Состав</span></td>
                    <td class="text-left">Редактировать</td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(isset($data['product_ingredients'])) { ?>
                    <?php foreach($data['product_ingredients'] as $product_ingredients) { ?>
                      <tr id ="ingredients_row_tr<?php echo $ingredients_row;?>">
                        <td style="display:none"> <input id="ingredients_id_<?php echo $ingredients_row;?>"  value="<?php echo $product_ingredients['ingredients_id']?>"> </td>
                        <td class="text-left">
                          <select id="ingredients_name_<?php echo $ingredients_row;?>" name="product_ingredients[<?php echo $ingredients_row;?>][ingredients_id]" class="form-control" onchange="changePrice(<?php echo $ingredients_row;?>)" data-live-search="true">
                            <?php foreach($data['ingredients_list'] as $ingredients_list) { ?>
                              <option value="<?php echo $ingredients_list['ingredients_id'] ?>" <?php if($ingredients_list['ingredients_id'] == $product_ingredients['ingredients_id']) echo 'selected' ?>><?php echo $ingredients_list['name']?><?php if($ingredients_list['help_string']) echo " (".$ingredients_list['help_string'].")" ?></option>
                            <?php } ?>
                          </select>
                        </td>
                        <?php if( $is_italy ) { ?>
                        <td class="text-left">
                          <select name="product_ingredients[<?php echo $ingredients_row;?>][option_value_my_id][]" class="form-control" multiple size=<?php echo count($option_my_id[1])?>>
                              <?php foreach ($option_my_id[1] as $option_value) { ?>
                                <option value="<?php echo $option_value['option_value_my_id'] ?>"<?php if(isset($product_ingredients['option_value_my_id']))foreach($product_ingredients['option_value_my_id'] as $option_value_my_id) if($option_value['option_value_my_id'] == $option_value_my_id) echo 'selected' ?>> <?php echo $option_value['name'] ?></option>
                            <?php } ?>
                          </select>
                        </td>
                        <?php } else if( $is_america ) { ?>
                        <td class="text-left">
                          <select name="product_ingredients[<?php echo $ingredients_row;?>][option_value_my_id][]" class="form-control" multiple size=<?php echo count($option_my_id[2])?>>
                              <?php foreach ($option_my_id[2] as $option_value) { ?>
                                <option value="<?php echo $option_value['option_value_my_id'] ?>"<?php if(isset($product_ingredients['option_value_my_id']))foreach($product_ingredients['option_value_my_id'] as $option_value_my_id) if($option_value['option_value_my_id'] == $option_value_my_id) echo 'selected' ?>> <?php echo $option_value['name'] ?></option>
                              <?php } ?>
                          </select>
                        </td>
                        <?php } ?>

                        <td class="text-left"><input readonly value="<?php echo $product_ingredients['price'] ?>" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][price]"></td>
                        <td class="text-left"><input readonly value="<?php echo $product_ingredients['articul'] ?>" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][articul]"></td>

                        <td class="text-left"><input readonly value="<?php echo $product_ingredients['weight'] ?>" class="form-control" name="product_ingredients[<?php echo $ingredients_row;?>][weight]"></td>

                        <td class="text-left"><input type="checkbox" <?php if(isset($product_ingredients['main_cast']) && $product_ingredients['main_cast']) echo 'checked' ?> name="product_ingredients[<?php echo $ingredients_row;?>][main_cast]" class="form-control"></td>
                        <td class="text-left"><button type="button" onclick="$(this).tooltip('destroy');$('#ingredients_row_tr<?php echo $ingredients_row;?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                      </tr>
                    <?php $ingredients_row++;?>
                    <?php } ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="6"></td>
                    <td class="text-left"><button type="button" onclick="addIngredients();" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                  </table>
                <div style="display:none;" id="product_ingredients_list">
                  <?php foreach($data['ingredients_list'] as $ingredients_list) { ?>
                    <option value="<?php echo $ingredients_list['ingredients_id'] ?>"><?php echo $ingredients_list['name']?><?php if($ingredients_list['help_string']) echo " (".$ingredients_list['help_string'].")" ?></option>
                  <?php } ?>
                </div>
                </div>

            </div>
            <!-- tab-ingredients close-->
            <!-- ИСКЛЮЧЕНО -->
            <div class="tab-pane" id="tab-attribute">
              <div class="table-responsive">
                <table id="attribute" class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_attribute; ?></td>
                    <td class="text-left"><?php echo $entry_text; ?></td>
                    <td></td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $attribute_row = 0; ?>
                  <?php if(isset($product_attributes) && is_array($product_attributes)) : ?>
                  <?php foreach ($product_attributes as $product_attribute) { ?>
                  <tr id="attribute-row<?php echo $attribute_row; ?>">
                    <td class="text-left" style="width: 40%;"><input type="text" name="product_attribute[<?php echo $attribute_row; ?>][name]" value="<?php echo $product_attribute['name']; ?>" placeholder="<?php echo $entry_attribute; ?>" class="form-control" />
                      <input type="hidden" name="product_attribute[<?php echo $attribute_row; ?>][attribute_id]" value="<?php echo $product_attribute['attribute_id']; ?>" /></td>
                    <td class="text-left"><?php foreach ($languages as $language) { ?>
                      <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                        <textarea name="product_attribute[<?php echo $attribute_row; ?>][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"><?php echo isset($product_attribute['product_attribute_description'][$language['language_id']]) ? $product_attribute['product_attribute_description'][$language['language_id']]['text'] : ''; ?></textarea>
                      </div>
                      <?php } ?></td>
                    <td class="text-left"><button type="button" onclick="$('#attribute-row<?php echo $attribute_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  <?php $attribute_row++; ?>
                  <?php } ?>
                  <?php endif; ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="2"></td>
                    <td class="text-left"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="<?php echo $button_attribute_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>

            <!-- tab-option open-->
            <div class="tab-pane" id="tab-option">
              <div class="row">
                <div class="col-sm-2">
                  <ul class="nav nav-pills nav-stacked" id="option">
                    <?php $option_row = 0; ?>
                    <?php if(isset($product_options) && is_array($product_options)) : ?>
                    <?php foreach ($product_options as $product_option) { ?>
                    <li><a href="#tab-option<?php echo $option_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-option<?php echo $option_row; ?>\']').parent().remove(); $('#tab-option<?php echo $option_row; ?>').remove(); $('#option a:first').tab('show');"></i> <?php echo $product_option['name']; ?></a></li>
                    <?php $option_row++; ?>
                    <?php } ?>
                    <?php endif; ?>
                    <li>
                      <input type="text" name="option" value="" placeholder="<?php echo $entry_option; ?>" id="input-option" class="form-control" />
                    </li>
                  </ul>
                </div>
                <div class="col-sm-10">
                  <div class="tab-content">
                    <?php $option_row = 0; ?>
                    <?php $option_value_row = 0; ?>
                    <?php if(isset($product_options) && is_array($product_options)) : ?>
                    <?php foreach ($product_options as $product_option) { ?>
                    <div class="tab-pane" id="tab-option<?php echo $option_row; ?>">
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_id]" value="<?php echo $product_option['product_option_id']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][name]" value="<?php echo $product_option['name']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][option_id]" value="<?php echo $product_option['option_id']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][type]" value="<?php echo $product_option['type']; ?>" />
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-required<?php echo $option_row; ?>"><?php echo $entry_required; ?></label>
                        <div class="col-sm-10">
                          <select name="product_option[<?php echo $option_row; ?>][required]" id="input-required<?php echo $option_row; ?>" class="form-control">
                            <?php if ($product_option['required']) { ?>
                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                            <option value="0"><?php echo $text_no; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_yes; ?></option>
                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <?php if ($product_option['type'] == 'text') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'textarea') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <textarea name="product_option[<?php echo $option_row; ?>][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control"><?php echo $product_option['value']; ?></textarea>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'file') { ?>
                      <div class="form-group" style="display: none;">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'date') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-3">
                          <div class="input-group date">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'time') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <div class="input-group time">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'datetime') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <div class="input-group datetime">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') { ?>
                      <div class="table-responsive">
                        <table id="option-value<?php echo $option_row; ?>" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                            <td class="text-left"><?php echo $entry_option_value; ?></td>
                            <td class="text-right">Артикул</td>
                            <td class="text-right"><?php echo $entry_price; ?></td>
                            <td class="text-right"><?php echo $entry_option_points; ?></td>
                            <td class="text-right"><?php echo $entry_weight; ?></td>
                            <td></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
                          <tr id="option-value-row<?php echo $option_value_row; ?>">
                            <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]" class="form-control">
                                <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" /></td>
                            <td class="text-right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][articul]" value="<?php echo $product_option_value['articul']; ?>" placeholder="Артикул" class="form-control" /></td>

                            <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price_prefix]" class="form-control">
                                <?php if ($product_option_value['price_prefix'] == '+') { ?>
                                <option value="+" selected="selected">+</option>
                                <?php } else { ?>
                                <option value="+">+</option>
                                <?php } ?>
                                <?php if ($product_option_value['price_prefix'] == '-') { ?>
                                <option value="-" selected="selected">-</option>
                                <?php } else { ?>
                                <option value="-">-</option>
                                <?php } ?>
                              </select>
                              <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price]" value="<?php echo $product_option_value['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                            <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points_prefix]" class="form-control">
                                <?php if ($product_option_value['points_prefix'] == '+') { ?>
                                <option value="+" selected="selected">+</option>
                                <?php } else { ?>
                                <option value="+">+</option>
                                <?php } ?>
                                <?php if ($product_option_value['points_prefix'] == '-') { ?>
                                <option value="-" selected="selected">-</option>
                                <?php } else { ?>
                                <option value="-">-</option>
                                <?php } ?>
                              </select>
                              <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points]" value="<?php echo $product_option_value['points']; ?>" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>
                            <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight_prefix]" class="form-control">
                                <?php if ($product_option_value['weight_prefix'] == '+') { ?>
                                <option value="+" selected="selected">+</option>
                                <?php } else { ?>
                                <option value="+">+</option>
                                <?php } ?>
                                <?php if ($product_option_value['weight_prefix'] == '-') { ?>
                                <option value="-" selected="selected">-</option>
                                <?php } else { ?>
                                <option value="-">-</option>
                                <?php } ?>
                              </select>
                              <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight]" value="<?php echo $product_option_value['weight']; ?>" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>
                            <td class="text-left"><button type="button" onclick="$(this).tooltip('destroy');$('#option-value-row<?php echo $option_value_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                          </tr>
                          <?php $option_value_row++; ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <td colspan="5"></td>
                            <td class="text-left"><button type="button" onclick="addOptionValue('<?php echo $option_row; ?>');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                      <select id="option-values<?php echo $option_row; ?>" style="display: none;">
                        <?php if (isset($option_values[$product_option['option_id']])) { ?>
                        <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                        <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                      <?php } ?>

                      <?php if ($product_option['type'] == 'select_2') { ?>

                      <div class="table-responsive">
                        <table id="option-value<?php echo $option_row; ?>" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                            <td class="text-left">Размер теста</td>
                            <td class="text-left">Тип теста</td>
                            <td class="text-right">Артикул</td>
                            <td class="text-right"><?php echo $entry_price; ?></td>
                            <td class="text-right"><?php echo $entry_option_points; ?></td>
                            <td class="text-right"><?php echo $entry_weight; ?></td>
                            <td></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
                          <tr id="option-value-row<?php echo $option_value_row; ?>">
                            <!-- Размер -->
                            <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]" class="form-control">
                                <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                  <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                    <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                        <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
                                      <?php } else { ?>
                                        <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                      <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" /></td>
                            <!-- Тип -->
                            <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_2_id]" class="form-control">
                                <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                  <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                    <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                      <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
                                    <?php } else { ?>
                                      <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" /></td>
                            <!-- Артикул -->
                            <td class="text-right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][articul]" value="<?php echo $product_option_value['articul']; ?>" placeholder="Артикул" class="form-control" /></td>

                            <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price_prefix]" class="form-control">
                                <?php if ($product_option_value['price_prefix'] == '+') { ?>
                                <option value="+" selected="selected">+</option>
                                <?php } else { ?>
                                <option value="+">+</option>
                                <?php } ?>
                                <?php if ($product_option_value['price_prefix'] == '-') { ?>
                                <option value="-" selected="selected">-</option>
                                <?php } else { ?>
                                <option value="-">-</option>
                                <?php } ?>
                              </select>
                              <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price]" value="<?php echo $product_option_value['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                            <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points_prefix]" class="form-control">
                                <?php if ($product_option_value['points_prefix'] == '+') { ?>
                                <option value="+" selected="selected">+</option>
                                <?php } else { ?>
                                <option value="+">+</option>
                                <?php } ?>
                                <?php if ($product_option_value['points_prefix'] == '-') { ?>
                                <option value="-" selected="selected">-</option>
                                <?php } else { ?>
                                <option value="-">-</option>
                                <?php } ?>
                              </select>
                              <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points]" value="<?php echo $product_option_value['points']; ?>" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>
                            <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight_prefix]" class="form-control">
                                <?php if ($product_option_value['weight_prefix'] == '+') { ?>
                                <option value="+" selected="selected">+</option>
                                <?php } else { ?>
                                <option value="+">+</option>
                                <?php } ?>
                                <?php if ($product_option_value['weight_prefix'] == '-') { ?>
                                <option value="-" selected="selected">-</option>
                                <?php } else { ?>
                                <option value="-">-</option>
                                <?php } ?>
                              </select>
                              <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight]" value="<?php echo $product_option_value['weight']; ?>" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>
                            <td class="text-left"><button type="button" onclick="$(this).tooltip('destroy');$('#option-value-row<?php echo $option_value_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                          </tr>
                          <?php $option_value_row++; ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <td colspan="6"></td>
                            <td class="text-left"><button type="button" onclick="addOptionValue('<?php echo $option_row; ?>');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                      <select id="option-values<?php echo $option_row; ?>" style="display: none;">
                        <?php if (isset($option_values[$product_option['option_id']])) { ?>
                          <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                            <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                          <?php } ?>
                        <?php } ?>
                      </select>

                      <select id="option-values<?php echo $option_row; ?>" style="display: none;">
                        <?php if (isset($option_values[$product_option['option_id']])) { ?>
                          <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                            <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                          <?php } ?>
                        <?php } ?>
                      </select>
                      <?php } ?>

                    </div>
                    <?php $option_row++; ?>
                    <?php } ?>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- tab-option close-->

            <div class="tab-pane" id="tab-recurring">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_recurring; ?></td>
                    <td class="text-left"><?php echo $entry_customer_group; ?></td>
                    <td class="text-left"></td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $recurring_row = 0; ?>
                  <?php if(isset($product_recurrings) && is_array($product_recurrings)) : ?>
                  <?php foreach ($product_recurrings as $product_recurring) { ?>

                  <tr id="recurring-row<?php echo $recurring_row; ?>">
                    <td class="text-left"><select name="product_recurring[<?php echo $recurring_row; ?>][recurring_id]" class="form-control">
                        <?php foreach ($recurrings as $recurring) { ?>
                        <?php if ($recurring['recurring_id'] == $product_recurring['recurring_id']) { ?>
                        <option value="<?php echo $recurring['recurring_id']; ?>" selected="selected"><?php echo $recurring['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select></td>
                    <td class="text-left"><select name="product_recurring[<?php echo $recurring_row; ?>][customer_group_id]" class="form-control">
                        <?php foreach ($customer_groups as $customer_group) { ?>
                        <?php if ($customer_group['customer_group_id'] == $product_recurring['customer_group_id']) { ?>
                        <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select></td>
                    <td class="text-left"><button type="button" onclick="$('#recurring-row<?php echo $recurring_row; ?>').remove()" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  <?php $recurring_row++; ?>
                  <?php } ?>
                  <?php endif; ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="2"></td>
                    <td class="text-left"><button type="button" onclick="addRecurring()" data-toggle="tooltip" title="<?php echo $button_recurring_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!--Скидки-->
            <div class="tab-pane" id="tab-discount">
              <div class="table-responsive">
                <table id="discount" class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_customer_group; ?></td>
                    <td class="text-right"><?php echo $entry_quantity; ?></td>
                    <td class="text-right"><?php echo $entry_priority; ?></td>
                    <td class="text-right"><?php echo $entry_price; ?></td>
                    <td class="text-left"><?php echo $entry_date_start; ?></td>
                    <td class="text-left"><?php echo $entry_date_end; ?></td>
                    <td></td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $discount_row = 0; ?>
                  <?php foreach ($product_discounts as $product_discount) { ?>
                  <tr id="discount-row<?php echo $discount_row; ?>">
                    <td class="text-left"><select name="product_discount[<?php echo $discount_row; ?>][customer_group_id]" class="form-control">
                        <?php foreach ($customer_groups as $customer_group) { ?>
                        <?php if ($customer_group['customer_group_id'] == $product_discount['customer_group_id']) { ?>
                        <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select></td>
                    <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][quantity]" value="<?php echo $product_discount['quantity']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>
                    <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][priority]" value="<?php echo $product_discount['priority']; ?>" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>

                    <td class="text-right"><div class="form-inline"><input type="text" id="precent_d_<?php echo $discount_row; ?>" onkeyup="changeDiscount(<?php echo $discount_row; ?>);" style="width:50px" class="form-control">% <input type="text" name="product_discount[<?php echo $discount_row; ?>][price]" value="<?php echo $discount_row['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control price_d" onkeyup="changePercentDisc(<?php echo $discount_row; ?>);"/></div></td>
                    <td class="text-left" style="width: 20%;"><div class="input-group date">
                        <input type="text" name="product_discount[<?php echo $discount_row; ?>][date_start]" value="<?php echo $product_discount['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                    <td class="text-left" style="width: 20%;"><div class="input-group date">
                        <input type="text" name="product_discount[<?php echo $discount_row; ?>][date_end]" value="<?php echo $product_discount['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                    <td class="text-left"><button type="button" onclick="$('#discount-row<?php echo $discount_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  <?php $discount_row++; ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="6"></td>
                    <td class="text-left"><button type="button" onclick="addDiscount();" data-toggle="tooltip" title="<?php echo $button_discount_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!--акции-->
            <div class="tab-pane" id="tab-special">
              <div class="table-responsive">
                <table id="special" class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_customer_group; ?></td>
                    <td class="text-right"><?php echo $entry_priority; ?></td>
                    <td class="text-right"><?php echo $entry_price; ?></td>
                    <td class="text-left"><?php echo $entry_date_start; ?></td>
                    <td class="text-left"><?php echo $entry_date_end; ?></td>
                    <td></td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $special_row = 0; ?>
                  <?php foreach ($product_specials as $product_special) { ?>
                  <tr id="special-row<?php echo $special_row; ?>">
                    <td class="text-left"><select name="product_special[<?php echo $special_row; ?>][customer_group_id]" class="form-control">
                        <?php foreach ($customer_groups as $customer_group) { ?>
                        <?php if ($customer_group['customer_group_id'] == $product_special['customer_group_id']) { ?>
                        <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select></td>
                    <td class="text-right"><input type="text" name="product_special[<?php echo $special_row; ?>][priority]" value="<?php echo $product_special['priority']; ?>" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>
                    <td class="text-right"><div class="form-inline"><input type="text" id="precent_s_<?php echo $special_row; ?>" onkeyup="changeSpecial(<?php echo $special_row; ?>);" style="width:50px" class="form-control">Скидка в % <input type="text" name="product_special[<?php echo $special_row; ?>][price]" value="<?php echo $product_special['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control price_s" onkeyup="changePercentSp(<?php echo $special_row; ?>);"/></div></td>
                    <td class="text-left" style="width: 20%;"><div class="input-group date">
                        <input type="text" name="product_special[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                    <td class="text-left" style="width: 20%;"><div class="input-group date">
                        <input type="text" name="product_special[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                    <td class="text-left"><button type="button" onclick="$('#special-row<?php echo $special_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  <?php $special_row++; ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="5"></td>
                    <td class="text-left"><button type="button" onclick="addSpecial();" data-toggle="tooltip" title="<?php echo $button_special_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-image">
              <div class="table-responsive">
                <table id="images" class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_image; ?></td>
                    <td class="text-right"><?php echo $entry_sort_order; ?></td>
                    <td></td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $image_row = 0; ?>
                  <?php foreach ($product_images as $product_image) { ?>
                  <tr id="image-row<?php echo $image_row; ?>">
                    <td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $product_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="input-image<?php echo $image_row; ?>" /></td>
                    <td class="text-right"><input type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                    <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  <?php $image_row++; ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="2"></td>
                    <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-reward">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-points"><span data-toggle="tooltip" title="<?php echo $help_points; ?>"><?php echo $entry_points; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="points" value="<?php echo $points; ?>" placeholder="<?php echo $entry_points; ?>" id="input-points" class="form-control" />
                </div>
              </div>
              <!--<div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_customer_group; ?></td>
                    <td class="text-right"><?php echo $entry_reward; ?></td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($customer_groups as $customer_group) { ?>
                  <tr>
                    <td class="text-left"><?php echo $customer_group['name']; ?></td>
                    <td class="text-right"><input type="text" name="product_reward[<?php echo $customer_group['customer_group_id']; ?>][points]" value="<?php echo isset($product_reward[$customer_group['customer_group_id']]) ? $product_reward[$customer_group['customer_group_id']]['points'] : ''; ?>" class="form-control" /></td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>-->
            </div>
            <div class="tab-pane" id="tab-design">
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_store; ?></td>
                    <td class="text-left"><?php echo $entry_layout; ?></td>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td class="text-left"><?php echo $text_default; ?></td>
                    <td class="text-left"><select name="product_layout[0]" class="form-control">
                        <option value=""></option>
                        <?php foreach ($layouts as $layout) { ?>
                        <?php if (isset($product_layout[0]) && $product_layout[0] == $layout['layout_id']) { ?>
                        <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select></td>
                  </tr>
                  <?php foreach ($stores as $store) { ?>
                  <tr>
                    <td class="text-left"><?php echo $store['name']; ?></td>
                    <td class="text-left"><select name="product_layout[<?php echo $store['store_id']; ?>]" class="form-control">
                        <option value=""></option>
                        <?php foreach ($layouts as $layout) { ?>
                        <?php if (isset($product_layout[$store['store_id']]) && $product_layout[$store['store_id']] == $layout['layout_id']) { ?>
                        <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select></td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <input type="hidden" name="apply" id="apply" value="0">
        </form>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function() {
      json_ingredients_list = JSON.parse('<?php if(isset($json_ingredients_list)) echo $json_ingredients_list ?>');
      is_america = false;
      is_italy = false;
    });
  </script>
  <script type="text/javascript"><!--
    <?php foreach ($languages as $language) { ?>
      $('#input-description<?php echo $language['language_id']; ?>').summernote({height: 300});
    <?php } ?>
    //--></script>
  <script type="text/javascript"><!--
    // Manufacturer
    $('input[name=\'manufacturer\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            json.unshift({
              manufacturer_id: 0,
              name: '<?php echo $text_none; ?>'
            });

            response($.map(json, function(item) {
              return {
                label: item['name'],
                value: item['manufacturer_id']
              }
            }));
          }
        });
      },
      'select': function(item) {
        $('input[name=\'manufacturer\']').val(item['label']);
        $('input[name=\'manufacturer_id\']').val(item['value']);
      }
    });

    // Category
    $('input[name=\'category\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                label: item['name'],
                value: item['category_id']
              }
            }));
          }
        });
      },
      'select': function(item) {
        $('input[name=\'category\']').val('');

        $('#product-category' + item['value']).remove();

        $('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_category[]" value="' + item['value'] + '" /></div>');

        $('#product-category-text').attr("type","hidden");

        /*Вставка размеров пиццы*/
      }
    });

    $('#product-category').delegate('.fa-minus-circle', 'click', function() {
      $(this).parent().remove();
      $('#product-category-text').attr("type","text");
    });

    // Filter
    $('input[name=\'filter\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                label: item['name'],
                value: item['filter_id']
              }
            }));
          }
        });
      },
      'select': function(item) {
        $('input[name=\'filter\']').val('');

        $('#product-filter' + item['value']).remove();

        $('#product-filter').append('<div id="product-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_filter[]" value="' + item['value'] + '" /></div>');
      }
    });

    $('#product-filter').delegate('.fa-minus-circle', 'click', function() {
      $(this).parent().remove();
    });

    // Downloads
    $('input[name=\'download\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=catalog/download/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                label: item['name'],
                value: item['download_id']
              }
            }));
          }
        });
      },
      'select': function(item) {
        $('input[name=\'download\']').val('');

        $('#product-download' + item['value']).remove();

        $('#product-download').append('<div id="product-download' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_download[]" value="' + item['value'] + '" /></div>');
      }
    });

    $('#product-download').delegate('.fa-minus-circle', 'click', function() {
      $(this).parent().remove();
    });

    // Related
    $('input[name=\'related\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                label: item['name'],
                value: item['product_id']
              }
            }));
          }
        });
      },
      'select': function(item) {
        $('input[name=\'related\']').val('');

        $('#product-related' + item['value']).remove();

        $('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');
      }
    });

    $('#product-related').delegate('.fa-minus-circle', 'click', function() {
      $(this).parent().remove();
    });
    //--></script>
  <script type="text/javascript"><!--
    var attribute_row = <?php echo $attribute_row; ?>;

    function addAttribute() {
      html  = '<tr id="attribute-row' + attribute_row + '">';
      html += '  <td class="text-left" style="width: 20%;"><input type="text" name="product_attribute[' + attribute_row + '][name]" value="" placeholder="<?php echo $entry_attribute; ?>" class="form-control" /><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value="" /></td>';
      html += '  <td class="text-left">';
    <?php foreach ($languages as $language) { ?>
        html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span><textarea name="product_attribute[' + attribute_row + '][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"></textarea></div>';
      <?php } ?>
      html += '  </td>';
      html += '  <td class="text-left"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#attribute tbody').append(html);

      attributeautocomplete(attribute_row);

      attribute_row++;
    }

    function attributeautocomplete(attribute_row) {
      $('input[name=\'product_attribute[' + attribute_row + '][name]\']').autocomplete({
        'source': function(request, response) {
          $.ajax({
            url: 'index.php?route=catalog/attribute/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
            dataType: 'json',
            success: function(json) {
              response($.map(json, function(item) {
                return {
                  category: item.attribute_group,
                  label: item.name,
                  value: item.attribute_id
                }
              }));
            }
          });
        },
        'select': function(item) {
          $('input[name=\'product_attribute[' + attribute_row + '][name]\']').val(item['label']);
          $('input[name=\'product_attribute[' + attribute_row + '][attribute_id]\']').val(item['value']);
        }
      });
    }

    $('#attribute tbody tr').each(function(index, element) {
      attributeautocomplete(index);
    });
    //--></script>
  <script type="text/javascript"><!--
    var option_row = <?php echo $option_row; ?>;

    $('input[name=\'option\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=catalog/option/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                category: item['category'],
                label: item['name'],
                value: item['option_id'],
                type: item['type'],
                option_value: item['option_value']
              }
            }));
          }
        });
      },
      'select': function(item) {
        html  = '<div class="tab-pane" id="tab-option' + option_row + '">';
        html += '	<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
        html += '	<input type="hidden" name="product_option[' + option_row + '][name]" value="' + item['label'] + '" />';
        html += '	<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + item['value'] + '" />';
        html += '	<input type="hidden" name="product_option[' + option_row + '][type]" value="' + item['type'] + '" />';

        html += '	<div class="form-group">';
        html += '	  <label class="col-sm-2 control-label" for="input-required' + option_row + '"><?php echo $entry_required; ?></label>';
        html += '	  <div class="col-sm-10"><select name="product_option[' + option_row + '][required]" id="input-required' + option_row + '" class="form-control">';
        html += '	      <option value="1"><?php echo $text_yes; ?></option>';
        html += '	      <option value="0"><?php echo $text_no; ?></option>';
        html += '	  </select></div>';
        html += '	</div>';

        if (item['type'] == 'text') {
          html += '	<div class="form-group">';
          html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
          html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
          html += '	</div>';
        }

        if (item['type'] == 'textarea') {
          html += '	<div class="form-group">';
          html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
          html += '	  <div class="col-sm-10"><textarea name="product_option[' + option_row + '][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control"></textarea></div>';
          html += '	</div>';
        }

        if (item['type'] == 'file') {
          html += '	<div class="form-group" style="display: none;">';
          html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
          html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
          html += '	</div>';
        }

        if (item['type'] == 'date') {
          html += '	<div class="form-group">';
          html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
          html += '	  <div class="col-sm-3"><div class="input-group date"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
          html += '	</div>';
        }

        if (item['type'] == 'time') {
          html += '	<div class="form-group">';
          html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
          html += '	  <div class="col-sm-10"><div class="input-group time"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
          html += '	</div>';
        }

        if (item['type'] == 'datetime') {
          html += '	<div class="form-group">';
          html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
          html += '	  <div class="col-sm-10"><div class="input-group datetime"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
          html += '	</div>';
        }

        if (item['type'] == 'select' || item['type'] == 'radio' || item['type'] == 'checkbox' || item['type'] == 'image') {
          html += '<div class="table-responsive">';
          html += '  <table id="option-value' + option_row + '" class="table table-striped table-bordered table-hover">';
          html += '  	 <thead>';
          html += '      <tr>';
          html += '        <td class="text-left"><?php echo $entry_option_value; ?></td>';
          html += '        <td class="text-right">Артикул</td>';
          html += '        <td class="text-right"><?php echo $entry_price; ?></td>';
          html += '        <td class="text-right"><?php echo $entry_option_points; ?></td>';
          html += '        <td class="text-right"><?php echo $entry_weight; ?></td>';
          html += '        <td></td>';
          html += '      </tr>';
          html += '  	 </thead>';
          html += '  	 <tbody>';
          html += '    </tbody>';
          html += '    <tfoot>';
          html += '      <tr>';
          html += '        <td colspan="5"></td>';
          html += '        <td class="text-left"><button type="button" onclick="addOptionValue(' + option_row + ');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>';
          html += '      </tr>';
          html += '    </tfoot>';
          html += '  </table>';
          html += '</div>';

          html += '  <select id="option-values' + option_row + '" style="display: none;">';

          for (i = 0; i < item['option_value'].length; i++) {
            html += '  <option value="' + item['option_value'][i]['option_value_id'] + '">' + item['option_value'][i]['name'] + '</option>';
          }

          html += '  </select>';
          html += '</div>';
        }

        $('#tab-option .tab-content').append(html);

        $('#option > li:last-child').before('<li><a href="#tab-option' + option_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#tab-option' + option_row + '\\\']\').parent().remove(); $(\'#tab-option' + option_row + '\').remove(); $(\'#option a:first\').tab(\'show\')"></i> ' + item['label'] + '</li>');

        $('#option a[href=\'#tab-option' + option_row + '\']').tab('show');

        $('.date').datetimepicker({
          pickTime: false
        });

        $('.time').datetimepicker({
          pickDate: false
        });

        $('.datetime').datetimepicker({
          pickDate: true,
          pickTime: true
        });

        option_row++;
      }
    });
    //--></script>
  <script type="text/javascript"><!--
    var option_value_row = <?php echo $option_value_row; ?>;

    function addOptionValue(option_row) {
      html  = '<tr id="option-value-row' + option_value_row + '">';
      html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]" class="form-control">';
      html += $('#option-values' + option_row).html();
      html += '  </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
      html += '  <td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][articul]" value="" placeholder="Артикул" class="form-control" /></td>';
      html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price_prefix]" class="form-control">';
      html += '    <option value="+">+</option>';
      html += '    <option value="-">-</option>';
      html += '  </select>';
      html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
      html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points_prefix]" class="form-control">';
      html += '    <option value="+">+</option>';
      html += '    <option value="-">-</option>';
      html += '  </select>';
      html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points]" value="" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>';
      html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight_prefix]" class="form-control">';
      html += '    <option value="+">+</option>';
      html += '    <option value="-">-</option>';
      html += '  </select>';
      html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight]" value="" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>';
      html += '  <td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#option-value' + option_row + ' tbody').append(html);
      $('[rel=tooltip]').tooltip();

      option_value_row++;
    }
    //--></script>
  <script type="text/javascript">
    var pizza_value_it_row = <?php echo $pizza_value_row; ?>;

    function addPizzaValue() {
      initPizza(true);
      var html  = '<tr id="option-pizza-row' + pizza_value_it_row + '">';
      html += '  <td class="text-right"><select name = "pizza_option[' + pizza_value_it_row + '][pizza_option_comb_data][0][1]" class="form-control">';
      html +=    $('#option_pizza_size_it').html();
      html += '  </select></td>';
      html += '  <td class="text-right"><select name = "pizza_option[' + pizza_value_it_row + '][pizza_option_comb_data][1][3]" class="form-control">';
      html +=    $('#option_pizza_dough_it').html();
      html += '  </select></td>';
      html += '  <td class="text-right"><input type="text" name="pizza_option[' + pizza_value_it_row + '][articul]" required value="" placeholder="Артикул" class="form-control" /></td>';
      html += '  <td class="text-right"><input type="text" name="pizza_option[' + pizza_value_it_row + '][price]" required value="" placeholder="Цена" class="form-control" /></td>';
      html += '  <td class="text-right"><input type="text" name="pizza_option[' + pizza_value_it_row + '][points]" required value="0" placeholder="Баллы" class="form-control" /></td>';
      html += '  <td class="text-right"><input type="text" name="pizza_option[' + pizza_value_it_row + '][weight]" required value="" placeholder="Вес" class="form-control" /></td>';
      html += '  <td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#option-pizza-row' + pizza_value_it_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#option_pizza-row tbody').append(html);
     // $('[rel=tooltip]').tooltip();
      pizza_value_it_row++;
    }
    function initPizza(param) {
     /* var is_italy = false;
      var is_america = false;

      param ? is_italy = true : is_america = true;*/
      var articul = $('#input-articul');
      articul.prop('readonly',true);
      articul.prop('value','');
      articul.prop('placeholder','Вы используете опцию')

      var price = $('#input-price');
      price.prop('readonly',true);
      price.prop('value','');
      price.prop('placeholder','Вы используете опцию');

      var weight = $('#input-weight');
      weight.prop('readonly',true);
      weight.prop('value','');
      weight.prop('placeholder','Вы используете опцию');

      var weight = $('#input-weight-class');
      weight.prop('disabled',true);
      //weight.prop('value','');


      $('#tab-discount').remove();
      $('#tab-special').remove();
      $('#tab-reward').remove();

      $('#remove1').remove();
      $('#remove2').remove();
      $('#remove3').remove();

      $('#tab-ingredients-none').css('display','block');

      if(param) {
        is_italy = true;
        $('#option_pizza-am-row').remove();
      }
      else {
        is_america = true;
        $('#option_pizza-row').remove();
      }
    }
  </script>

  <script type="text/javascript">
    var pizza_value_row = <?php echo $pizza_am_value_row; ?>;
    function addPizzaAmValue() {
      initPizza(false);
      var html  = '<tr id="option-pizza-am-row' + pizza_value_row + '">';
      html += '  <td class="text-right"><select name = "pizza_option[' + pizza_value_row + '][pizza_option_comb_data][0][2]" class="form-control">';
      html +=    $('#option_pizza_size_am').html();
      html += '  </select></td>';
      html += '  <td class="text-right"><input type="text" name="pizza_option[' + pizza_value_row + '][articul]" required value="" placeholder="Артикул" class="form-control" /></td>';
      html += '  <td class="text-right"><input type="text" name="pizza_option[' + pizza_value_row + '][price]" required value="" placeholder="Цена" class="form-control" /></td>';
      html += '  <td class="text-right"><input type="text" name="pizza_option[' + pizza_value_row + '][points]" required value="0" placeholder="Баллы" class="form-control" /></td>';
      html += '  <td class="text-right"><input type="text" name="pizza_option[' + pizza_value_row + '][weight]" required value="" placeholder="Вес" class="form-control" /></td>';
      html += '  <td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#option-pizza-am-row' + pizza_value_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#option_pizza-am-row tbody').append(html);
      // $('[rel=tooltip]').tooltip();
      pizza_value_row++;
    }
    var ingredients_row = <?php echo $ingredients_row; ?>;

    function changePrice(param){
      var ingredients_name =  $('select[name^="product_ingredients[' + param + '][ingredients_id]"]').find(':selected').text();
      jQuery.each( json_ingredients_list, function( i, val ) {
         if(val.name == ingredients_name) {
           $('input[name^="product_ingredients[' + param + '][price]"]').val(val.price);
           $('input[name^="product_ingredients[' + param + '][weight]"]').val(val.weight);
           $('input[name^="product_ingredients[' + param + '][articul]"]').val(val.articul);
         }
      });
    }
    function addIngredients() {
      <?php if($is_america) { ?>
        is_america = true;
      <?php } else if($is_italy) { ?>
        is_italy = true;
      <?php }  ?>

      var click = 'onchange=changePrice('+ingredients_row+')';
      var html  = '<tr id="ingredients_row_tr' + ingredients_row + '">';
      html += '  <td class="text-right"><select name = "product_ingredients[' + ingredients_row + '][ingredients_id]" class="form-control" '+click+'> ';
      html +=    $('#product_ingredients_list').html();
      html += '  </select></td>';
    if(is_italy) {
      html += '  <td class="text-right"><select name = "product_ingredients[' + ingredients_row + '][option_value_my_id][]" class="form-control" multiple size="<?php echo count($option_my_id[1])?>"> ';
      html +=     $('#option_pizza_size_it').html();
      html += '  </select></td>';
    } else if(is_america) {
      html += '  <td class="text-right"><select name = "product_ingredients[' + ingredients_row + '][option_value_my_id][]" class="form-control" multiple size="<?php echo count($option_my_id[2])?>"> ';
      html +=     $('#option_pizza_size_am').html();
      html += '  </select></td>';
    }
      html += ' <td class="text-left"><input readonly type="text" value="0" name="product_ingredients[' + ingredients_row + '][articul]"  class="form-control"> </td>';
      html += ' <td class="text-left"><input readonly type="text" value="0" name="product_ingredients[' + ingredients_row + '][price]"  class="form-control"> </td>';
      html += ' <td class="text-left"><input readonly type="text" value="0" name="product_ingredients[' + ingredients_row + '][weight]"  class="form-control"> </td>';
      html += ' <td class="text-left"><input type="checkbox" name="product_ingredients[' + ingredients_row + '][main_cast]" class="form-control"></td>';
      html += ' <td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#ingredients_row_tr' + ingredients_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#ingredients_table tbody').append(html);
      var ingredients_name = $("#ingredients_name_"+ingredients_row).find(':selected').text();
      // $('[rel=tooltip]').tooltip();
      changePrice(ingredients_row);
      ingredients_row++;
      $(select).selectpicker('refresh');
    }
  </script>


  <script type="text/javascript"><!--
    var discount_row = <?php echo $discount_row; ?>;

    function addDiscount() {
      html  = '<tr id="discount-row' + discount_row + '">';
      html += '  <td class="text-left"><select name="product_discount[' + discount_row + '][customer_group_id]" class="form-control">';
    <?php foreach ($customer_groups as $customer_group) { ?>
        html += '    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo addslashes($customer_group['name']); ?></option>';
      <?php } ?>
      html += '  </select></td>';
      html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][quantity]" value="" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>';
      html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][priority]" value="" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>';
      html += '  <td class="text-right"><div class="form-inline">';
      html += '  <input type="text" id="precent_d_' + discount_row + '" onkeyup="changeDiscount(' + discount_row + ');" style="width:50px" class="form-control">% ';
      html += '  <input type="text" name="product_discount[' + discount_row + '][price]" placeholder="<?php echo $entry_price; ?>" class="form-control price_s" onkeyup="changePercentDisc(' + discount_row + ');"/>';
      html += '  </div></td>';
      html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_discount[' + discount_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
      html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_discount[' + discount_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
      html += '  <td class="text-left"><button type="button" onclick="$(\'#discount-row' + discount_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#discount tbody').append(html);

      $('.date').datetimepicker({
        pickTime: false
      });

      discount_row++;
    }
    //--></script>
  <script type="text/javascript"><!--
    var special_row = <?php echo $special_row; ?>;

    function addSpecial() {
      html  = '<tr id="special-row' + special_row + '">';
      html += '  <td class="text-left"><select name="product_special[' + special_row + '][customer_group_id]" class="form-control">';
    <?php foreach ($customer_groups as $customer_group) { ?>
        html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo addslashes($customer_group['name']); ?></option>';
      <?php } ?>
      html += '  </select></td>';
      html += '  <td class="text-right"><input type="text" name="product_special[' + special_row + '][priority]" value="" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>';
      html += '  <td class="text-right"><div class="form-inline">';
      html += '  <input type="text" id="precent_s_' + special_row + '" onkeyup="changeSpecial(' + special_row + ');" style="width:50px" class="form-control" >Скидка в %';
      html += '  <input type="text" name="product_special[' + special_row + '][price]" placeholder="<?php echo $entry_price; ?>" class="form-control price_s" onkeyup="changePercentSp(' + special_row + ');"/>';
      html += '  </div></td>';
      html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
      html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
      html += '  <td class="text-left"><button type="button" onclick="$(\'#special-row' + special_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#special tbody').append(html);
      $('#price-special').replaceWith('<td class="text-left" id="price-special"><strike>Цена</strike> <p style="color:red ">Цена с учетом акции</p></td>');

      $('.date').datetimepicker({
        pickTime: false,
      });
      /*$('.date').datetimepicker.setDefaults(
              $.extend(
                      $.datepicker.regional['fr']
              )
      );
      $.datepicker.regional['fr']*/

      special_row++;
    }
    //--></script>
  <script type="text/javascript"><!--
    var image_row = <?php echo $image_row; ?>;

    function addImage() {
      html  = '<tr id="image-row' + image_row + '">';
      html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
      html += '  <td class="text-right"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
      html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#images tbody').append(html);

      image_row++;
    }
    //--></script>
  <script type="text/javascript"><!--
    $('.date').datetimepicker({
      pickTime: false
    });

    $('.time').datetimepicker({
      pickDate: false
    });

    $('.datetime').datetimepicker({
      pickDate: true,
      pickTime: true
    });
    //--></script>
  <script type="text/javascript"><!--
    $('#language a:first').tab('show');
    $('#option a:first').tab('show');
    //--></script></div>
<?php echo $footer; ?>

<script><!--
  $( document ).ready(function() {

    var price = parseFloat($("input[name=price]").val());
    if (price >0.0001 && price != "") {
      // DISABLE IF PRICE IS 0!!!
      //discount
      $('.price_d').each(function(index,data) {
        var discount_price = $('input[name^="product_discount['+index+'][price]"]').val();
        var recalculate = 100-(discount_price/price*100);
        $('#precent_d_'+index).val(recalculate);
      });

      //special
      $('.price_s').each(function(index,data) {
        var special_price = parseFloat($('input[name^="product_special['+index+'][price]"]').val());
        var recalculate = 100-(special_price/price*100);
        $('#precent_s_'+index).val(recalculate);
      });
    }
  });

  //Special
  function changeSpecial(row_id){
    var price = parseFloat($("input[name=price]").val());
    var precent = $('#precent_s_'+row_id).val();
    if (precent <= 100 || $.isNumeric(precent) == true){
      var recalculated =  price - ( price * precent / 100 );
      $('input[name^="product_special['+row_id+'][price]"]').val(recalculated);
    }else{
      $('input[name^="product_special['+row_id+'][price]"]').val(0);
    }
    $('#price-special').replaceWith('<td class="text-left" id="price-special"><strike>Цена</strike> <p style="color:red ">Скидка '+ precent +'%</p></td>');
  }

  //Discount
  function changeDiscount(row_id){
    var price = parseFloat($("input[name=price]").val());
    var precent = $('#precent_d_'+row_id).val();
    if (precent <= 100 || $.isNumeric(precent) == true){
      var recalculated =  price - ( price * precent / 100 );
      $('input[name^="product_discount['+row_id+'][price]"]').val(recalculated);
    }else{
      $('input[name^="product_discount['+row_id+'][price]"]').val(0);
    }
  }

  //Special
  function changePercentSp(row_id){
    var price = parseFloat($("input[name=price]").val());
    var special =  parseFloat($('input[name^="product_special['+row_id+'][price]"]').val());
    if (special <= price || $.isNumeric(special) == true){
      var discount_precent = 100-(special/price*100);
      $('#precent_s_'+row_id).val(discount_precent);
    }else{
      $('#precent_s_'+row_id).val("");
    }
  }

  function changePercentDisc(row_id){
    var price = parseFloat($("input[name=price]").val());
    var special =  parseFloat($('input[name^="product_discount['+row_id+'][price]"]').val());
    if (special <= price || $.isNumeric(special) == true){
      var discount_precent = 100-(special/price*100);
      $('#precent_d_'+row_id).val(discount_precent);
    }else{
      $('#precent_s_'+row_id).val("");
    }
  }
  //--></script>
<script>
$(document).ready(function(e) {
  $('.selectpicker').selectpicker();
});</script>
